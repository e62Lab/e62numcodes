/**
 * @file
 * @brief Implementation of non templated functions defined in the header common.hpp.
 */

#include "common.hpp"

/**
 * A namespace wrapping everything in this library. Name mm stands for Meshless Machine.
 */
namespace mm {

namespace assert_internal {
bool assert_handler_implementation(const char* condition, const char* file, int line,
                                   const char* message, tfm::FormatListRef format_list) {
    std::cerr << "\x1b[37;1m";  // white bold
    tfm::format(std::cerr, "%s:%d: Assertion `%s' failed with message:\n", file, line, condition);
    std::cerr << "\x1b[31;1m";  // red bold
    tfm::vformat(std::cerr, message, format_list);
    std::cerr << "\x1b[37;0m\n";  // no color
    std::cerr.flush();
    return true;
}
}  // namespace assert_internal

std::ostream& print_formatted(int x, const std::string&, const std::string&,
                              const std::string&, const std::string&, std::ostream& xx) {
    return xx << x;
}
std::ostream& print_formatted(double x, const std::string&, const std::string&,
                              const std::string&, const std::string&, std::ostream& xx) {
    return xx << std::fixed << std::setprecision(16) << x;
}

void print_red(const std::string& s) { std::cout << "\x1b[31;1m" << s << "\x1b[37;0m"; }

void print_white(const std::string& s) { std::cout << "\x1b[37;1m" << s << "\x1b[37;0m"; }

void print_green(const std::string& s) { std::cout << "\x1b[32;1m" << s << "\x1b[37;0m"; }

unsigned int get_seed() {
    try {
        std::random_device rd;
        return rd();
    } catch (std::exception& e) {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }
}

std::string mem2str(size_t bytes) {
    double amount = bytes;
    std::vector<std::string> suffix = {"B", "kB", "MB", "GB"};
    for (int i = 0; i < 4; ++i) {
        if (amount < 100) {
            std::stringstream ss;
            ss << static_cast<int>(amount*10+0.5) / 10.0 << " " << suffix[i];
            return ss.str();
        }
        amount /= 1000;
    }
    return "More than your mem.";
}

std::vector<std::string> split(const std::string& str, const std::string& delim) {
    assert_msg(!delim.empty(), "Delimiter must not be empty.");
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        tokens.emplace_back(str.substr(prev, pos-prev));
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());
    if (prev == str.length()) tokens.emplace_back("");
    return tokens;
}
std::vector<std::string> split(const std::string& str, char delim) {
    return split(str, std::string(1, delim));
}

std::string join(const std::vector<std::string>& parts, const std::string& joiner) {
    if (parts.size() == 0) return "";
    std::string result = parts[0];
    int n = parts.size();
    for (int i = 1; i < n; ++i) {
        result += joiner + parts[i];
    }
    return result;
}
std::string join(const std::vector<std::string>& parts, char joiner) {
    return join(parts, std::string(1, joiner));
}

int Timer::addCheckPoint(const std::string& label) {
    assert_msg(std::find(labels.begin(), labels.end(), label) == labels.end(),
               "Label '%s' already exists. Use stopwatch to time repeatedly.", label);
    labels.push_back(label);
    times.push_back(std::chrono::high_resolution_clock::now());
    return times.size() - 1;
}
void Timer::showTimings(std::ostream& os) const {
    assert_msg(labels.size() >= 2,
               "You need at least two checkpoints, but you have %d", times.size());
    size_t M = 0;
    for (const auto& label : labels) M = std::max(M, label.size());
    for (size_t c = 1; c < labels.size(); ++c) {
        os << std::left
           << std::setw(M) << labels[c - 1] << " -- " << std::setw(M) << labels[c]
           << ' ' << std::setw(10) << std::scientific
           << std::chrono::duration<double>(times[c] - times[c-1]).count() << " [s]"
           << std::endl;
    }
    os << std::left << std::setw(2*M+5)
       << "total time " << std::setw(10)  // << std::scientific
       << std::chrono::duration<double>(times.back() - times[0]).count()
       << " [s]" << std::endl;
}

void Timer::showTimings(const std::string& from, const std::string& to,
                        std::ostream& os) const {
    showTimings(getID(from), getID(to), os);
}

void Timer::showTimings(int from, int to, std::ostream& os) const {
    assert_msg(0 <= from && from < size(), "From ID %d out of range [0, %d).", from, size());
    assert_msg(0 <= to && to < size(), "To ID %d out of range [0, %d).", to, size());
    os << std::left << std::setw(20) << labels[from] << " -- "
       << std::setw(20) << labels[to] << std::setw(18)
       << std::chrono::duration<double>(times[to] - times[from]).count()
       << "[s]" << std::endl;
}

Timer::time_type Timer::getTime(int id) const {
    assert_msg(0 <= id && id < size(), "ID %d out of range [0, %d).", id, size());
    return times[id];
}

Timer::time_type Timer::getTime(const std::string& label) const {
    return times[getID(label)];
}
double Timer::getTime(const std::string& from, const std::string& to) const {
    return std::chrono::duration<double>(times[getID(to)] - times[getID(from)]).count();
}
double Timer::getTimeToNow(const std::string& from) const {
    return std::chrono::duration<double>(
            std::chrono::high_resolution_clock::now() - times[getID(from)]).count();
}
int Timer::getID(const std::string& label) const {
    auto it = std::find(labels.begin(), labels.end(), label);
    assert_msg(it != labels.end(), "Label '%s' not found. Available labels: %s.",
               label.c_str(), labels);
    return it - labels.begin();
}
int Timer::size() const { return labels.size(); }
void Timer::clear() {
    times.clear();
    labels.clear();
}
int Timer::stopwatchGetID(const std::string& label) const {
    auto it = std::find(stopwatch_labels.begin(), stopwatch_labels.end(), label);
    assert_msg(it != stopwatch_labels.end(), "Label '%s' not found. Available labels: %s.",
               label.c_str(), stopwatch_labels);
    return it - stopwatch_labels.begin();
}
void Timer::startWatch(const std::string& label) {
    for (int i = 0; i < static_cast<int>(stopwatch_labels.size()); i++) {
        if (stopwatch_labels[i] == label) {
            assert_msg(!currently_running[i],
                       "startWatch() for label '%s' was called more than once"
                               " before stopWatch() call.", label.c_str());
            stopwatch_times[i] = std::chrono::high_resolution_clock::now();
            currently_running[i] = true;
            return;
        }
    }
    // label not found
    stopwatch_labels.push_back(label);
    stopwatch_times.push_back(std::chrono::high_resolution_clock::now());
    cumulative_time.push_back(0.0);
    stopwatch_counts.push_back(0);
    currently_running.push_back(true);
}
void Timer::stopWatch(const std::string& label) {
    int id = stopwatchGetID(label);
    assert_msg(currently_running[id],
               "stopWatch() for label '%s' was called"
                       " more than once before startWatch() call.",
               label.c_str());
    ++stopwatch_counts[id];
    cumulative_time[id] += std::chrono::duration<double>(
            std::chrono::high_resolution_clock::now()
            - stopwatch_times[stopwatchGetID(label)]).count();
    currently_running[id] = false;
}
double Timer::getCumulativeTime(const std::string& label) {
    assert_msg(!currently_running[stopwatchGetID(label)],
               "Stopwatch with label '%s' is still running, `stopWatch()` must be called"
                       " before results can be displayed.", label.c_str());
    return cumulative_time[stopwatchGetID(label)];
}
int Timer::getNumLaps(const std::string& label) {
    assert_msg(!currently_running[stopwatchGetID(label)],
               "Stopwatch with label '%s' is still running, `stopWatch()` must be called"
                       " before results can be displayed.", label.c_str());
    return stopwatch_counts[stopwatchGetID(label)];
}
double Timer::getTimePerLap(const std::string& label) {
    assert_msg(!currently_running[stopwatchGetID(label)],
               "Stopwatch with label '%s' is still running, `stopWatch()` must be called"
                       " before results can be displayed.", label.c_str());
    return getCumulativeTime(label)/static_cast<double>(getNumLaps(label));
}
void Timer::stopwatchClear() {
    stopwatch_times.clear();
    stopwatch_labels.clear();
    stopwatch_counts.clear();
    cumulative_time.clear();
    currently_running.clear();
}
const std::vector<std::string>& Timer::getLabels() const {
    return labels;
}

}  // namespace mm
