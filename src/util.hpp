#ifndef SRC_UTIL_HPP_
#define SRC_UTIL_HPP_

/**
 * @file util.hpp
 * @brief File with useful utilities.
 * @example util_test.cpp
 */

#include "includes.hpp"
#include "types.hpp"
#include "common.hpp"
#ifndef __MIC__  // PCM does not work on MIC
#include "pcm/cpucounters.h"
#endif

namespace mm {

/**
 * @brief Multidimansional clone of matlabs linspace function.
 * Uniformly discretizes cuboid givern with beg and end points.
 * See test_util.hpp for examples. Similar to numpy's linspace and Matlab's meshgrid.
 * @param beg Beggining of a cubiod.
 * @param end Ending of a cuboid.
 * @param counts How many discretization points to use in each dimansion.
 * @param include_boundary Flag whether to include bounddary of a cuboid in a given
 * dimension.
 * @return Uniform discretization points of a domain with counts[i] points in dimension i.
 * Instead of returning a n-dim matrix it returns it as a Range instead.
 */
template <class scalar_t, int dim>
Range<Vec<scalar_t, dim>> linspace(const Vec<scalar_t, dim>& beg, const Vec<scalar_t, dim>& end,
                                   const Vec<int, dim>& counts,
const Vec<bool, dim> include_boundary = true) {
    Range<Vec<scalar_t, dim>> ret;
    for (int i = 0; i < dim; ++i) {
        if (include_boundary[i]) assert(counts[i] >= 2);
        else assert(counts[i] >= 0);
        if (counts[i] == 0) return ret;
    }
    Vec<scalar_t, dim> step;
    for (int i = 0; i < dim; ++i) {
        if (include_boundary[i]) step[i] = (end[i] - beg[i]) / (counts[i] - 1);
        else step[i] = (end[i] - beg[i]) / (counts[i] + 1);
    }
    Vec<int, dim> counter = 0;
    do {
        Vec<scalar_t, dim> tmp;
        for (int i = 0; i < dim; ++i) {
            if (include_boundary[i]) tmp[i] = beg[i] + counter[i] * step[i];
            else tmp[i] = beg[i] + (counter[i] + 1) * step[i];
        }
        ret.push_back(tmp);
    } while (incrementCounter(counter, counts));
    return ret;
}

/// Overload for bool argument of include_boundary.
template <class scalar_t, int dim>
Range<Vec<scalar_t, dim>> linspace(const Vec<scalar_t, dim>& beg, const Vec<scalar_t, dim>& end,
const Vec<int, dim>& counts, bool include_boundary) {
    return linspace(beg, end, counts, Vec<bool, dim>(include_boundary));
}

/// Overload for 1 dimension.
Range<double> linspace(double beg, double end, int count, bool include_boundary = true);

/**
 * @brief Increments a multidimanional counter with given limits.
 * @param counter A valid counter state
 * @param limit Limit for each dimanesion of a counter.
 * @return true if an increment was performed and false otherwise.
 * Repeated application of this function to counter with initial state
 * 0 0 0 and limits 1 2 3 yields a sequence:
 * 0 0 0 -> true
 * 0 0 1 -> true
 * 0 0 2 -> true
 * 0 1 0 -> true
 * 0 1 1 -> true
 * 0 1 2 -> true
 * 0 1 2 -> false
 */
template <int dim>
bool incrementCounter(Vec<int, dim>& counter, const Vec<int, dim>& limit) {
    for (int i = dim - 1; i >= 0; --i) {
        if (counter[i] >= limit[i] - 1) {
            counter[i] = 0;
        } else {
            counter[i]++;
            return true;
        }
    }
    return false;
}

/// overload for custom generator
template <typename T, typename URNG>
T random_choice(const Range<T>& elements, const Range<double>& weights, bool normed,
                URNG& generator) {
    Range<double> actual_weights = weights;
    if (actual_weights.empty()) {
        normed = true;
        actual_weights.assign(elements.size(), 1.0 / elements.size());
    }
    assert(actual_weights.size() == elements.size() &&
           "Weights not specified for all elements");
    if (!normed) {  // must be 0.0 so that the inferred type is double
        double sum = std::accumulate(actual_weights.begin(), actual_weights.end(), 0.0);
        for (auto& w : actual_weights) w /= sum;
    }
    std::uniform_real_distribution<double> uniform(0, 1);
    double x = uniform(generator);
    for (int i = 0; i < elements.size(); ++i) {
        if (x < actual_weights[i]) return elements[i];
        x -= actual_weights[i];
    }
    assert(!"Should never get here");
    return elements[0];
}

/**
 * Randomly returns one of the specified elements with distribution according to given
 * weights. A mt19937 generator is created and seeded randomly with call to get_seed.
 * @param elements A pool of elements to choose from.
 * @param weights Weights of the elements. If this argument is omitted all elements are
 * assigned the same weights.
 * @param normed Boolean indicating that the weights are already normed and that
 * additional
 * computation is not necessary. <b>Using this flag with non-normed weights can cause the
 * function to
 * crash (failed assert).</b>
 * @sa get_seed
 */
template <typename T>
T random_choice(const Range<T>& elements, const Range<double>& weights = {},
                bool normed = false) {
    std::mt19937 generator(get_seed());
    return random_choice(elements, weights, normed, generator);
}

#ifndef __MIC__
/**
 * Class to monitor time and CPU counters.
 *
 * This is a wrapper around IntelPCM class.
 * Refer <a href="http://intel-pcm-api-documentation.github.io/classPCM.html">
 * here</a> for their documentation.
 */
class Monitor : public Timer {
    PCM* m;  ///< PCM singleton instance
    std::vector<SystemCounterState> counter_states;  ///< list of recorded CPU states
    std::vector<int> ids;  ///< list of Timer ID's
  public:
    /// Creates PCM monitor
    Monitor();
    /// Cleans up PCM monitor
    ~Monitor();
    /// Adds check point with current time and CPU counter state
    int addCheckPoint(const std::string& label);
    /// Get state at certain checkpoint with given label
    SystemCounterState getState(const std::string& label) const;
    /// Get state at certain checkpoint with given ID
    SystemCounterState getState(int id) const;
    /// Shows given statistics between two checkpoints with given labels
    template <typename Func>
    void showStatistics(const std::string& from, const std::string& to,
                        Func stat, std::ostream& os = std::cout) {
        showStatistics(getID(from), getID(to), stat, os);
    }
    /** Shows given statistics between two checkpoints with given ID's
     *
     * Given function can be any of IntelPCM monitor get* functions, for example
     * <a href="http://intel-pcm-api-documentation.github.io/cpucounters_8h.html#a7b03f0cd02862716d717e897032b7885">
     * getL3CacheHitRatio</a>.
     */
    template <typename Func>
    void showStatistics(int from, int to, Func stat, std::ostream& os = std::cout) {
        assert(0 <= from && from < size() && "Invalid from ID.");
        assert(0 <= to && to <  size() && "Invalid to ID.");
        int idfrom = getStateID(from);
        int idto = getStateID(to);
        os << std::left << std::setw(20) << labels[from] << " -- "
           << std::setw(20) << labels[to] << std::setw(18)
           << ": " << stat(counter_states[idfrom], counter_states[idto])
           << std::endl;
    }
    /// Get L2 cache ratio between two states
    double getL2CacheHitRatio(const std::string& from, const std::string& to);
    /// Get L3 cache ratio between two states
    double getL3CacheHitRatio(const std::string& from, const std::string& to);
    /// Shows timing and L3 cache hit ratio between checkpoints with given labels
    void showTimingAndCacheRatios(const std::string& from, const std::string& to,
                                  std::ostream& os = std::cout) const;
    /// Shows timing and L3 cache hit ratio between checkpoints with given ID's
    void showTimingAndCacheRatios(int from, int to, std::ostream& os = std::cout) const;
    /// Clear all measurements
    void clear();
  private:
    /// Convert timer ID to our ID
    int getStateID(int id) const;
};

#endif  // ifndef __MIC__

/**
 * Transforms nD vector stored in 1D Range<scal> into Range<Vec<scal, nD>>.
 */
template<typename vec_t>
void reshape_vec(const Range<double>& input, Range<vec_t>& out) {
    static const int dim = vec_t::dimension;
    out.clear();
    assert(input.size() % dim == 0 &&
           "Reshaping vector should have size dim*N, check dimensions of inputs.");
    int N = input.size() / dim;
    for (auto i = 0; i < input.size() / dim; ++i) {
        vec_t tmp;
        for (auto j = 0; j < dim; ++j) {
            tmp[j] = input[i+j*N];
        }
        out.push_back(tmp);
    }
}

}  // namespace mm

#endif  // SRC_UTIL_HPP_
