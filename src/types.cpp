#include "types.hpp"

namespace mm {
Range<double> reshape(const Eigen::VectorXd& input) {
    int N = input.size();
    Range<double> ret;
    for (int n = 0; n < N; ++n) ret.push_back(input[n]);
    return ret;
}
}
