#ifndef SRC_SHAPE_STORAGE_HPP_
#define SRC_SHAPE_STORAGE_HPP_

#include "includes.hpp"
#include "types.hpp"
#include "common.hpp"

namespace mm {

/**
 * @file shape_storage.hpp
 * This file contains facilities to compute and store shape functions efficiently.
 *
 * Shape storage concept:
 * ...
 */

/**
 * Namespace holding MLSM related things, like masks for shapes.
 */
namespace mlsm {

/**
 * Type representing flags for shape functions. It has to be an integral type and combinable
 * with bitwise or operator `|` indicating flag union.
 */
typedef int shape_flags;
static const shape_flags d1 = 1;  ///< Indicates to calculate d1 shapes = 0b00000001
static const shape_flags lap = 2;  ///< Indicates to calculate laplace shapes = 0b00000010
static const shape_flags d2 = 4;  ///< Indicates to calculate d2 shapes = 0b00000100
static const shape_flags div = d1;  ///< Indicates to prepare all shapes needed for div
static const shape_flags grad = d1;  ///< Indicates to prepare all shapes needed for div
static const shape_flags graddiv = d2;  ///< Indicates to prepare all shapes needed for graddiv
static const shape_flags all = d1 | d2 | lap;  ///< Indicates to prepare all shapes, this is default

}  // namespace mlsm

/**
 * @brief Efficiently stores requested shape functions.
 * @details It contains an approximation engine, typically MLS,
 * and to a domain engine. All types are degraded to std::vector
 * for optimal performance (<a href="tech_report.pdf"> check technical report
 * </a>). Note that: domain positions and support indexes are copied to local
 * std::vector for better cache utilization (in Domain class 2D containers are
 * used, i.e. Range<Range<int>>).
 * @tparam approx_t A copy-constructible EngineMLS like engine used to create the shape
 * functions.
 * @tparam domain_t A domain class storing the nodes of the domain and their support.
 * @tparam mask A bitmask indicating which shapes to use. The masks for basic shapes are
 * located in mlsm namespace. These are mlsm::d1, mlsm::d2 and mlsm::lap.
 * If you want to solve a Laplace equation with Neumann BC, you would create a class with
 * the shape `mlsm::d1 | mlsm::lap`. The default is mlsm::all which prepares all shapes.
 *
 * If you try to call a function that needs a shape that was not specified for preparation
 * by your flags, you will get a *compile time* error like:
 * @code
 * Error: static assertion failed: D2 shapes must be initialized to use this function.
 * Check your shape bitmask.
 * @endcode
 *
 * @sa mlsm
 */
template <typename domain_t, typename approx_t, mlsm::shape_flags shape_mask = mlsm::all>
class UniformShapeStorage {
  public:
    static_assert(shape_mask != 0, "Cannot create an operator with no shapes.");
    /// Bitmask telling us which shapes to create.
    static const mlsm::shape_flags mask = shape_mask;
    typedef typename domain_t::vector_t vec_t;  ///< Vector type used.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type used.
    /// Dimensionality of the domain.
    enum { dim = vec_t::dimension };
  private:
    /// Const reference to the domain.
    std::reference_wrapper<const domain_t> domain;
    /// Copy of approximation engine.
    approx_t approx;

    /// Support size.
    int support_size_;
    /// Domain size.
    int domain_size_;
    /// Weight shape deduced from the weight function object, used in non-const weight shapes.
    vec_t initial_weight_shape = static_cast<scalar_t>(0.);
    /// Basis shape deduced from the weight function object, used in non-const basis shapes.
    vec_t initial_basis_shape = static_cast<scalar_t>(0.);
    /// Shape container for laplace operator.
    Range<scalar_t> shape_laplace_;
    /// Local copy of support domains.
    Range<int> support_domain;
    /// Shape container for first derivatives.
    Range<scalar_t> shape_d1_;
    /** shape container for second derivatives. Access shape for
     *  @f[
     *    \left. \frac{\partial^2}{\partial x_i \partial x_j}\right|_n \ ,
     *    \ \ \ \ i \leq j
     *  @f]
     *  derivation by starting take the following elements
     *  @f[
     *    \mathrm{support\_size}\,\left(
     *      \frac{1}{2}\mathrm{dim}\, (\mathrm{dim}+1)\,\mathrm{node} +
     *      \frac{1}{2} j \, (j+1) + i
     *      \right) + k \ ,
     *    \ \ \ \ k=0,1, \dots, \mathrm{support\_size}-1
     *  @f]
     */
    Range<scalar_t> shape_d2_;
  public:
    /**
     * @brief Constructor initializing specified shape functions.
     * @param domain_ Domain of the calculation.
     * @param approx_ Approximation engine, usually MLS.
     * @param ind List of indices of nodes scheduled to prepare operators.
     * @param use_const_shape If true, MLS will use constant shape for basis and weight functions.
     * If `false`, weight will be multiplied with a distance to the closest support node (dx). In
     * other words, if you use `use_const_shape`, supply a normalized weight, i.e. `sigma*dx`, and
     * if you use `!use_const_shape`, supply only shape parameter and it will be automatically
     * normalized with `dx`.
     **/
    UniformShapeStorage(const domain_t& domain_, const approx_t& approx_, const Range<int>& ind,
                 bool use_const_shape) : domain(domain_), approx(approx_) {
        domain_size_ = domain.get().size();
        assert_msg(domain.get().support.size() == domain_size_, "domain.support.size = %d and "
                "domain.size = %d, but should be the same. Did you forget to find support before "
                "computing shapes?", domain.get().support.size(), domain.get().size());
        assert_msg(!ind.empty(), "Collection of indexes must be nonempty.");
        for (auto& c : ind) {
            assert_msg(0 <= c && c < domain_size_, "Index %d is not a valid index of a point in "
                    "the domain, must be in range [%d, %d).", c, 0, domain_size_);
            assert_msg(!domain.get().support[c].empty(), "Node %d has empty support! Did you"
                    " forget to find support before computing shapes?", c);
        }
        support_size_ = domain.get().support[ind[0]].size();
        for (auto& c : ind) {
            assert_msg(support_size_ == domain.get().support[c].size(), "Support sized of the "
                    "given indexes are not all the same, support[%d].size = %d and support[%d].size"
                    " = %d.", ind[0], support_size_, c, domain.get().support[c].size());
        }
        // Fills support with -1, to indicate which nodes have their shapes computed.
        support_domain.resize(domain_size_ * support_size_, -1);

        // checks flags
        if (shape_mask & mlsm::d1) {
            shape_d1_.resize(domain_size_ * dim * support_size_, 0);
        }
        if (shape_mask & mlsm::lap) {
            shape_laplace_.resize(domain_size_ * support_size_, 0);
        }
        if (shape_mask & mlsm::d2) {
            shape_d2_.resize(domain_size_ * dim * (dim + 1) * support_size_ / 2, 0);
        }

        if (!use_const_shape) {
            initial_weight_shape = approx.getWeightShape();
            initial_basis_shape = approx.getBasisShape();
        }

        // construct shape functions for every point specified
        int cc;
        // create local copies of mls for each thread
        #if !defined(_OPENMP)
        approx_t& local_approx = approx;
        #else
        std::vector<approx_t> approx_copies(omp_get_max_threads(), approx);
        #pragma omp parallel for private(cc) schedule(static)
        #endif
        for (cc = 0; cc < ind.size(); ++cc) {
            int c = ind[cc];
            //  store local copy of MLS engine -- for parallel computing
            #if defined(_OPENMP)
            approx_t& local_approx = approx_copies[omp_get_thread_num()];
            #endif
            // preps local 1D support domain vector (cache friendly storage)
            for (int j = 0; j < support_size_; ++j) {
                support_domain[c * support_size_ + j] = domain.get().support[c][j];
            }
            // resets local mls with local parameters
            Range<vec_t> supp_domain = domain.get().positions[domain.get().support[c]];
            local_approx.setSupport(supp_domain);
            // note that all weight shapes are in squared form --
            // distances[1] is a squared distance to first supp. node
            if (!use_const_shape) {
                scalar_t dx = std::sqrt(domain.get().distances[c][1]);
                local_approx.resetWeightShape(initial_weight_shape * dx);
                local_approx.resetBasisShape(initial_basis_shape * dx);
            }

            // Laplace
            if (shape_mask & mlsm::lap) {
                for (int d = 0; d < dim; ++d) {
                    std::array<int, dim> derivative;
                    derivative.fill(0);
                    derivative[d] = 2;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    for (int j = 0; j < support_size_; ++j) {
                        shape_laplace_[c * support_size_ + j] += sh[j];
                    }
                }
            }

            std::array<int, dim> derivative;
            // all first derivatives
            if (shape_mask & mlsm::d1) {
                for (int d = 0; d < dim; ++d) {
                    derivative.fill(0);
                    derivative[d] = 1;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    for (int j = 0; j < support_size_; ++j) {
                        shape_d1_[dim * support_size_ * c + support_size_ * d + j] = sh[j];
                    }
                }
            }
            // All second order derivatives
            if (shape_mask & mlsm::d2) {
                for (int d1 = 0; d1 < dim; ++d1) {
                    for (int d2 = 0; d2 <= d1; ++d2) {
                        derivative.fill(0);
                        derivative[d1] += 1;
                        derivative[d2] += 1;
                        auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                        int idx = dim * (dim + 1) * c / 2 + (d1 * (d1 + 1) / 2 + d2);
                        for (int j = 0; j < support_size_; ++j) {
                            shape_d2_[idx * support_size_ + j] = sh[j];
                        }
                    }
                }
            }
        }
    }

    /// Returns number of nodes.
    int size() const {
        return domain_size_;
    }

    /**
     * Returns index of `j`-th neighbour of `node`-th node, ie. `support[node][j]`,
     * but possibly faster.
     */
    int support(int node, int j) const {
        return support_domain[node * support_size_ + j];
    }

    /// Returns support size of i-th node.
    int support_size(int /* node */) const {
        return support_size_;
    }

    /// Returns a vector of support sizes for each node, useful for matrix space prealocation.
    Range<int> support_sizes() const {
        return Range<int>(domain_size_, support_size_);
    }


    /**
     * Return shape coefficient for `j`-th neighbour of `node`-th node, ie.
     * `shape_laplace[node][j]`, but possibly faster.
     */
    scalar_t laplace(int node, int j) const {
        static_assert(mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your shape bitmask.");
        return shape_laplace_[node * support_size_ + j];
    }

    /// Return `j`-th shape coefficient for derivative wrt. variable `var` in `node`.
    scalar_t d1(int var, int node, int j) const {
        static_assert(mask & mlsm::d1, "D1 shapes must be initialized to use this function."
                      " Check your shape bitmask.");
        assert_msg(0 <= var && var < dim,
                   "Variable index %d out of bounds [%d, %d).", var, 0, dim);
        return shape_d1_[dim * support_size_ * node + var * support_size_ + j];
    }

    /**
     * Return `j`-th shape coefficient for mixed derivative of variables `varmin` and `varmax` in
     * `node`.
     */
    scalar_t d2(int varmin, int varmax, int node, int j) const {
        static_assert(mask & mlsm::d2, "D2 shapes must be initialized to use this "
                      "function. Check your shape bitmask.");
        assert_msg(0 <= varmin && varmax < dim,
                   "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
        assert_msg(0 <= varmax && varmax < dim,
                   "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
        assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
                   varmin, varmax);
        int idx = dim * (dim+1)/2 * node + (varmax*(varmax+1)/2 + varmin);
        return shape_d2_[idx * support_size_ + j];
    }


    template <typename D, typename A, int M>
    friend std::ostream& operator<<(std::ostream& os, const UniformShapeStorage<D, A, M>& shapes);
};


/**
 * Prints data about this mlsm setup to stdout.
 * @param os Stream to print to.
 * @param shapes Shapes collection to print.
 */
template <typename D, typename A, int M>
std::ostream& operator<<(std::ostream& os, const UniformShapeStorage<D, A, M>& shapes) {
    std::string shapes_str = "";
    if (M & mlsm::d1) shapes_str += " d1";
    if (M & mlsm::lap) shapes_str += " lap";
    if (M & mlsm::d2) shapes_str += " d2";

    size_t mem = sizeof(shapes);
    size_t mem_d1 = mem_used(shapes.shape_d1_);
    size_t mem_d2 = mem_used(shapes.shape_d2_);
    size_t mem_lap = mem_used(shapes.shape_laplace_);
    size_t mem_supp = mem_used(shapes.support_domain);

    os << "UniformShapeStorage:\n"
       << "    type: " << typeid(shapes).name() << '\n'
       << "    dimension: " << shapes.dim << '\n'
       << "    domain size: " << shapes.size() << '\n'
        << "    mask = " << std::bitset<8>(M) << '\n'
       << "    mem used total: " << mem2str(mem + mem_d1 + mem_d2 + mem_lap + mem_supp) << '\n'
       << "        d1:   " << mem2str(mem_d1) << '\n'
       << "        d2:   " << mem2str(mem_d2) << '\n'
       << "        lap:  " << mem2str(mem_lap) << '\n'
       << "        supp: " << mem2str(mem_supp) << '\n'
       << '\n' << shapes.domain.get() << '\n' << shapes.approx;
    return os;
}

/**
 * Returns an object efficiently storing requested shape functions.
 * @param domain Reference to the domain engine.
 * @param approx Approximation engine.
 * @param ind range of nodes, in terms of indices, where the operators are prepared. If empty,
 * operators are prepared in all nodes, which is the default.
 * @param use_const_shape Defines either to use constant shapes or to use dynamic scaling.
 * Read more in shape construction documentation.
 * @tparam mask Bitmask indicating which flags to create.
 * @tparam approx_t Type of approximation engine.
 * @tparam domain_t Type of domain engine.
 * @warning Template parameters have a different order than in class MLSM, to support
 * calls in form `make_shapes<mlsm::lap>(domain, engine, range)`.
 * @sa UniformShapeStorage::UniformShapeStorage
 */
template<mlsm::shape_flags mask = mlsm::all, typename domain_t, typename approx_t>
UniformShapeStorage<domain_t, approx_t, mask> make_shapes(const domain_t& domain,
        const approx_t& approx, Range<int> ind = {}, bool use_const_shape = true) {
    if (ind.empty()) ind = domain.types != 0;
    return UniformShapeStorage<domain_t, approx_t, mask>(
            domain, approx, ind, use_const_shape);
}


/**
 * @brief Efficiently stores requested shape functions.
 * @details It contains an approximation engine, typically MLS,
 * and to a domain engine. All types are degraded to std::vector
 * for optimal performance (<a href="tech_report.pdf"> check technical report
 * </a>). Note that: domain positions and support indexes are copied to local
 * std::vector for better cache utilization (in Domain class 2D containers are
 * used, i.e. Range<Range<int>>).
 * @tparam approx_t A copy-constructible EngineMLS like engine used to create the shape
 * functions.
 * @tparam domain_t A domain class storing the nodes of the domain and their support.
 * @tparam mask A bitmask indicating which shapes to use. The masks for basic shapes are
 * located in mlsm namespace. These are mlsm::d1, mlsm::d2 and mlsm::lap.
 * If you want to solve a Laplace equation with Neumann BC, you would create a class with
 * the shape `mlsm::d1 | mlsm::lap`. The default is mlsm::all which prepares all shapes.
 *
 * If you try to call a function that needs a shape that was not specified for preparation
 * by your flags, you will get a *compile time* error like:
 * @code
 * Error: static assertion failed: D2 shapes must be initialized to use this function.
 * Check your shape bitmask.
 * @endcode
 *
 * @sa mlsm
 */
template <typename domain_t, typename approx_t, mlsm::shape_flags shape_mask = mlsm::all>
class RaggedShapeStorage {
  public:
    static_assert(shape_mask != 0, "Cannot create an operator with no shapes.");
    /// Bitmask telling us which shapes to create.
    static const mlsm::shape_flags mask = shape_mask;
    typedef typename domain_t::vector_t vec_t;  ///< Vector type used.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type used.
    /// Dimensionality of the domain.
    enum { dim = vec_t::dimension };
  private:
    /// Const reference to the domain.
    std::reference_wrapper<const domain_t> domain;
    /// Copy of approximation engine.
    approx_t approx;

    /// Support sizes.
    Range<int> support_sizes_;
    /// Indexes of starts of supports. Cumulative sums of `support_sizes_`.
    Range<int> support_starts_;
    /// Number of nodes in the domain.
    int domain_size_;
    /// Sum of all support sizes.
    int total_size_;
    /// Weight shape deduced from the weight function object, used in non-const weight shapes.
    vec_t initial_weight_shape = static_cast<scalar_t>(0.);
    /// Basis shape deduced from the weight function object, used in non-const basis shapes.
    vec_t initial_basis_shape = static_cast<scalar_t>(0.);
    /// Shape container for laplace operator.
    Range<scalar_t> shape_laplace_;
    /// Local copy of support domains.
    Range<int> support_domain;
    /// Shape container for first derivatives.
    Range<scalar_t> shape_d1_;
    /** Shape container for second derivatives. Access shape for
     *  @f[
     *    \left. \frac{\partial^2}{\partial x_i \partial x_j}\right|_n \ ,
     *    \ \ \ \ i \leq j
     *  @f]
     *  derivation by starting take the following elements
     *  @f[
     *    \mathrm{support\_size}\,\left(
     *      \frac{1}{2}\mathrm{dim}\, (\mathrm{dim}+1)\,\mathrm{node} +
     *      \frac{1}{2} j \, (j+1) + i
     *      \right) + k \ ,
     *    \ \ \ \ k=0,1, \dots, \mathrm{support\_size}-1
     *  @f]
     */
    Range<scalar_t> shape_d2_;
  public:
    /**
     * @brief Constructor initializing specified shape functions.
     * @param domain_ Domain of the calculation.
     * @param approx_ Approximation engine, usually MLS.
     * @param ind List of indices of nodes scheduled to prepare operators.
     * @param use_const_shape If true MLS will use constant shape for basis and weight functions, if
     * false weight will be multiplied with a distance to the closest support node (dx), in another
     * words If you use `use_const_shape` supply a normalized weight, i.e. `sigma*dx`. if you use
     * `!use_const_shape` supply only shape parameter and it will be automatically normalized with
     * `dx`
     **/
    RaggedShapeStorage(const domain_t& domain_, const approx_t& approx_,
                       const Range<int>& ind, bool use_const_shape) :
            domain(domain_), approx(approx_) {
        domain_size_ = domain.get().size();
        assert_msg(domain.get().support.size() == domain_size_, "domain.support.size = %d and "
                "domain.size = %d, but should be the same. Did you forget to find support before "
                "computing shapes?", domain.get().support.size(), domain.get().size());
        assert_msg(!ind.empty(), "Collection of indexes must be nonempty.");
        for (auto& c : ind) {
            assert_msg(0 <= c && c < domain_size_, "Index %d is not a valid index of a point in "
                    "the domain, must be in range [%d, %d).", c, 0, domain_size_);
            assert_msg(!domain.get().support[c].empty(), "Node %d has empty support! Did you"
                    " forget to find support before computing shapes?", c);
        }
        support_starts_.resize(domain_size_, 0);
        support_sizes_.resize(domain_size_, 0);
        support_sizes_[0] = domain.get().support[0].size();
        for (int i = 1; i < domain_size_; ++i) {
            support_sizes_[i] = domain.get().support[i].size();
            support_starts_[i] = support_starts_[i-1] + support_sizes_[i-1];
        }
        total_size_ = support_starts_[domain_size_-1] + support_sizes_[domain_size_-1];

        // Fills support with -1, to indicate which nodes have their shapes computed.
        support_domain.resize(total_size_, -1);

        // checks flags
        if (shape_mask & mlsm::d1) {
            shape_d1_.resize(dim * total_size_, 0);
        }
        if (shape_mask & mlsm::lap) {
            shape_laplace_.resize(total_size_, 0);
        }
        if (shape_mask & mlsm::d2) {
            shape_d2_.resize(dim * (dim + 1) * total_size_ / 2, 0);
        }

        if (!use_const_shape) {
            initial_weight_shape = approx.getWeightShape();
            initial_basis_shape = approx.getBasisShape();
        }

        // construct shape functions for every point specified
        int cc;
        // create local copies of mls for each thread
        #if !defined(_OPENMP)
        approx_t& local_approx = approx;
        #else
        std::vector<approx_t> approx_copies(omp_get_max_threads(), approx);
        #pragma omp parallel for private(cc) schedule(static)
        #endif
        for (cc = 0; cc < ind.size(); ++cc) {
            int c = ind[cc];
            #if defined(_OPENMP)
            //  store local copy of MLS engine -- for parallel computing
            approx_t& local_approx = approx_copies[omp_get_thread_num()];
            #endif
            // preps local 1D support domain vector (cache friendly storage)
            for (int j = 0; j < support_sizes_[c]; ++j) {
                support_domain[support_starts_[c] + j] = domain.get().support[c][j];
            }
            // resets local mls with local parameters
            Range<vec_t> supp_domain = domain.get().positions[domain.get().support[c]];
            local_approx.setSupport(supp_domain);
            // note that all weight shapes are in squared form --
            // distances[1] is a squared distance to first supp. node
            if (!use_const_shape) {
                scalar_t dx = std::sqrt(domain.get().distances[c][1]);
                local_approx.resetWeightShape(initial_weight_shape * dx);
                local_approx.resetBasisShape(initial_basis_shape * dx);
            }

            // Laplace
            if (shape_mask & mlsm::lap) {
                for (int d = 0; d < dim; ++d) {
                    std::array<int, dim> derivative;
                    derivative.fill(0);
                    derivative[d] = 2;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    for (int j = 0; j < support_sizes_[c]; ++j) {
                        shape_laplace_[support_starts_[c] + j] += sh[j];
                    }
                }
            }

            std::array<int, dim> derivative;
            // all first derivatives
            if (shape_mask & mlsm::d1) {
                for (int d = 0; d < dim; ++d) {
                    derivative.fill(0);
                    derivative[d] = 1;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    for (int j = 0; j < support_sizes_[c]; ++j) {
                        shape_d1_[dim * support_starts_[c] + d * support_sizes_[c] + j] = sh[j];
                    }
                }
            }
            // All second order derivatives
            if (shape_mask & mlsm::d2) {
                for (int d1 = 0; d1 < dim; ++d1) {
                    for (int d2 = 0; d2 <= d1; ++d2) {
                        derivative.fill(0);
                        derivative[d1] += 1;
                        derivative[d2] += 1;
                        auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                        int node_idx = dim * (dim + 1)/ 2 * support_starts_[c];
                        int dim_idx = (d1 * (d1 + 1) / 2 + d2);
                        for (int j = 0; j < support_sizes_[c]; ++j) {
                            shape_d2_[node_idx + dim_idx * support_sizes_[c] + j] = sh[j];
                        }
                    }
                }
            }
        }
    }

    /// Returns number of nodes.
    int size() const {
        return domain_size_;
    }

    /**
     * Returns index of `j`-th neighbour of `node`-th node, ie. `support[node][j]`,
     * but possibly faster.
     */
    int support(int node, int j) const {
        return support_domain[support_starts_[node] + j];
    }

    /// Returns support size of `node`-th node.
    int support_size(int node) const {
        return support_sizes_[node];
    }

    /// Returns a vector of support sizes for each node, useful for matrix space prealocation.
    Range<int> support_sizes() const {
        return support_sizes_;
    }


    /**
     * Return shape coefficient for `j`-th neighbour of `node`-th node, ie.
     * `shape_laplace[node][j]`, but possibly faster.
     */
    scalar_t laplace(int node, int j) const {
        static_assert(mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your shape bitmask.");
        return shape_laplace_[support_starts_[node] + j];
    }

    /// Return `j`-th shape coefficient for derivative wrt. variable `var` in `node`.
    scalar_t d1(int var, int node, int j) const {
        static_assert(mask & mlsm::d1, "D1 shapes must be initialized to use this function."
                " Check your shape bitmask.");
        assert_msg(0 <= var && var < dim,
                   "Variable index %d out of bounds [%d, %d).", var, 0, dim);
        return shape_d1_[dim * support_starts_[node] + var * support_sizes_[node] + j];
    }

    /**
     * Return `j`-th shape coefficient for mixed derivative of variables `varmin` and `varmax` in
     * `node`.
     */
    scalar_t d2(int varmin, int varmax, int node, int j) const {
        static_assert(mask & mlsm::d2, "D2 shapes must be initialized to use this "
                "function. Check your shape bitmask.");
        assert_msg(0 <= varmin && varmax < dim,
                   "Variable varmin %d out of bounds [%d, %d).", varmin, 0, dim);
        assert_msg(0 <= varmax && varmax < dim,
                   "Variable varmax %d out of bounds [%d, %d).", varmax, 0, dim);
        assert_msg(varmin <= varmax, "Varmin (%d) must be smaller than varmax (%d).",
                   varmin, varmax);
        int node_idx = dim * (dim + 1)/ 2 * support_starts_[node];
        int dim_idx = (varmax * (varmax + 1) / 2 + varmin);
        return shape_d2_[node_idx + dim_idx * support_sizes_[node] + j];
    }

    template <typename D, typename A, int M>
    friend std::ostream& operator<<(std::ostream& os, const RaggedShapeStorage<D, A, M>& shapes);
};


/**
 * Prints data about this shape storage, such as memory usage to stdout.
 * @param os Stream to print to.
 * @param shapes Shapes collection to print.
 */
template <typename D, typename A, int M>
std::ostream& operator<<(std::ostream& os, const RaggedShapeStorage<D, A, M>& shapes) {
    std::string shapes_str = "";
    if (M & mlsm::d1) shapes_str += " d1";
    if (M & mlsm::lap) shapes_str += " lap";
    if (M & mlsm::d2) shapes_str += " d2";

    size_t mem = sizeof(shapes);
    size_t mem_d1 = mem_used(shapes.shape_d1_);
    size_t mem_d2 = mem_used(shapes.shape_d2_);
    size_t mem_lap = mem_used(shapes.shape_laplace_);
    size_t mem_supp = mem_used(shapes.support_domain);

    os << "RaggedShapeStorage:\n"
       << "    type: " << typeid(shapes).name() << '\n'
       << "    dimension: " << shapes.dim << '\n'
       << "    domain size: " << shapes.size() << '\n'
       << "    mask = " << std::bitset<8>(M) << '\n'
       << "    mem used total: " << mem2str(mem + mem_d1 + mem_d2 + mem_lap + mem_supp) << '\n'
       << "        d1:   " << mem2str(mem_d1) << '\n'
       << "        d2:   " << mem2str(mem_d2) << '\n'
       << "        lap:  " << mem2str(mem_lap) << '\n'
       << "        supp: " << mem2str(mem_supp) << '\n'
       << '\n' << shapes.domain.get() << '\n' << shapes.approx;
    return os;
}

/**
 * Returns an object efficiently storing requested shape functions.
 * @param domain Reference to the domain engine.
 * @param approx Approximation engine.
 * @param ind range of nodes, in terms of indices, where the operators are prepared. If empty,
 * operators are prepared in all nodes, which is the default.
 * @param use_const_shape Defines either to use constant shapes or to use dynamic scaling
 * read more in MLSM constructor documentation.
 * @tparam mask Bitmask indicating which flags to create.
 * @tparam approx_engine Type of approximation engine.
 * @tparam domain_engine Type of domain engine.
 * @warning Template parameters have a different order than in class MLSM, to support
 * calls in form `make_shapes<mlsm::lap>(domain, engine, range)`.
 */
template<mlsm::shape_flags mask = mlsm::all, typename domain_engine, typename approx_engine>
RaggedShapeStorage<domain_engine, approx_engine, mask> make_ragged_shapes(
        const domain_engine& domain, const approx_engine& approx, Range<int> ind = {},
        bool use_const_shape = true) {
    if (ind.empty()) ind = domain.types != 0;
    return RaggedShapeStorage<domain_engine, approx_engine, mask>(domain, approx, ind,
                                                                  use_const_shape);
}

}  // namespace mm

#endif  // SRC_SHAPE_STORAGE_HPP_
