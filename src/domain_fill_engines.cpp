#include "domain_fill_engines.hpp"

namespace mm {
PoissonDiskSamplingFill::PoissonDiskSamplingFill() {
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::iterations(int iterations) {
    num_iterations = iterations; return *this;
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::proximity_relax(double in) {
    proximity_relaxation = in; return *this;
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::randomize(bool in) {
    use_MC_in_iteration = in; return *this;
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::optim_after(size_t in) {
    optimize_after = in; return *this;
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::internal_type(int in) {
    internal_t = in; return *this;
}

PoissonDiskSamplingFill& PoissonDiskSamplingFill::seed(int in) {
    assert_msg(in > -1, "PDS Fill::seed should be > -1");
    rseed = in; return *this;
}
}  // namespace mm
