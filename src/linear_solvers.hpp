#ifndef SRC_LINEAR_SOLVERS_HPP_
#define SRC_LINEAR_SOLVERS_HPP_

#include "includes.hpp"
#include "types.hpp"

/// @cond
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/SVD"
/// @endcond

namespace mm {

/**
 * @file linear_solvers.hpp
 * @brief A library with wrappers for linear solvers found in Eigen.
 * @example linear_solvers_test.cpp
 */


/**
 * @brief Solves Ax=b
 * @details A base abstract class that defines the signature for all linear
 * solvers.
 *
 * @tparam scalar_t Scalar type used
 */
template<typename scalar_t>
class LinearSolver {
  public:
    /// Eigen matrix type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// Eigen vector type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> ei_vec_t;

    /// Computes the decomposition (if any) for matrix A
    virtual void compute(const ei_matrix_t& A) = 0;
    /// After `compute` it returns such x that Ax=b
    virtual ei_vec_t solve(const ei_vec_t& b) = 0;
    /// Returns condition number of A
    virtual scalar_t getCondNo() const = 0;
    /// Clears the calculated calculations
    virtual void clear() = 0;
    /// Is the decomposition prepared for usage?
    virtual bool ready() const = 0;
};

/**
 * @brief A general wrapper for Eigen linear solvers
 * @details A wrapper to use around Linear solvers like SVD, LU, Cholesky,...
 *
 * @tparam scalar_t Scalar type used
 * @tparam EigenSolver Linear solver from Eigen (e.g. JacobiSVD, FullPivLU,
 * HouseholderQR,...). Note that some need a bit of tweaking. See:
 * https://eigen.tuxfamily.org/dox//group__TutorialLinearAlgebra.html
 */
template<class EigenSolver>
class EigenLinearSolver : LinearSolver<typename EigenSolver::MatrixType::Scalar> {
  public:
    /// Scalar type
    typedef typename EigenSolver::MatrixType::Scalar scalar_t;
    /// Matrix type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// Vector type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> ei_vec_t;
  protected:
    /// This is Eigen's solver class (SVD, LU, Cholesky,..)
    EigenSolver decomposition;
    /// Was the function `compute` already called
    bool decomposition_ready;
    /// Condition number
    scalar_t cond_no;
  public:
    /// Default constructor. Marks `decomposition_ready=false`
    EigenLinearSolver() : decomposition_ready(false) {}
    /**
     * @brief Calculates the decomposition
     * @details Calls Eigen method to calculate the decomposition of A and
     * saves it internally
     *
     * @param A A matrix to decompose
     */
    void virtual compute(const ei_matrix_t& A) {
        decomposition.compute(A);
        decomposition_ready = true;
        cond_no = calcCondNo();
    }
    /**
     * @brief Solve Ax=b
     * @details Find such vector x that Ax=b using the decomposition
     * calculated in the compute step.
     * Note: You must call the compute method before calling this.
     *
     * @param b Right hand side of the equation
     * @return The solution x of Ax=b
     */
    ei_vec_t solve(const ei_vec_t& b) {
        assert(decomposition_ready == true &&
            "You have to call `compute` before you call `solve`");
        return decomposition.solve(b);
    }

    /**
     * @brief Get calculated condition number
     * @details Return the already saved condition number that was calculated
     * during the last call of compute() function
     * @return Condition number of matrix A
     */
    scalar_t getCondNo() const {
        assert(decomposition_ready == true &&
            "You have to call `compute` before you call `get CondNo`");
        return cond_no;
    }

    /**
     * @brief Unvalidates the currently calculated decomposition
     * @details Sets an internal flag `decomposition_ready` to false.
     */
    void clear() {
        decomposition_ready = false;
    }
    /**
     * @brief Tells if the decomposition is ready (if `compute` was called)
     * @return True if decomposition is ready, else False
     */
    bool ready() const {
        return decomposition_ready;
    }

  private:
    /**
     * @brief Detects if the class T::rcond exists
     * @details If class T has a member `rcond` (regardless of its type) the
     * value parameter will be true, or false otherwise
     *
     * @tparam T The class to check
     */
    template<typename T>
    class DetectCond {
        /// Fallback class that definitely has a member `rcond`
        struct Fallback {
            /// Add member name "rcond"
            int rcond;
        };
        /// We will test the type of this class's `rcond` member
        struct Derived : T, Fallback { };
        /// Templated class
        template<typename U, U> struct Check;

        /// typedef for an array of size one.
        typedef char ArrayOfOne[1];
        /// typedef for an array of size two.
        typedef char ArrayOfTwo[2];

        /// This accepts all classes where rcond is derived from Fallback class
        template<typename U>
        static ArrayOfOne & func(Check<int Fallback::*, &U::rcond> *);

        /// This accepts all arguments (the ones where rcond is in T)
        template<typename U>
        static ArrayOfTwo & func(...);

      public:
        /// Self referenced type
        typedef DetectCond type;
        /// Check the return type of func to determine if T has member `rcond`.
        enum { value = sizeof(func<Derived>(0)) == 2 };
    };  // End DetectCond


    /**
     * @brief Calculate condition number
     * @details Using the method .rcond of the decomposition which is supported
     * in eigen classes: FullPivLU, PartialPivLU, LDLT, and LLT. This method
     * only exists if EigenSolver has the method .rcond() available.
     * @return Condition number approximation
     */
    template<typename T = EigenSolver>
    typename std::enable_if<DetectCond<T>::value, scalar_t>::type
    calcCondNo() {
        assert(decomposition_ready == true &&
            "You have to call `compute` before you call `calcCondNo`");
        // Condition support available for: FullPivLU, PartialPivLU, LDLT, and
        // LLT
        scalar_t cond = 1./decomposition.rcond();
        if (std::isnormal(cond)) return cond;
        std::cout << "Warning: There was a problem calculationg condition "
                     "number: cond_no=" << cond << std::endl;
        return -1.;
    }

    /**
     * @brief Placeholder function for calculationg condition number
     * @details Does nothng but print a warning that this function is not yet
     * implemented. This method only exists if EigenSolver does not have the
     * method .rcond() available.
     * @return returns -1
     */
    template<typename T = EigenSolver>
    typename std::enable_if<!DetectCond<T>::value, scalar_t>::type
    calcCondNo() {
        std::cout << "Warning: calcCondNo is not implemented in this solver "
                  << "class because .rcond() method was not detected." << std::endl;
        return -1.;
    }
};

/**
 * @brief Linear solver that uses JabobiSVD to solve linear equations
 *
 * @tparam scalar_t The scalar type to be used in the calculations
 */
template<typename scalar_t>
class SVDLinearSolver : EigenLinearSolver<Eigen::JacobiSVD<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>> {
  public:
    /// Matrix type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// Vector type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> ei_vec_t;
    /// Parent class typedef
    typedef EigenLinearSolver<Eigen::JacobiSVD<ei_matrix_t>> ParentClass;
  protected:
    using ParentClass::decomposition;
    using ParentClass::decomposition_ready;
    using ParentClass::cond_no;
  public:
    using ParentClass::solve;
    using ParentClass::getCondNo;
    using ParentClass::compute;
    using ParentClass::clear;
    using ParentClass::ready;
    /**
     * @brief Calculates SVD decomposition
     * @details Calls Eigen method to calculate SVD decomposition of A and
     * saves it internally
     *
     * @param A A matrix to decompose
     */
    void compute(const ei_matrix_t& A) override {
        decomposition_ready = true;
        decomposition.compute(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
        cond_no = calcCondNo();
    }
  private:
    /**
     * @brief Calulate condition number
     * @details Approximate condition number as a coefficient between the
     * largest and smallest (non-zero) singular values.
     * @return Condition number
     */
    scalar_t calcCondNo(){
        assert(decomposition_ready == true &&
            "You have to call `compute` before you call `calcCondNo`");
        auto singular_vals = decomposition.singularValues();
        scalar_t min_val = singular_vals(0);
        for (scalar_t val : singular_vals) {
            if (std::abs(val) > std::abs(singular_vals(0)*decomposition.threshold())) {
                min_val = val;
            }
        }
        return singular_vals(0)/min_val;
    }
};

/// LU module
/// Linear solver using Eigen's solver FullPivLU
template<typename scalar_t>
using FullPivLULinearSolver = EigenLinearSolver<Eigen::FullPivLU<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

/// Linear solver using Eigen's solver PartialPivLU
template<typename scalar_t>
using PartialPivLULinearSolver = EigenLinearSolver<Eigen::PartialPivLU<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

// Cholesky module
/// Linear solver using Eigen's solver LDLT
template<typename scalar_t>
using LDLTLinearSolver = EigenLinearSolver<Eigen::LDLT<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;
/// Linear solver using Eigen's solver LLT
template<typename scalar_t>
using LLTLinearSolver = EigenLinearSolver<Eigen::LLT<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

// QR module
/// Linear solver using Eigen's solver ColPivHouseholderQR
template<typename scalar_t>
using ColPivHouseholderQRLinearSolver = EigenLinearSolver<Eigen::ColPivHouseholderQR<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

/// Linear solver using Eigen's solver CompleteOrthogonalDecomposition
template<typename scalar_t>
using CompleteOrthogonalDecompositionLinearSolver = EigenLinearSolver<
        Eigen::CompleteOrthogonalDecomposition<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

/// Linear solver using Eigen's solver FullPivHouseholderQR
template<typename scalar_t>
using FullPivHouseholderQRLinearSolver = EigenLinearSolver<Eigen::FullPivHouseholderQR<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;

/// Linear solver using Eigen's solver HouseholderQR
template<typename scalar_t>
using HouseholderQRLinearSolver = EigenLinearSolver<Eigen::HouseholderQR<
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>>>;


}  // namespace mm

#endif  // SRC_LINEAR_SOLVERS_HPP_
