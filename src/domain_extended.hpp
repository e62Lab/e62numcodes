#ifndef SRC_DOMAIN_EXTENDED_HPP_
#define SRC_DOMAIN_EXTENDED_HPP_

#include "common.hpp"
#include "domain.hpp"
#include "kdtree.hpp"
#include "types.hpp"
#include <Eigen/SVD>
#include "domain_support_engines.hpp"

namespace mm {

/**
 * @file
 * @brief File implementing extra functionality for domains.  This is in a separate file,
 * because of its dependencies, which are large and their inclusion may not always be
 * wanted by default.
 * @example domain_extended_test.cpp
 */

/**
 * @brief Finds and sets support of a domain. For each point at `support_size` points
 * closest to it are found and stored in support attribute. Points are stored in non
 * descending fashion according to the distances from the queried points, the first one
 * being the point itself.
 * The squares of distances from point `i` to its support are stored in `distances[i]`
 * array.
 * @param support_size Size of support for each node.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size);
}

/**
 * Find support for a subset of nodes.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size,
                for_which);
}

/**
 * Find support for a subset of nodes, and also looking for support only among
 *`search_among`.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 * @param search_among Indexes of nodes among which to search for support.
 * @param force_self If true each node's support will contain itself regardless,
 * even if it is not in `search_among`.
 *
 * Example: (find support for boundary nodes, but support is made only of internal nodes)
 * @code
 * d.findSupport(1, d.types < 0, d.types > 0);
 * @endcode
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size, for_which,
                search_among, force_self);
}

/**
 * Same as without radius, but only points within the radius are included in support.
 * If radius is infinite, this is equal to the first version.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size) {
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(radius_squared, max_support_size, all_ind);
}

/// Same as without radius, but only points within the radius are included in support.
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which) {
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(KDTree<vec_t>(positions), radius_squared, max_support_size,
                for_which, all_ind);
}

/// Same as without radius, but only points within the radius are included in support.
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which, const Range<int>& search_among,
                                bool force_self) {
    for (int x : search_among) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in "
                "search_among.", x, 0, positions.size());
    }
    findSupport(KDTree<vec_t>(positions[search_among]), radius_squared, max_support_size,
                for_which, search_among, force_self);
}

/**
 * Overload with user provided kdtree. The tree must contain points with indexes listed in
 * `search_among`. The rest of parameters are the same as usual.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(const KDTree<vec_t>& tree, scalar_t radius_squared,
                                int max_support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    assert_msg(!positions.empty(), "Cannot find support in an empty domain.");
    assert_msg(max_support_size > 0, "Support size must be greater than 0, got %d.",
               max_support_size);
    assert_msg(tree.size() == search_among.size(), "Size of indexes to search among must match "
               "the size of the tree, but got %d vs. %d.", search_among.size(), tree.size());
    assert_msg(max_support_size <= tree.size(),
               "Support size (%d) cannot exceed number of points that we are searching among (%d).",
               max_support_size, search_among.size());
    assert_msg(radius_squared > 0, "Support radius must be greater than 0, got %s.",
               radius_squared);
    assert_msg(!for_which.empty(), "Set of nodes for which to find the support is empty.");
    assert_msg(!search_among.empty(), "Set of nodes to search among is empty.");
    for (int x : for_which) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in for_which.",
                   x, 0, positions.size());
    }
    for (int x : search_among) {
        assert_msg(0 <= x && x < positions.size(), "Index %d out of range [%d, %d) in "
                   "search_among.", x, 0, positions.size());
    }

    int n = positions.size();
    support.resize(n);
    distances.resize(n);
    // Only clear those supports that we will re-write
    support[for_which] = Range<int>();
    distances[for_which] = Range<scalar_t>();
    bool is_inf = std::isinf(radius_squared);
    for (int i : for_which) {
        auto res = (is_inf) ? tree.query(positions[i], max_support_size)
                            : tree.query(positions[i], radius_squared, max_support_size);
        support[i].reserve(res.first.size());
        distances[i].reserve(res.first.size());
        if (force_self && (search_among[res.first[0]] != i)) {
            // Add current point to its support
            support[i].push_back(i);
            distances[i].push_back(0);
            // Remove the furthest point from support
            res.first.pop_back();
            res.second.pop_back();
        }
        for (int j : res.first) support[i].push_back(search_among[j]);
        for (scalar_t j : res.second) distances[i].push_back(j);
    }
}

/**
 * Projects `point` to the boundary. First, it does an exponential search for the other side and
 * then uses bisection to find the precise location of the boundary. the returned point lies within
 * `precision` of the analytical boundary. Works in 2D and 3D.
 *
 * @param point Point close to the boundary hinting where to add the boundary point.
 * @param unit_normal An outside unit normal point to the boundary.
 * @param precision The distance of the found point from the boundary will be at most `precision`.
 * @return Whether the addition was successful and the coordinates of the point added, if it was.
 *
 * Example:
 * @snippet domain_extended_test.cpp Add to boundary
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::projectPointToBoundary(
        const vec_t& point, vec_t unit_normal, scalar_t precision) const {
    assert_msg(unit_normal.norm() > 1e-9, "Normal %s given for point %s is zero.",
               unit_normal, point);
    if (vec_t::dimension == 1) return {false, point};
    // Find point on the other side of the boundary with exponential search
    vec_t start = point;
    bool is_inside = contains(start);
    scalar_t max_stretch = characteristicDistance() / 100;
    while (true) {
        if (contains(start + max_stretch * unit_normal) != is_inside) break;
        if (contains(start - max_stretch * unit_normal) != is_inside) {
            max_stretch *= -1;
            break;
        }
        max_stretch *= 2;
        if (std::isinf(max_stretch)) {  // hint is bad
            return {false, vec_t()};
        }
    }

    // Find the point on the boundary using bisection
    scalar_t stretch = max_stretch;
    while (std::abs(stretch) > precision) {
        stretch /= 2;
        if (contains(start + stretch * unit_normal) == is_inside) {
            start += stretch * unit_normal;
        }
    }

    // Make unit normal point outside
    if (is_inside) {
        unit_normal *= signum(max_stretch);
    } else {
        unit_normal *= -signum(max_stretch);
    }

    // Make sure point is inside (it's maybe EPS away)
    while (!contains(start)) start -= precision * unit_normal;

    return {true, start};
}

}  // namespace mm

#endif  // SRC_DOMAIN_EXTENDED_HPP_
