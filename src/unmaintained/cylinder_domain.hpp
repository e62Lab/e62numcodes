/**
 * @brief Class to hold information about 3d cylinder domains.
 * Domain is a vec_t::dimension cylinder between beg and end points.
 * It is only implemented for 3 dimensions -> use only Vec3d as template type.
 * Note that bbox is not well defined for Cylidner3D class. bbox just contains
 * points at the center of starting and ending plate of cylinder.
 */
template <class vec_t>
class Cylinder3D : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    vec_t beg;  ///< Start point of a domain.
    vec_t end;  ///< End point of a domain.
    typename vec_t::scalar_t radius;  ///< Radius of the cylinder
    typename vec_t::scalar_t length;  ///< Length of the cylinder
    vec_t cylinder_direction;  ///< Unit vector in the direction of cylinder coat
    vec_t radius_direction;  ///< A vector perpendicular to cylinder_direction -
    /// lies in the plane of cylinder circle
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;
    /// Allow default constructor
    Cylinder3D<vec_t>() {}
    /// Allow move constructor
    Cylinder3D<vec_t>(Cylinder3D<vec_t>&&) = default;
    /// Allow move assignment
    Cylinder3D<vec_t>& operator=(Cylinder3D<vec_t>&&) = default;
    /// Remove copy constructor
    Cylinder3D<vec_t>(const Cylinder3D<vec_t>&) = delete;
    /// Remove copy assignment
    Cylinder3D<vec_t>& operator=(const Cylinder3D<vec_t>&) = delete;

    /**
     * Creates a cylinder domain. The dimensions of both points must be the same.
     * @param beg_ Point in the middle of a circle at the beginning of the cylinder.
     * @param end_ A point in the middle of a circle at the end of cylinder.
     * @param radius_ Radius of the created cylinder.
     */
    Cylinder3D(const vec_t& beg_, const vec_t& end_, const scalar_t& radius_)
            : beg(beg_), end(end_), radius(radius_) {
        assert(vec_t::dimension == 3 && "Currently Cyllinder3D only works in 3 dimensions.");
        assert(radius_ > 0 && "Radius of a cylinder must be greater than 0.");
        assert(beg_ != end_ && "Starting and ending points of a cylinder should be different.");
        length = 0;
        for (int i = 0; i < vec_t::dimension; i++) length += std::pow(beg[i] - end[i], 2);
        length = std::sqrt(length);
        cylinder_direction = (end - beg) / length;
        radius_direction = vec_t({cylinder_direction[1] + cylinder_direction[2],
                                  -cylinder_direction[0], -cylinder_direction[0]});
        radius_direction = radius_direction / radius_direction.norm();
        calculateBBox();
    }
    /**
     * Overload for unambiguous cylinder3D({1.2, 2.1, 0}, {-2.3, 4.56, 0}, 5);
     */
    Cylinder3D(std::initializer_list<scalar_t> beg, std::initializer_list<scalar_t> end,
               const scalar_t radius) : Cylinder3D<vec_t>(vec_t(beg), vec_t(end), radius) {}

    /**
     * @brief Fills boundary and interior uniformly by supplying node counts.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(const Vec<int, vec_t::dimension>& internal_counts,
                     const Vec<int, vec_t::dimension>& boundary_counts) {
        fillUniformInterior(internal_counts);
        fillUniformBoundary(boundary_counts);
    }
    /**
     * @brief Fills boundary and interior uniformly by supplying spatial step.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniformWithStep(const vec_t& internal_steps, const vec_t& boundary_steps) {
        fillUniformInteriorWithStep(internal_steps);
        fillUniformBoundaryWithStep(boundary_steps);
    }

    /**
     * @brief Uniformly discretizes underlying domain interior with counts[i] points
     * in dimension i. It fills positions with appropriate coordinates and types with 1s.
     * See test_domain.hpp for examples.
     * @param counts how many discretization points to use in each dimension.
     * Dimension are radial direction, angular direction, longitudinal respectively.
     */
    void fillUniformInterior(const Vec<int, vec_t::dimension>& counts) {
        clearInternalNodes();
        scalar_t dfi = 2.0 * M_PI / counts[1];
        scalar_t dr = radius / counts[0];
        scalar_t dl = length / (counts[2] + 1);
        positions.reserve(positions.size() + counts[2] + (counts[0] - 1) * counts[1] * counts[2]);
        // This part is for r!=0. For r=0 radial direction does not matter.
        // angular_direction
        for (int j = 0; j < counts[1]; j++) {
            // Rodrigues formula - check wiki
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(cylinder_direction) * std::sin(dfi * j);
            // radial direction
            for (int i = 1; i < counts[0]; ++i) {
                // longitudinal direction
                for (int k = 1; k <= counts[2]; k++) {
                    positions.push_back(beg + cylinder_direction * dl * k +
                                        radius_direction_rotated * dr * i);
                    types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
                }
            }
        }
        // This part is for r=0. Radial and angular direction does not matter.
        // longitudinal direction
        for (int k = 1; k <= counts[2]; k++) {
            positions.push_back(beg + cylinder_direction * dl * k);
            types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
        }
    }

    /**
     * @brief Uniformly discretizes underlying domain interior by calling fillUniformInterior with
     * appropriate counts.
     * @param steps what discretization step to use in each dimension
     * (radial, angular and longitudinal respectively)
     */
    void fillUniformInteriorWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in interior discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        counts[0] = static_cast<int>(std::ceil(radius / steps[0]));
        counts[1] = static_cast<int>(std::ceil(2.0 * M_PI / steps[1]));
        counts[2] = static_cast<int>(std::ceil(2.0 * M_PI / steps[2])) - 1;
        fillUniformInterior(counts);
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary. It fills positions
     * with appropriate coordinates and types with -1s. Dimensions higher than 3 are
     * currently unsupported.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformBoundary(const Vec<int, vec_t::dimension>& counts) {
        for (auto x : counts) assert(x >= 2 && "All counts must be greater than 2.");
        clearBoundaryNodes();
        double dfi = 2.0 * M_PI / (counts[1]);
        double dr = radius / (counts[0] - 1);
        double dl = length / (counts[2] - 1);
        positions.reserve(positions.size() + 2 + 2 * (counts[0] - 2) * counts[1] +
                          (counts[2]) * counts[1]);
        // this os for the beginning surface of the cylinder - circular end-plate at beginning
        //  angular direction
        for (int j = 0; j < counts[1]; j++) {
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(cylinder_direction) * std::sin(dfi * j);
            // radial direction
            for (int i = 0; i < counts[0] - 1; i++) {
                if (i == 0 && j > 0) continue;
                positions.push_back(beg + dr * i * radius_direction_rotated);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }
        // this is for the ending surface of the cylinder - circular end-plate at the end
        // angular direction
        for (int j = 0; j < counts[1]; j++) {
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(cylinder_direction) * std::sin(dfi * j);
            // radial direction
            for (int i = 0; i < counts[0] - 1; i++) {
                if (i == 0 && j > 0) continue;
                positions.push_back(end + dr * (i)*radius_direction_rotated);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }
        // this is for cylinder coat
        // angular direction
        for (int j = 0; j < counts[1]; j++) {
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(cylinder_direction) * std::sin(dfi * j);
            // longitudinal direction
            for (int i = 0; i < counts[2]; i++) {
                positions.push_back(beg + radius_direction_rotated * radius +
                                    cylinder_direction * dl * i);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary by calling
     * fillUniformBoundary with appropriate counts.
     * @param steps what discretization step to use in each dimension
     * (radial, angular and longitudinal respectively)
     */
    void fillUniformBoundaryWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in boundary discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        counts[0] = static_cast<int>(std::ceil(radius / steps[0])) + 1;
        counts[1] = static_cast<int>(std::ceil(2.0 * M_PI / steps[1]));
        counts[2] = static_cast<int>(std::ceil(length / steps[2])) + 1;
        fillUniformBoundary(counts);
    }
    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }
    /// Randomly fills 3D cylinder interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        positions.reserve(positions.size() + num_of_points);
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            double fi = uniform(generator) * 2.0 * M_PI;
            // randomly rotate radial direction - using Rodrigues formula
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(fi) +
                    radius_direction.cross(cylinder_direction) * std::sin(fi);
            vec_t x = beg + uniform(generator) * radius * radius_direction_rotated +
                      uniform(generator) * length * cylinder_direction;
            positions.push_back(x);
            types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
        }
        guessSetContainsPrecision();
    }

    /// Randomly fills 3D cylinder boundary with points.
    void fillRandomBoundary(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Random point picking of surfaces of cuboids of dimension >=4 is "
                              "currently not supported.");
        clearBoundaryNodes();
        positions.reserve(positions.size() + num_of_points);
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        Range<int> elem = {0, 1, 2};
        Range<scalar_t> weights = {std::abs(radius * radius),  // circular plate at the beginning
                                   std::abs(radius * radius),  // circular plate at the end
                                   std::abs(2.0 * radius * length)};  // coat of cylinder
        for (int i = 0; i < num_of_points; ++i) {
            int choice = random_choice(elem, weights, false, generator);
            scalar_t fi = uniform(generator) * 2 * M_PI;
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(fi) +
                    radius_direction.cross(cylinder_direction) * std::sin(fi);
            vec_t point = uniform(generator) * 2 * M_PI * radius;
            if (choice == 0) {  // circular plate at the beginning
                point = beg + radius_direction_rotated * radius * uniform(generator);
            } else if (choice == 1) {  // circular plate at the end
                point = end + radius_direction_rotated * radius * uniform(generator);
            } else {  // cylinder coat
                point = beg + radius_direction_rotated * radius +
                        cylinder_direction * length * uniform(generator);
            }
            positions.push_back(point);
            types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
        }
        guessSetContainsPrecision();
    }

    scalar_t volume() const override { return M_PI * radius * radius * length; }

    scalar_t surface_area() const override {
        return 2 * M_PI * radius * length + 2 * M_PI * radius * radius;
    }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /**
     * @brief vec_test if a point is contained in a basic domain, paying
     * no notice to subdomains. A point is inside a cuboid if every of
     * its coordinates lies in the interval defined with corresponding
     * begin and end coordinates. An EPS margin pointing away from normal is used.
     * @param point The point we are interested in,
     * @return True if point is conatined in the basic domain
     * and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        // scalar_t margin = contains_precision * thickness * normal_vector;
        vec_t beg_point_vector = point - beg;
        vec_t end_point_vector = point - end;
        double alpha = std::acos(
                cylinder_direction.dot(beg_point_vector) / beg_point_vector.norm());
        double beta = std::acos(
                -cylinder_direction.dot(end_point_vector) / end_point_vector.norm());
        double distance_from_cylinder_axis = std::sin(alpha) * beg_point_vector.norm();
        // in below conditions some numerical offset is needed, because otherwise
        // boundary points are outside the domain
        if (alpha > (M_PI / 2.0) * (1.0 + 1e-10) || beta > (M_PI / 2.0) * (1.0 + 1e-10) ||
            distance_from_cylinder_axis > radius * (1.0 + 1e-10))
            return normal_vector == OUTSIDE;
        return normal_vector == INSIDE;
    }

    /// calculates domain's bounding box
    void calculateBBox() {
        vec_t min, max;
        for (int i = 0; i < vec_t::dimension; i++) {
            if (beg[i] < end[i]) {
                min[i] = beg[i] - radius;
                max[i] = end[i] + radius;
            } else {
                min[i] = end[i] - radius;
                max[i] = beg[i] + radius;
            }
        }
        bbox = std::make_pair(min, max);
    }
    /**
     * Return a new cylinder3D, which is a copy of this one.
     */
    Cylinder3D* clone() const override {
        auto p = new Cylinder3D(beg, end, radius);
        this->clone_base(p);
        p->length = length;
        p->cylinder_direction = cylinder_direction;
        p->radius_direction = radius_direction;
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const Cylinder3D<vec_t>& d) {
    os << "Cylinder3D:\n"
       << "    beg: " << d.beg << "\n"
       << "    end: " << d.end << "\n";
    return d.output_report(os);
}

/**
 * @brief Class to hold information about 3d tube domains.
 * This is rope or tube like domain. It has circular intersection. The path
 * of the "rope/tube" is determined by the user supplied function.
 * It is only implemented for 3 dimensions -> use only Vec3d as template type.
 * Note that bbox is not well defined for Cylidner3D class. bbox just contains
 * points at the center of starting and ending plate of cylinder.
 */
template <class vec_t>
class Tube3D : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;
    scalar_t t_beg;  ///< Starting point of a domain: f(t_beg).
    scalar_t t_end;  ///< Ending point of a domain: f(t_end).
    scalar_t radius;  ///< Radius of a tube
    int characteristic_number;  ///< this number tells the class how many points to use for
    ///< Calculation of area, volume and in the function inside().
    std::function<vec_t(const double&)> f;
    ///< Function which describes the shape of a tube.
    std::function<vec_t(const double&)> df;
    ///< Derivative of above function (tangent to the curve described by f).
    ///< User should enshure that tanget is always nonzero vector, i.e. it must not be (0,0,0)!
    /// Allow default constructor
    Tube3D<vec_t>() {}
    /// Allow move constructor
    Tube3D<vec_t>(Tube3D<vec_t>&&) = default;
    /// Allow move assignment
    Tube3D<vec_t>& operator=(Tube3D<vec_t>&&) = default;
    /// Remove copy constructor
    Tube3D<vec_t>(const Tube3D<vec_t>&) = delete;
    /// Remove copy assignment
    Tube3D<vec_t>& operator=(const Tube3D<vec_t>&) = delete;
    /**
     * Creates a tube domain. The dimensions of both points must be the same.
     * @param t_beg_ Starting value of a parameter t (parameter of a function f).
     * @param t_end_ Ending values of a parameter t (parameter of a function f).
     * @param radius_ radius of a tube/rope.
     * @param characteristic_number_ number should represent useful division
     * of rope into smaller sections.
     * It is used for calculation of volume and area.
     * Usually larger characteristic_number is better for calculation of volume and surface area
     * but adds significant time to newton algorithm -> check method inside()
     * which is used for e.g. when subtracting a domain
     * Use 100-500 and only if this does not work try higher.
     * @param f_ Function describing the shape of rope/tube.
     * @param df_ Derivative of function f (tangent to the curve discribing tube shape).
     * The derivative is a 3d vector describing tangent space along the rope.
     */
    Tube3D(const scalar_t& t_beg_, const scalar_t& t_end_, const scalar_t& radius_,
           const int characteristic_number_, std::function<vec_t(const double&)> f_,
           std::function<vec_t(const double&)> df_)
            : t_beg(t_beg_),
              t_end(t_end_),
              radius(radius_),
              characteristic_number(characteristic_number_),
              f(f_),
              df(df_) {
        assert(vec_t::dimension == 3 && "Tube3D works only in 3 dimensions.");
        assert(t_beg != t_end && "Beginning and ending points should not be the same.");
        assert(radius > 0 && "Tube radius should be greater than 0.");
        if (t_beg > t_end) std::swap(t_end, t_beg);
        calculateBBox();
    }
    /**
     * Overload for unambiguous Tube3D({1.2, 2.1, 0, 10000}, f, df);
     */
    Tube3D(std::initializer_list<scalar_t> params, std::function<vec_t(const double&)> f_,
           std::function<vec_t(const double&)> df_) {
        assert(params.size() == 4 && "Incorrect number of parameters for Tube constructor.");
        assert(vec_t::dimension == 3 && "Tube3D works only in 3 dimensions.");
        Range<double> param(params);
        Tube3D<vec_t>(param[0], param[1], param[2], static_cast<int>(param[3]), f_, df_);
    }
    /**
     * @brief Fills boundary and interior uniformly by supplying node counts.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(const Vec<int, vec_t::dimension>& internal_counts,
                     const Vec<int, vec_t::dimension>& boundary_counts) {
        fillUniformInterior(internal_counts);
        fillUniformBoundary(boundary_counts);
    }
    /**
     * @brief Fills boundary and interior uniformly by supplying spatial step.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniformWithStep(const vec_t& internal_steps, const vec_t& boundary_steps) {
        fillUniformInteriorWithStep(internal_steps);
        fillUniformBoundaryWithStep(boundary_steps);
    }

    /**
     * @brief Uniformly discretises underlying domain interior with counts[i] points
     * in dimension i. It fills positions with appropriate coordinates and types with 1s.
     * See test_domain.hpp for examples.
     * @param counts how many discretization points to use in each dimension.
     * Dimension are radial direction, angular direction, longitudinal direction
     */
    void fillUniformInterior(const Vec<int, vec_t::dimension>& counts) {
        clearInternalNodes();
        scalar_t dfi = 2.0 * M_PI / counts[1];
        scalar_t dr = radius / counts[0];
        scalar_t dt = (t_end - t_beg) / (counts[2] + 1);
        positions.reserve(positions.size() + counts[2] + (counts[0] - 1) * counts[1] * counts[2]);

        // This part is for r!=0. For r=0 radial direction does not matter.
        // longitudinal direction
        for (int k = 1; k <= counts[2]; k++) {
            vec_t tube_direction = df(t_beg + dt * k);
            tube_direction = tube_direction / tube_direction.norm();
            vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                            -tube_direction[0] - tube_direction[2],
                                            tube_direction[1] - tube_direction[0]});
            radius_direction = radius_direction / radius_direction.norm();

            // angular_direction
            for (int j = 0; j < counts[1]; j++) {
                // Rodrigues formula - check wiki
                vec_t radius_direction_rotated =
                        radius_direction * std::cos(dfi * j) +
                        radius_direction.cross(tube_direction) * std::sin(dfi * j);
                // radial direction
                for (int i = 1; i < counts[0]; ++i) {
                    positions.push_back(f(t_beg + dt * k) + radius_direction_rotated * dr * i);
                    types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
                }
            }
        }
        // This part is for r=0. Radial and angular direction does not matter.
        // longitudinal direction
        for (int k = 1; k <= counts[2]; k++) {
            positions.push_back(f(t_beg + dt * k));
            types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
        }
        // just checking number of generated points
        /*
         int number_of_points = counts[2] + (counts[0]-1)*counts[1]*counts[2];
        */
    }

    /**
     * @brief Uniformly discretizes underlying domain interior by calling fillUniformInterior with
     * appropriate counts.
     * @param steps what discretization step to use in each dimension
     * (radial, angular and longitudinal respectively)
     */
    void fillUniformInteriorWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in interior discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        counts[0] = static_cast<int>(std::ceil(radius / steps[0]));
        counts[1] = static_cast<int>(std::ceil(2.0 * M_PI / steps[1]));
        counts[2] = static_cast<int>(std::ceil(2.0 * M_PI / steps[2])) - 1;
        fillUniformInterior(counts);
    }

    /**
     * @brief Uniformly discretizes underlying domain boundary. It fills positions
     * with appropriate coordinates and types with -1s.
     * Currently only 3 dimensional tube is supported.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformBoundary(const Vec<int, vec_t::dimension>& counts) {
        static_assert(vec_t::dimension == 3, "Currently Tube only works in 3 dimensions!");
        for (auto x : counts) assert(x >= 2 && "All counts must be greater than 2.");
        clearBoundaryNodes();
        double dfi = 2.0 * M_PI / (counts[1]);
        double dr = radius / (counts[0] - 1);
        double dt = (t_end - t_beg) / (counts[2] - 1);
        if ((f(t_beg) - f(t_end)).norm() < EPS)
            positions.reserve(positions.size() + 2 * counts[2] * counts[1]);
        else
            positions.reserve(positions.size() + counts[0] * counts[1] * counts[2]);
        vec_t tube_direction = df(t_beg);
        tube_direction = tube_direction / tube_direction.norm();
        vec_t radius_direction =
                vec_t({tube_direction[1] + tube_direction[2], -tube_direction[0] - tube_direction[2],
                       tube_direction[1] - tube_direction[0]});
        radius_direction = radius_direction / radius_direction.norm();
        bool closed_curve = false;
        if ((f(t_beg) - f(t_end)).norm() < EPS) closed_curve = true;
        // prn(closed_curve);

        // this is for cylinder coat
        // longitudinal direction
        int max_long_count = counts[2];
        if (closed_curve) max_long_count = counts[2] - 1;
        for (int i = 0; i < max_long_count; i++) {
            tube_direction = df(t_beg + i * dt);
            tube_direction = tube_direction / tube_direction.norm();
            radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                      -tube_direction[0] - tube_direction[2],
                                      tube_direction[1] - tube_direction[0]});
            radius_direction = radius_direction / radius_direction.norm();
            // angular direction
            for (int j = 0; j < counts[1]; j++) {
                vec_t radius_direction_rotated =
                        radius_direction * std::cos(dfi * j) +
                        radius_direction.cross(tube_direction) * std::sin(dfi * j);
                positions.push_back(f(t_beg + i * dt) + radius_direction_rotated * radius);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }

        tube_direction = df(t_beg);
        tube_direction = tube_direction / tube_direction.norm();
        radius_direction =
                vec_t({tube_direction[1] + tube_direction[2], -tube_direction[0] - tube_direction[2],
                       tube_direction[1] - tube_direction[0]});
        radius_direction = radius_direction / radius_direction.norm();
        // this is for the beginning surface of the cylinder - circular end-plate at beginning
        //  angular direction
        NODE_TYPE joining_points = DEFAULT_BOUNDARY;  // Points located at the end.
        // For nonclosed shapes, these nodes are boundary,
        // but for closed shapes these nodes are internal.
        if (closed_curve) joining_points = DEFAULT_INTERNAL;
        // prn(closed_curve);
        for (int j = 0; j < counts[1]; j++) {
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(tube_direction) * std::sin(dfi * j);
            // radial direction - we omit the outermost point, which will be added to the coat
            // boundary
            for (int i = 0; i < counts[0] - 1; i++) {
                if (i == 0 && j > 0) continue;
                positions.push_back(f(t_beg) + dr * i * radius_direction_rotated);
                types.push_back(joining_points);
            }
        }

        if (closed_curve) return;
        // this is for the ending surface of the cylinder - circular end-plate at the end
        // angular direction
        tube_direction = df(t_end);
        tube_direction = tube_direction / tube_direction.norm();
        radius_direction =
                vec_t({tube_direction[1] + tube_direction[2], -tube_direction[0] - tube_direction[2],
                       tube_direction[1] - tube_direction[0]});
        radius_direction = radius_direction / radius_direction.norm();
        // this is for the ending surface of the tube - circular end-plate at the end
        // angular direction
        for (int j = 0; j < counts[1]; j++) {
            vec_t radius_direction_rotated =
                    radius_direction * std::cos(dfi * j) +
                    radius_direction.cross(tube_direction) * std::sin(dfi * j);
            // radial direction
            for (int i = 0; i < counts[0] - 1; i++) {
                if (i == 0 && j > 0) continue;
                positions.push_back(f(t_end) + dr * (i)*radius_direction_rotated);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }
        // just checking number of boundary points
        /*
        int number_of_points =2 +2* (counts[0]-2)*counts[1]+ (counts[2])*counts[1];
        */
        guessSetContainsPrecision();
    }

    /**
     * @brief Uniformly discretizes underlying domain boundary by calling
     * fillUniformBoundary with appropriate counts.
     * @param steps what discretization step to use in each dimension
     * (radial, angular and longitudinal respectively)
     */
    void fillUniformBoundaryWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in boundary discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        counts[0] = static_cast<int>(std::ceil(radius / steps[0])) + 1;
        counts[1] = static_cast<int>(std::ceil(2.0 * M_PI / steps[1]));
        counts[2] = static_cast<int>(std::ceil((t_end - t_beg) / steps[2])) + 1;
        fillUniformBoundary(counts);
    }

    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }

    /// Randomly fills tube interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        positions.reserve(positions.size() + num_of_points);
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            double random_t = uniform(generator) * (t_end - t_beg);
            vec_t tube_direction = df(random_t);
            tube_direction = tube_direction / tube_direction.norm();
            vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                            -tube_direction[0] - tube_direction[2],
                                            tube_direction[1] - tube_direction[0]});
            radius_direction = radius_direction / radius_direction.norm();
            double fi = uniform(generator) * 2.0 * M_PI;
            // randomly rotate radial direction - using Rodrigues formula
            vec_t radius_direction_rotated = radius_direction * std::cos(fi) +
                                             radius_direction.cross(tube_direction) * std::sin(fi);
            vec_t x = f(random_t) + uniform(generator) * radius * radius_direction_rotated;
            positions.push_back(x);
            types.push_back(NODE_TYPE::DEFAULT_INTERNAL);
        }
        guessSetContainsPrecision();
    }

    /// Randomly fills tube boundary with points.
    void fillRandomBoundary(int num_of_points) {
        clearBoundaryNodes();
        positions.reserve(positions.size() + num_of_points);
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        Range<int> elem = {0, 1, 2};

        bool closed_curve = false;
        if ((f(t_beg) - f(t_end)).norm() < EPS) closed_curve = true;
        if (!closed_curve) {
            double circle_surface = M_PI * radius * radius;
            Range<scalar_t> weights = {
                    std::abs(circle_surface),  // circular plate at the beginning
                    std::abs(circle_surface),  // circular plate at the end
                    std::abs(surface_area() - 2 * circle_surface)};  // coat of cylinder

            vec_t point;
            for (int i = 0; i < num_of_points; ++i) {
                int choice = random_choice(elem, weights, false, generator);
                double fi = uniform(generator) * 2 * M_PI;
                if (choice == 0) {  // tube plate at the beginning
                    vec_t tube_direction = df(t_beg);
                    tube_direction = tube_direction / tube_direction.norm();
                    vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                                    -tube_direction[0] - tube_direction[2],
                                                    tube_direction[1] - tube_direction[0]});
                    radius_direction = radius_direction / radius_direction.norm();
                    vec_t radius_direction_rotated =
                            radius_direction * std::cos(fi) +
                            radius_direction.cross(tube_direction) * std::sin(fi);
                    point = f(t_beg) + radius_direction_rotated * uniform(generator) * radius;
                } else if (choice == 1) {  // tube plate at the end
                    vec_t tube_direction = df(t_end);
                    tube_direction = tube_direction / tube_direction.norm();
                    vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                                    -tube_direction[0] - tube_direction[2],
                                                    tube_direction[1] - tube_direction[0]});
                    radius_direction = radius_direction / radius_direction.norm();
                    vec_t radius_direction_rotated =
                            radius_direction * std::cos(fi) +
                            radius_direction.cross(tube_direction) * std::sin(fi);
                    point = f(t_end) + radius_direction_rotated * uniform(generator) * radius;
                } else {  // tube coat
                    scalar_t random_t = (t_end - t_beg) * uniform(generator);
                    vec_t tube_direction = df(random_t);
                    tube_direction = tube_direction / tube_direction.norm();
                    vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                                    -tube_direction[0] - tube_direction[2],
                                                    tube_direction[1] - tube_direction[0]});
                    radius_direction = radius_direction / radius_direction.norm();
                    vec_t radius_direction_rotated =
                            radius_direction * std::cos(fi) +
                            radius_direction.cross(tube_direction) * std::sin(fi);
                    point = f(random_t) + radius_direction_rotated * radius;
                }
                positions.push_back(point);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        } else {
            for (int i = 0; i < num_of_points; ++i) {
                double fi = uniform(generator) * 2 * M_PI;
                scalar_t random_t = (t_end - t_beg) * uniform(generator);
                vec_t tube_direction = df(random_t);
                tube_direction = tube_direction / tube_direction.norm();
                vec_t radius_direction = vec_t({tube_direction[1] + tube_direction[2],
                                                -tube_direction[0] - tube_direction[2],
                                                tube_direction[1] - tube_direction[0]});
                radius_direction = radius_direction / radius_direction.norm();
                vec_t radius_direction_rotated =
                        radius_direction * std::cos(fi) +
                        radius_direction.cross(tube_direction) * std::sin(fi);
                vec_t point = f(random_t) + radius_direction_rotated * radius;
                positions.push_back(point);
                types.push_back(NODE_TYPE::DEFAULT_BOUNDARY);
            }
        }
        guessSetContainsPrecision();
    }

    /**
    * @brief Calculates volume of a tube. It uses characteristic_number for
    * numerical calculation of tube length (characteristic_number == number of steps).
    **/
    scalar_t volume() const override {
        double dt = (t_beg - t_end) / static_cast<double>(characteristic_number);
        double len = 0;
        // length of tube
        for (int i = 0; i < characteristic_number; i++) {
            len += (f(t_beg + (i + 1) * dt) - f(t_beg + i * dt)).norm();
        }
        return len * M_PI * radius * radius;
    }

    /**
    * @brief Calculates surface area of a tube. It uses characteristic_number for
    * numerical calculation of tube length (characteristic_number == number of steps).
    **/
    scalar_t surface_area() const override {
        double dt = (t_beg - t_end) / static_cast<double>(characteristic_number);
        double len = 0;
        // length of tube
        for (int i = 0; i < characteristic_number; i++) {
            len += (f(t_beg + (i + 1) * dt) - f(t_beg + i * dt)).norm();
        }
        if (std::fabs(f(t_end) == f(t_beg)) < EPS) return 2.0 * M_PI * radius * len;
        return 2.0 * M_PI * radius * len + 2.0 * M_PI * radius * radius;
    }

    /**
     * @brief checks if the curve is closed or not
     * If beginning and ending points are less than EPS apart,
     * then the curve is closed, otherwise it's not.
     */
    bool is_closed() const { return (f(t_beg) - f(t_end)).norm() < EPS; }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /// Calculates bbox of a domain.
    void calculateBBox() {
        vec_t min(f(t_beg));
        vec_t max(f(t_beg));
        double dt = (t_end - t_beg) / characteristic_number;
        for (int i = 1; i <= characteristic_number; i++) {
            vec_t point = f(dt * i + t_beg);
            for (int j = 0; j < vec_t::dimension; j++) {
                if (point[j] < min[j]) min[j] = point[j];
                if (point[j] > max[j]) max[j] = point[j];
            }
        }
        bbox = std::make_pair(min - 1.3 * vec_t({radius, radius, radius}),
                              max + 1.3 * vec_t({radius, radius, radius}));
    }
    /**
     * @brief Calculates a distance between point and curve.
     * @param point Point in 3d space. Function calculate distance between this point and curve f.
     * @param t Scalar parameter of curve f (curve describing tube shape).
     */
    double distance(vec_t point, double t) const { return (point - f(t)).norm(); }

    /**
     * @brief Calculates the derivative of function distance() with respect to t.
     * @param point Point in 3D space.
     * @param t Scalar parameter of curve f (curve describing tube shape).
     */
    double distance_der(vec_t point, double t) const {
        return (f(t) - point).dot(df(t)) / distance(point, t);
    }

    /**
     * @brief vec_test if a point is contained in a basic domain, paying
     * no notice to subdomains. We use Newton method to find minimal distance
     * between point and curve. If distance is less than or equal to radius
     * the point is inside, otherwise it's outside.
     * An EPS margin pointing away from normal is used.
     * @param point The point we are interested in,
     * @return True if point is conatined in the basic domain
     * and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        // scalar_t margin = contains_precision * thickness * normal_vector;
        double min = radius * characteristic_number * 10;
        double dt = (t_end - t_beg) / static_cast<double>(characteristic_number);
        double min_t = t_beg;
        for (int i = 0; i < characteristic_number; i++) {
            double dist = distance(point, t_beg + i * dt);
            if (dist < min) {
                min = dist;
                min_t = t_beg + dt * i;
            }
        }
        if (min < radius * (1.0 + 1e-8)) return normal_vector == INSIDE;
        dt = 1;
        double t2 = min_t;
        int i = 0;
        double delta = 1.0 / static_cast<double>(characteristic_number);
        while (std::fabs(dt) > EPS) {
            double val = distance_der(point, min_t);
            double der = (distance_der(point, min_t + delta) - val) / delta;
            dt = val / der;
            t2 = min_t - dt;
            // if (t2 < t_beg-0.1) t2 = t_beg;
            // if (t2 > t_end+0.1) t2 = t_end;
            /* if(i>40) {
              prn(dt);
              prn(distance(point, t2));
              prn(t2)
              } */
            if (i > 30) {
                if (distance(point, t2) < radius * (1.0 + EPS)) return normal_vector == INSIDE;
                return normal_vector == OUTSIDE;
            }
            min_t = t2;
            i++;
        }
        if (distance(point, t2) < radius * (1.0 + EPS)) return normal_vector == INSIDE;
        return normal_vector == OUTSIDE;
    }

    /**
     * Return a new Tube3D, which is a copy of this one.
     */
    Tube3D* clone() const override {
        auto p = new Tube3D(t_beg, t_end, radius, characteristic_number, f, df);
        this->clone_base(p);
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const Tube3D<vec_t>& d) {
    os << "Tube3D:\n"
       << "    beg: " << d.t_beg << "\n"
       << "    end: " << d.t_end << "\n";
    return d.output_report(os);
}