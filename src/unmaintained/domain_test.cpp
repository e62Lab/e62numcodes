#include "domain.hpp"
#include "includes.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, Area) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    // Test default and copy constructor
    RectangleDomain<Vec3d> domain3d_ = RectangleDomain<Vec3d>({0, 1, 2}, {-1, 2, 3});

    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    CircleDomain<Vec3d> cd3_ = CircleDomain<Vec3d>({4, -2, 0}, 4);

    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);
    Cylinder3D<Vec3d> cylinder_ = Cylinder3D<Vec3d>({3, -2, 0}, {6, 3, -3}, 2.5);

    // area
    EXPECT_DOUBLE_EQ(2, std::abs(domain1d.surface_area()));
    EXPECT_DOUBLE_EQ(1.4, std::abs(domain2d.surface_area()));
    EXPECT_DOUBLE_EQ(6, std::abs(domain3d.surface_area()));
    EXPECT_DOUBLE_EQ(6, std::abs(domain3d_.surface_area()));
    EXPECT_DOUBLE_EQ(2, std::abs(cd1.surface_area()));
    EXPECT_DOUBLE_EQ(2 * M_PI * 5, std::abs(cd2.surface_area()));
    EXPECT_DOUBLE_EQ(4 * M_PI * 4 * 4, std::abs(cd3.surface_area()));
    EXPECT_DOUBLE_EQ(4 * M_PI * 4 * 4, std::abs(cd3_.surface_area()));

    EXPECT_DOUBLE_EQ(2 * M_PI * 2.5 * std::sqrt(43) + 2 * M_PI * 2.5 * 2.5,
                     std::abs(cylinder.surface_area()));
    EXPECT_DOUBLE_EQ(2 * M_PI * 2.5 * std::sqrt(43) + 2 * M_PI * 2.5 * 2.5,
                     std::abs(cylinder_.surface_area()));

    // cylidner constructed using tube
    double tube_radius = 2.5;
    Tube3D<Vec3d> tube(0, 1, tube_radius, 10000,
                     [](const double& a){return Vec3d({2, 2, 2+a});},
                     [](const double&){return Vec3d({0, 0, 0});});
    EXPECT_NEAR(M_PI*tube_radius*tube_radius, tube.volume(), 1e-7);

    // torus constructed using tube
    double torus_smaller_radius = 1.5;
    double torus_larger_radius = 5.5;
    Tube3D<Vec3d> tube_torus(0, 2*M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t){
                                 return Vec3d({std::cos(t), std::sin(t), 0})*torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t){
                                 return Vec3d({-std::sin(t), std::cos(t), 0})*torus_larger_radius;
                             });
    // 1e-5 is a reasonable accuracy for numerically calculated surface area of a torus.
    EXPECT_NEAR(2.0*M_PI*torus_larger_radius*2.0*M_PI*torus_smaller_radius,
                tube_torus.surface_area(), 1e-5);
}

TEST(Domain, Volume) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);

    EXPECT_DOUBLE_EQ(1, std::abs(domain1d.volume()));
    EXPECT_DOUBLE_EQ(0.2 * 0.5, std::abs(domain2d.volume()));
    EXPECT_DOUBLE_EQ(1, std::abs(domain3d.volume()));
    EXPECT_DOUBLE_EQ(2, std::abs(cd1.volume()));
    EXPECT_DOUBLE_EQ(M_PI * 5 * 5, std::abs(cd2.volume()));
    EXPECT_DOUBLE_EQ(4.0 / 3 * M_PI * 4 * 4 * 4, std::abs(cd3.volume()));

    EXPECT_DOUBLE_EQ(M_PI * 2.5 * 2.5 * std::sqrt(43), std::abs(cylinder.volume()));

    double tube_radius = 2.5;
    Tube3D<Vec3d> tube(0, 1, tube_radius, 10000,
                       [](const double& a) -> Vec3d {
                           return Vec3d({2, 2, 2 + a});
                       },
                       [](const double& a) -> Vec3d {
                           return Vec3d({0, 0, a / a});
                       });
    EXPECT_NEAR(M_PI * tube_radius * tube_radius, tube.volume(), 1e-7);

    // torus constructed using tube
    double torus_smaller_radius = 1.5;
    double torus_larger_radius = 5.5;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });
    EXPECT_NEAR(2*M_PI*torus_larger_radius*M_PI*torus_smaller_radius*torus_smaller_radius,
                tube_torus.volume(), 1e-5);
}

TEST(Domain, ContainsBasic) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);

    EXPECT_TRUE(domain1d.contains({1.5}));
    EXPECT_TRUE(domain1d.contains({1.01}));
    EXPECT_TRUE(domain1d.contains({1.99}));
    EXPECT_TRUE(domain1d.contains({1}));
    EXPECT_TRUE(domain1d.contains({2}));
    EXPECT_TRUE(domain1d.contains({1 - 1e-13}));  // thick
    EXPECT_TRUE(domain1d.contains({2 + 1e-13}));
    EXPECT_FALSE(domain1d.contains({3}));
    EXPECT_FALSE(domain1d.contains({0}));
    EXPECT_FALSE(domain1d.contains({-1}));

    EXPECT_TRUE(domain2d.contains({1.1, 3.4}));
    EXPECT_FALSE(domain2d.contains({0.9, 3.4}));
    EXPECT_FALSE(domain2d.contains({1.1, 2.3}));
    EXPECT_FALSE(domain2d.contains({0.6, -1.4}));

    EXPECT_TRUE(domain3d.contains({-0.5, 1.5, 2.5}));
    EXPECT_TRUE(domain3d.contains({0, 1, 2}));
    EXPECT_FALSE(domain3d.contains({-1.5, 1.5, 2.5}));
    EXPECT_FALSE(domain3d.contains({-0.5, 2.5, 2.5}));
    EXPECT_FALSE(domain3d.contains({-0.5, 1.5, 3.5}));

    EXPECT_TRUE(cd1.contains({3.5}));
    EXPECT_TRUE(cd1.contains({4}));
    EXPECT_FALSE(cd1.contains({1}));
    EXPECT_FALSE(cd1.contains({5}));

    EXPECT_TRUE(cd2.contains({-0.9, 1.8}));
    EXPECT_TRUE(cd2.contains({-1.1, 2.2}));
    EXPECT_TRUE(cd2.contains({0, 3}));
    EXPECT_TRUE(cd2.contains({4, 2}));
    EXPECT_TRUE(cd2.contains({4 + 1e-13, 2}));  // thick
    EXPECT_FALSE(cd2.contains({-8, 3}));
    EXPECT_FALSE(cd2.contains({12, 3}));

    EXPECT_TRUE(cd3.contains({4, 2, 0}));
    EXPECT_TRUE(cd3.contains({4, -2, 3}));
    EXPECT_FALSE(cd3.contains({10, 3, 4}));
    EXPECT_FALSE(cd3.contains({-5, 3, 0}));

    EXPECT_TRUE(cylinder.contains({4, 1, -1}));
    EXPECT_TRUE(cylinder.contains({3, -1, -2}));
    EXPECT_TRUE(cylinder.contains({3, -2, 0}));
    EXPECT_TRUE(cylinder.contains({6, 3, -3}));
    EXPECT_FALSE(cylinder.contains({10, 3, 4}));
    EXPECT_FALSE(cylinder.contains({-5, 3, 0}));
    EXPECT_FALSE(cylinder.contains({-10, 10, 10}));

    double torus_smaller_radius = 2;
    double torus_larger_radius = 6;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });
    EXPECT_FALSE(tube_torus.contains({3, 2, 1}));
    EXPECT_FALSE(tube_torus.contains({10, 2, 1}));
    EXPECT_FALSE(tube_torus.contains({-3, 2, 1}));
    EXPECT_TRUE(tube_torus.contains({-5, 2, 1}));
    EXPECT_TRUE(tube_torus.contains({2, 5, -1}));
    EXPECT_TRUE(tube_torus.contains({-3, -4, -1.5}));

    // Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);
    Tube3D<Vec3d> tube_cylinder(0.0, 1.0, 2.5, 10000,
                                [](const double& t) -> Vec3d {
                                    return Vec3d({3.0 + 3.0 * t, -2.0 + 5.0 * t, -3.0 * t});
                                },
                                // return Vec3d({3.0, -2.0, 0.0}) + t*Vec3d({3.0, 5.0, -3.0});},
                                [](const double&) -> Vec3d {
                                    return Vec3d({3.0, 5.0, -3.0});
                                });
    EXPECT_TRUE(tube_cylinder.contains({4, 1, -1}));
    EXPECT_TRUE(tube_cylinder.contains({3, -1, -2}));
    EXPECT_TRUE(tube_cylinder.contains({3, -2, 0}));
    EXPECT_TRUE(tube_cylinder.contains({6, 3, -3}));
    EXPECT_FALSE(tube_cylinder.contains({10, 3, 4}));
    EXPECT_FALSE(tube_cylinder.contains({-5, 3, 0}));
    EXPECT_FALSE(tube_cylinder.contains({-10, 10, 10}));

    EXPECT_TRUE(tube_cylinder.contains({4, 1, -1}));
    EXPECT_TRUE(tube_cylinder.contains({3, -1, -2}));
    EXPECT_TRUE(tube_cylinder.contains({3, -2, 0}));
    EXPECT_TRUE(tube_cylinder.contains({6, 3, -3}));
    EXPECT_FALSE(tube_cylinder.contains({10, 3, 4}));
    EXPECT_FALSE(tube_cylinder.contains({-5, 3, 0}));
    EXPECT_FALSE(tube_cylinder.contains({-10, 10, 10}));
}

TEST(Domain, ContainsWithObstacles) {
    // domains with holes
    RectangleDomain<Vec2d> rd({-1, 1}, {5, 3});
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    rd.fillUniformBoundary({20, 5});
    cd2.subtract(rd);

    EXPECT_FALSE(cd2.contains({1, 2}));
    EXPECT_FALSE(cd2.contains({3.9, 2}));
    EXPECT_TRUE(cd2.contains({3, 3.5}));
    EXPECT_TRUE(cd2.contains({0, 4}));

    RectangleDomain<Vec2d> domain2d({0, 0}, {4, 3});
    domain2d.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    domain2d.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    EXPECT_TRUE(domain2d.contains({2, 1}));
    EXPECT_FALSE(domain2d.contains({0.5, 0.5}));
    EXPECT_FALSE(domain2d.contains({3, 2}));

    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);
    CircleDomain<Vec3d> cd3({4.5, 0.5, -1.5}, 1);
    cylinder.fillUniformBoundary({20, 10, 10});
    cylinder.subtract(cd3);

    EXPECT_FALSE(cylinder.contains({4.5, 0.5, -1.5}));
    EXPECT_FALSE(cylinder.contains({6, -3, -5}));
    EXPECT_FALSE(cylinder.contains({1.5, -4.5, 1.5}));
    EXPECT_TRUE(cylinder.contains({3, -1, -2}));
    EXPECT_TRUE(cylinder.contains({4, -1.5, -1}));

    double torus_smaller_radius = 2;
    double torus_larger_radius = 6;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });

    CircleDomain<Vec3d> circle1({6, 0, 0}, 2);
    CircleDomain<Vec3d> circle2({0, 6, 0}, 2);
    tube_torus.subtract(circle1);
    tube_torus.subtract(circle2);

    EXPECT_FALSE(tube_torus.contains({6, 0, 0}));
    EXPECT_FALSE(tube_torus.contains({6, 0.5, 0}));
    EXPECT_FALSE(tube_torus.contains({0, 6, 0.5}));
    EXPECT_TRUE(tube_torus.contains({-6, 0, 0}));
    EXPECT_TRUE(tube_torus.contains({0, -6, 0}));
}

TEST(Domain, ContainsUnion) {
    RectangleDomain<Vec2d> rd({-1, 1}, {5, 3});
    rd = RectangleDomain<Vec2d>({0, 0}, {4, 3});
    rd.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    rd.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    CircleDomain<Vec2d> circle_rect_union({4, 3}, 1);
    circle_rect_union.add(rd);
    EXPECT_TRUE(circle_rect_union.contains({4.1, 3.3}));
    EXPECT_TRUE(circle_rect_union.contains({1, 2}));
    EXPECT_FALSE(circle_rect_union.contains({0.5, 0.5}))
        << "CHECK: Are normal vectors copied on clone?";
    EXPECT_FALSE(circle_rect_union.contains({3, 2}));
    EXPECT_FALSE(circle_rect_union.contains({6.2, 3}));

    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);
    CircleDomain<Vec3d> cd3({6, 3, -3}, 2.5);
    cylinder.subtract(cd3);
    EXPECT_FALSE(cylinder.contains({6, 3, -3}));
    cylinder.add(cd3);
    EXPECT_TRUE(cylinder.contains({6, 3, -3}));
    EXPECT_TRUE(cylinder.contains({2, -0.5, 0}));
    cylinder.subtract(CircleDomain<Vec3d>({3, -2, 0}, 2.5));
    EXPECT_FALSE(cylinder.contains({2, -0.5, 0}));
}

TEST(Domain, ContainsNested) {
    // test inclusion - exclusion
    RectangleDomain<Vec2d> inner({1.5, 1.5}, {2.5, 2.5});
    inner.fillUniform({10, 10}, {10, 10});
    CircleDomain<Vec2d> middle({2, 2}, 1);
    middle.fillUniformBoundary(20);
    RectangleDomain<Vec2d> outer({0, 0}, {4, 4});
    outer.fillUniform({20, 20}, {20, 20});
    outer.subtract(middle);
    outer.add(inner);
    assert(outer.contains({1, 1}));
    assert(!outer.contains({1.2, 2}));
    assert(outer.contains({2, 2}));

    // test inclusion - exclusion
    inner.fillUniform({10, 10}, {10, 10});
    middle = CircleDomain<Vec2d>({2, 2}, 1);
    middle.fillUniform(80, 40);
    middle.subtract(inner);
    outer = RectangleDomain<Vec2d>({0, 0}, {4, 4});
    outer.fillUniform({50, 50}, {50, 50});
    outer.subtract(middle);
    assert(outer.contains({1, 1}));
    assert(!outer.contains({1.2, 2}));
    assert(outer.contains({2, 2}));

    Cylinder3D<Vec3d> cylinder({3, -2, 0}, {6, 3, -3}, 2.5);
    CircleDomain<Vec3d> circle1({6, 3, -3}, 2.5);
    CircleDomain<Vec3d> circle2({6, 3, -3}, 1);
    cylinder.subtract(circle1);
    EXPECT_FALSE(cylinder.contains({6, 3, -3}));
    cylinder.add(circle2);
    EXPECT_TRUE(cylinder.contains({6, 3, -3}));
    cylinder.subtract(RectangleDomain<Vec3d>({5, 2, -2}, {7, 4, -4}));
    EXPECT_FALSE(cylinder.contains({6, 3, -3}));
    EXPECT_FALSE(cylinder.contains({6.6, 3.3, -3.3}));
    EXPECT_FALSE(cylinder.contains({5.4, 2.7, -2.7}));
}
TEST(Domain, RectConstructor) {
    Range<Vec2d> expected = {{1, 1}, {-1, 1}, {0, -1}};
    RectangleDomain<Vec2d> rd(expected, 4);
    EXPECT_EQ(Vec2d({-1, -1}), rd.beg);
    EXPECT_EQ(Vec2d({1, 1}), rd.end);
    EXPECT_EQ(Vec2d({-1, -1}), rd.getBBox().first);
    EXPECT_EQ(Vec2d({1, 1}), rd.getBBox().second);
    for (int t : rd.types) EXPECT_EQ(4, t);
    EXPECT_EQ(expected, rd.positions);

    expected = {{1.3, 1}, {-1, 1.2}, {0, -1.6}};
    Range<int> exp_types = {1, 2, 6};
    rd = RectangleDomain<Vec2d>(expected, exp_types);
    EXPECT_EQ(Vec2d({-1, -1.6}), rd.beg);
    EXPECT_EQ(Vec2d({1.3, 1.2}), rd.end);
    EXPECT_EQ(Vec2d({-1, -1.6}), rd.getBBox().first);
    EXPECT_EQ(Vec2d({1.3, 1.2}), rd.getBBox().second);
    EXPECT_EQ(expected, rd.positions);
    EXPECT_EQ(exp_types, rd.types);
}

TEST(Domain, UniformCorrect) {
    // fill with count
    RectangleDomain<Vec1d> domain1d(Vec1d(0.0), {1});
    domain1d.fillUniform({3}, {40});
    Range<Vec1d> expected = {0.25, 0.5, 0.75};
    Range<Vec1d> actual = domain1d.getInternalNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    expected = {0., 1.};
    actual = domain1d.getBoundaryNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    double radius = 2.5;
    Cylinder3D<Vec3d> cylinder({2, 2, 2}, {2, 2, 3}, radius);
    Vec<int, 3> counts({17, 23, 13});  // choose some ugly divisions of intervals
    cylinder.fillUniformInterior(counts);
    Range<Vec3d> pos = cylinder.positions;
    pos.erase(std::remove_if(pos.begin(), pos.end(),
                             [](Vec3d& vec) { return (vec[0] != 2.0 || vec[1] != 2.0); }),
              pos.end());  // positions now contain only central cylinder line
    // let's check if points in z direction are correctly distributred;
    sort(pos.begin(), pos.end(), [](const Vec3d& a, const Vec3d& b) { return a[2] < b[2]; });
    ASSERT_EQ(counts[2], pos.size());
    double dz = 1.0 / static_cast<double>(counts[2] + 1);
    for (int i = 0; i < counts[2]; i++) {
        EXPECT_DOUBLE_EQ(pos[i][2], 2.0 + (i + 1) * dz);
    }

    // Let's repeat the procedure for radial positions
    pos = cylinder.positions;
    double dr = radius / static_cast<double>(counts[0]);
    std::vector<double> radii;
    for (Vec3d vec : pos)
        radii.push_back(std::sqrt((vec[0] - 2) * (vec[0] - 2) + (vec[1] - 2) * (vec[1] - 2)));
    std::sort(radii.begin(), radii.end());
    radii.erase(
        std::unique(radii.begin(), radii.end(),
                    [](const double& a, const double& b) { return std::abs(a - b) < 1e-14; }),
        radii.end());  // there is some numerical error involved - that's why 1e-14
    for (int i = 0; i < counts[0]; i++) {
        EXPECT_NEAR(radii[i], i * dr, 1e-14);
    }

    // fill with step
    domain1d = RectangleDomain<Vec1d>(Vec1d(0.0), {1});
    domain1d.fillUniformWithStep({0.25}, {0.01});
    expected = {0.25, 0.5, 0.75};
    actual = domain1d.getInternalNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    expected = {0., 1.};
    actual = domain1d.getBoundaryNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    CircleDomain<Vec1d> cd({0.5}, 0.5);
    cd.fillUniform({3}, {40});
    expected = {0.25, 0.5, 0.75};
    actual = cd.getInternalNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    expected = {0., 1.};
    actual = cd.getBoundaryNodes();
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) EXPECT_DOUBLE_EQ(expected[i][0], actual[i][0]);

    // fill with count
    RectangleDomain<Vec2d> domain2d({0, 0}, {1, 1});
    domain2d.fillUniform({1, 1}, {3, 3});
    Range<Vec2d> expected2 = {{0.5, 0.5}};
    Range<Vec2d> actual2 = domain2d.getInternalNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    expected2 = {{0, 0}, {0, 0.5}, {0, 1}, {0.5, 0}, {0.5, 1}, {1, 0}, {1, 0.5}, {1, 1}};
    actual2 = domain2d.getBoundaryNodes();
    std::sort(actual2.begin(), actual2.end());
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    // fill with step
    domain2d = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    domain2d.fillUniformWithStep(0.5, 0.5);
    expected2 = {{0.5, 0.5}};
    actual2 = domain2d.getInternalNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    expected2 = {{0, 0}, {0, 0.5}, {0, 1}, {0.5, 0}, {0.5, 1}, {1, 0}, {1, 0.5}, {1, 1}};
    actual2 = domain2d.getBoundaryNodes();
    std::sort(actual2.begin(), actual2.end());
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_DOUBLE_EQ(expected2[i][0], actual2[i][0]);
        EXPECT_DOUBLE_EQ(expected2[i][1], actual2[i][1]);
    }

    CircleDomain<Vec2d> cd2({0, 0}, 1);
    cd2.fillUniformBoundary(4);
    expected2 = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    actual2 = cd2.getBoundaryNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_NEAR(expected2[i][0], actual2[i][0], 1e-15);
        EXPECT_NEAR(expected2[i][1], actual2[i][1], 1e-15);
    }

    // fill with step
    cd2.fillUniformBoundaryWithStep(M_PI / 2);
    expected2 = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    actual2 = cd2.getBoundaryNodes();
    ASSERT_EQ(expected2.size(), actual2.size());
    for (int i = 0; i < expected2.size(); ++i) {
        EXPECT_NEAR(expected2[i][0], actual2[i][0], 1e-15);
        EXPECT_NEAR(expected2[i][1], actual2[i][1], 1e-15);
    }

    // cylinder - fill with step
    radius = 2.5;
    double radial_step = 0.125;
    double angle_step = M_PI / 5.0;
    cylinder.fillUniformWithStep({radial_step, angle_step, 0.1}, {radial_step, angle_step, 0.1});
    Range<Vec3d> int_nodes = cylinder.getInternalNodes();
    std::sort(int_nodes.begin(), int_nodes.end(), [](const Vec3d& a, const Vec3d& b) {
        return a[0] * a[0] + a[1] * a[1] < b[0] * b[0] + b[1] * b[1];
    });
    int_nodes.erase(
        std::unique(int_nodes.begin(), int_nodes.end(),
                    [](const Vec3d& a, const Vec3d& b) {
                        return std::sqrt(std::pow(a[0] - 2, 2) + std::pow(a[1] - 2, 2)) -
                                   std::sqrt(std::pow(b[0] - 2, 2) + std::pow(b[1] - 2, 2)) <
                               1e-10;
                    }),
        int_nodes.end());
    // there is some numerical error involved - that's why 1e-14 std:unique

    // checking internal nodes -radial direction
    Range<double> distances_from_center;
    std::transform(
        int_nodes.begin(), int_nodes.end(), std::back_inserter(distances_from_center),
        [](Vec3d vec) { return std::sqrt(std::pow(vec[0] - 2, 2) + std::pow(vec[1] - 2, 2)); });
    EXPECT_EQ(static_cast<int>(std::ceil(2.5 / radial_step)), distances_from_center.size());
    std::sort(distances_from_center.begin(), distances_from_center.end());
    dr = radius / (static_cast<int>(std::ceil(2.5 / radial_step)));
    for (int i = 0; i < distances_from_center.size(); i++) {
        EXPECT_NEAR(dr * i, distances_from_center[i], 1e-14);
    }
    int num_internal_radial_values = distances_from_center.size();

    // checking angle values
    Range<double> angles;
    int_nodes = cylinder.getInternalNodes();
    std::transform(int_nodes.begin(), int_nodes.end(), std::back_inserter(angles),
                   [](Vec3d vec) { return std::atan2(vec[1] - 2.0, vec[0] - 2.0) + M_PI; });
    std::sort(angles.begin(), angles.end());
    angles.erase(std::unique(angles.begin(), angles.end(),
                             [](double a, double b) {
                                 return std::abs(a - b) < 1e-14 ||
                                        std::abs(std::abs(a - b) - 2.0 * M_PI) < 1e-14;
                             }),
                 angles.end());
    if (std::abs(angles[angles.size() - 1] - 2.0 * M_PI) < 1e-14)
        angles.erase(angles.begin() + angles.size() - 1);
    // std::cout<<angles<<std::endl;
    EXPECT_EQ(static_cast<int>(std::ceil(2.0 * M_PI / angle_step)), angles.size());
    std::sort(angles.begin(), angles.end());
    double dfi = 2.0 * M_PI / (static_cast<int>(std::ceil(2 * M_PI / angle_step)));
    for (int i = 0; i < angles.size(); i++) {
        EXPECT_NEAR(dfi * i, angles[i], 1e-14);
    }

    // checking boundary nodes, first radial values
    Range<Vec3d> bou_nodes = cylinder.positions[cylinder.types < 0];
    std::sort(bou_nodes.begin(), bou_nodes.end(), [](const Vec3d& a, const Vec3d& b) {
        return a[0] * a[0] + a[1] * a[1] < b[0] * b[0] + b[1] * b[1];
    });
    bou_nodes.erase(
        std::unique(bou_nodes.begin(), bou_nodes.end(),
                    [](const Vec3d& a, const Vec3d& b) {
                        return std::sqrt(std::pow(a[0] - 2, 2) + std::pow(a[1] - 2, 2)) -
                                   std::sqrt(std::pow(b[0] - 2, 2) + std::pow(b[1] - 2, 2)) <
                               1e-14;
                    }),
        bou_nodes.end());
    // there is some numerical error involved - that's why 1e-14 std:unique

    distances_from_center.clear();
    std::transform(
        bou_nodes.begin(), bou_nodes.end(), std::back_inserter(distances_from_center),
        [](Vec3d vec) { return std::sqrt(std::pow(vec[0] - 2, 2) + std::pow(vec[1] - 2, 2)); });
    EXPECT_EQ(static_cast<int>(std::ceil(2.5 / radial_step)) + 1, distances_from_center.size());
    std::sort(distances_from_center.begin(), distances_from_center.end());
    dr = radius / (static_cast<int>(std::ceil(2.5 / radial_step)));
    EXPECT_DOUBLE_EQ(radius / (static_cast<int>(std::ceil(2.5 / radial_step))),
                     radius / (distances_from_center.size() - 1));
    for (int i = 0; i < distances_from_center.size(); i++) {
        EXPECT_NEAR(dr * i, distances_from_center[i], 1e-14);
    }
    // there should be one more radial boundary value than internal radial value
    EXPECT_EQ(num_internal_radial_values + 1, distances_from_center.size());

    // now let's check angle values for boundary nodes
    angles.clear();
    bou_nodes = cylinder.positions[cylinder.types < 0];
    std::transform(bou_nodes.begin(), bou_nodes.end(), std::back_inserter(angles),
                   [](Vec3d vec) { return std::atan2(vec[1] - 2.0, vec[0] - 2.0) + M_PI; });
    std::sort(angles.begin(), angles.end());
    angles.erase(std::unique(angles.begin(), angles.end(),
                             [](double a, double b) {
                                 return std::abs(a - b) < 1e-14 ||
                                        std::abs(std::abs(a - b) - 2.0 * M_PI) < 1e-14;
                             }),
                 angles.end());
    if (std::abs(angles[angles.size() - 1] - 2.0 * M_PI) < 1e-14)
        angles.erase(angles.begin() + angles.size() - 1);
    // std::cout<<angles<<std::endl;
    EXPECT_EQ(static_cast<int>(std::ceil(2.0 * M_PI / angle_step)), angles.size());
    std::sort(angles.begin(), angles.end());
    for (int i = 0; i < angles.size(); i++) {
        EXPECT_NEAR(dfi * i, angles[i], 1e-14);
    }
}

TEST(Domain, UniformInteriorConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    // torus using Tube3D class
    Tube3D<Vec3d> tube3d(0, 1, 2, 10,
                         [](const double& t) -> Vec3d {
                             return Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t});
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });

    domain1d.fillUniformInterior({10});
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillUniformInterior({10, 10});
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillUniformInterior({10, 10, 10});
    EXPECT_TRUE(domain3d.valid());
    cylinder3d.fillUniformInterior({10, 10, 10});
    EXPECT_TRUE(cylinder3d.valid());
    tube3d.fillUniformInterior({10, 10, 10});
    EXPECT_TRUE(tube3d.valid());
    domain1d.fillUniformBoundary({12});
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillUniformBoundary({12, 12});
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillUniformBoundary({12, 12, 12});
    EXPECT_TRUE(domain3d.valid());

    cylinder3d.fillUniformInterior({12, 12, 12});
    EXPECT_TRUE(cylinder3d.valid());
    tube3d.fillUniformInterior({12, 12, 12});
    EXPECT_TRUE(tube3d.valid());
    tube_torus.fillUniformInterior({12, 12, 12});
    EXPECT_TRUE(tube_torus.valid());
}

TEST(Domain, UniformBoundaryConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    Tube3D<Vec3d> tube3d(0, 1, 2, 10,
                         [](const double& t) -> Vec3d {
                             return (Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t}));
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });
    // test consistency of fills
    cd1.fillUniformBoundary(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillUniformBoundary(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillUniformBoundary(1000);
    EXPECT_TRUE(cd3.valid());
    cd1.fillUniformInterior(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillUniformInterior(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillUniformInterior(1000);
    EXPECT_TRUE(cd3.valid());
    cylinder3d.fillUniformBoundary({12, 12, 12});
    EXPECT_TRUE(cylinder3d.valid());
    tube3d.fillUniformBoundary({12, 12, 12});
    EXPECT_TRUE(tube3d.valid());
    tube_torus.fillUniformBoundary({12, 12, 12});
    EXPECT_TRUE(tube_torus.valid());
}

TEST(Domain, RandomInteriorConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    Tube3D<Vec3d> tube3d(0, 1, 2, 10,
                         [](const double& t) -> Vec3d {
                             return (Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t}));
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });

    domain1d.fillRandomInterior(100);
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillRandomInterior(100);
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillRandomInterior(100);
    EXPECT_TRUE(domain3d.valid());
    cd1.fillRandomInterior(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillRandomInterior(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillRandomInterior(1000);
    EXPECT_TRUE(cd3.valid());
    cylinder3d.fillRandomInterior(10000);
    EXPECT_TRUE(cd3.valid());
    tube3d.fillRandomInterior(10000);
    EXPECT_TRUE(tube3d.valid());
    tube_torus.fillRandomInterior(10000);
    EXPECT_TRUE(tube3d.valid());
}

TEST(Domain, RandomBoundaryConsistency) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    CircleDomain<Vec1d> cd1({3}, 1);
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    Tube3D<Vec3d> tube3d(0, 1, 2, 100,
                         [](const double& t) -> Vec3d {
                             return (Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t}));
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 100,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });

    domain1d.fillRandomBoundary(100);
    EXPECT_TRUE(domain1d.valid());
    domain2d.fillRandomBoundary(100);
    EXPECT_TRUE(domain2d.valid());
    domain3d.fillRandomBoundary(100);
    EXPECT_TRUE(domain3d.valid());
    cd1.fillRandomBoundary(100);
    EXPECT_TRUE(cd1.valid());
    cd2.fillRandomBoundary(100);
    EXPECT_TRUE(cd2.valid());
    cd3.fillRandomBoundary(1000);
    EXPECT_TRUE(cd3.valid());
    cylinder3d.fillRandomBoundary(10000);
    EXPECT_TRUE(cylinder3d.valid());
    tube3d.fillRandomBoundary(10000);
    EXPECT_TRUE(tube3d.valid());
    tube_torus.fillRandomBoundary(10000);
    EXPECT_TRUE(tube_torus.valid());
}

TEST(Domain, RandomFills) {
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    domain1d.fillRandom(10, 10);
    //      std::cerr << domain1d.getMatlabData() << std::endl;

    RectangleDomain<Vec2d> domain2d({1, 3}, {1.2, 3.5});
    domain2d.fillRandom(10, 10);
    //      std::cerr << domain2d.getMatlabData() << std::endl;

    RectangleDomain<Vec3d> domain3d({0, 1, 2}, {-1, 2, 3});
    domain3d.fillRandom(10, 10);
    //      std::cerr << domain3d.getMatlabData() << std::endl;

    CircleDomain<Vec1d> cd1({3}, 1);
    cd1.fillRandom(10, 10);
    //      std::cerr << cd1.getMatlabData() << std::endl;

    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    cd2.fillRandom(10, 10);
    //      std::cerr << cd2.getMatlabData() << std::endl;

    CircleDomain<Vec3d> cd3({4, -2, 0}, 4);
    cd3.fillRandom(10, 10);
    //      std::cerr << cd3.getMatlabData() << std::endl;

    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    Tube3D<Vec3d> tube3d(0, 1, 2, 10,
                         [](const double& t) -> Vec3d {
                             return (Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t}));
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });
    cylinder3d.fillRandom(10, 10);
    tube_torus.fillRandom(10, 10);
    tube3d.fillRandom(10, 10);
}

TEST(Domain, DeletesOldOnRefill) {
    CircleDomain<Vec2d> cd2({-1, 2}, 5);
    cd2.fillUniformInterior(100);
    int size = cd2.positions.size();
    cd2.fillUniformInterior(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillUniformBoundary(100);
    size = cd2.positions.size();
    cd2.fillUniformBoundary(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillRandomInterior(100);
    size = cd2.positions.size();
    cd2.fillRandomInterior(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    cd2.fillRandomBoundary(100);
    size = cd2.positions.size();
    cd2.fillRandomBoundary(50);
    EXPECT_EQ(size - 50, cd2.positions.size());

    RectangleDomain<Vec2d> rd2({1, 3}, {1.2, 3.5});
    rd2.fillUniformInterior({10, 10});
    rd2.fillUniformInterior({50, 50});
    EXPECT_EQ(50u * 50, rd2.positions.size());

    size = rd2.positions.size();
    rd2.fillUniformBoundary(100);
    rd2.fillUniformBoundary(50);
    EXPECT_EQ(size + 4 * 50 - 4, rd2.positions.size());

    rd2.fillRandomInterior(100);
    size = rd2.positions.size();
    rd2.fillRandomInterior(50);
    EXPECT_EQ(size - 50, rd2.positions.size());

    rd2.fillRandomBoundary(100);
    size = rd2.positions.size();
    rd2.fillRandomBoundary(50);
    EXPECT_EQ(size - 50, rd2.positions.size());
}

TEST(Domain, Size) {
    RectangleDomain<Vec1d> domain({1.0}, {2.0});
    EXPECT_EQ(0u, domain.size());
    domain.positions = {1., 2., 3., 4.};
    EXPECT_EQ(4u, domain.size());
}

TEST(Domain, ContainsPrecision) {
    // test contains precision
    RectangleDomain<Vec1d> domain1d({1.0}, {2.0});
    domain1d.fillUniformInterior({10});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-8);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-6);
    domain1d.fillUniformInterior({100});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-9);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-7);
    domain1d.fillUniformInterior({1000});
    ASSERT_GT(domain1d.getContainsPrecision(), 1e-10);
    ASSERT_LT(domain1d.getContainsPrecision(), 1e-8);

    CircleDomain<Vec1d> domain1dlarge({0}, 10000);
    domain1dlarge.fillUniform(100, 100);
    ASSERT_GT(domain1dlarge.getContainsPrecision(), 1e-4);
    ASSERT_LT(domain1dlarge.getContainsPrecision(), 1e-3);
}

TEST(Domain, ClearTest) {
    CircleDomain<Vec2d> cd3({4, 0}, 4);
    cd3.fillUniform(20, 42);
    cd3.clear();
    EXPECT_TRUE(cd3.positions.empty());
    EXPECT_TRUE(cd3.types.empty());
    EXPECT_TRUE(cd3.support.empty());
    EXPECT_TRUE(cd3.distances.empty());

    cd3 = CircleDomain<Vec2d>({4, -2}, 4);
    cd3.fillUniform(20, 42);
    cd3.clearBoundaryNodes();
    EXPECT_EQ(20u, cd3.positions.size());
    EXPECT_EQ(20u, cd3.types.size());
    EXPECT_EQ(0u, cd3.support.size());
    EXPECT_EQ(0u, cd3.distances.size());

    cd3 = CircleDomain<Vec2d>({-2, 0}, 4);
    cd3.fillUniform(2, 2);
    cd3.support = {{1, 2}, {0, 3}, {0, 1}, {2, 3}};  // random values
    cd3.distances = {{0, 1}, {4, 5}, {3, 5}, {3, 2}};  // random values
    cd3.clearBoundaryNodes();
    EXPECT_EQ(2u, cd3.positions.size());
    EXPECT_EQ(2u, cd3.types.size());
    EXPECT_EQ(2u, cd3.support.size());
    EXPECT_EQ(2u, cd3.distances.size());

    cd3 = CircleDomain<Vec2d>({4, -2}, 4);
    cd3.fillUniform(20, 42);
    cd3.clearInternalNodes();
    EXPECT_EQ(42u, cd3.positions.size());
    EXPECT_EQ(42u, cd3.types.size());
    EXPECT_EQ(0u, cd3.support.size());
    EXPECT_EQ(0u, cd3.distances.size());

    cd3 = CircleDomain<Vec2d>({-2, 0}, 4);
    cd3.fillUniform(2, 2);
    cd3.support = {{1, 2}, {0, 3}, {0, 1}, {2, 3}};  // random values
    cd3.distances = {{0, 1}, {4, 5}, {3, 5}, {3, 2}};  // random values
    cd3.clearInternalNodes();
    EXPECT_EQ(2u, cd3.positions.size());
    EXPECT_EQ(2u, cd3.types.size());
    EXPECT_EQ(2u, cd3.support.size());
    EXPECT_EQ(2u, cd3.distances.size());

    Cylinder3D<Vec3d> cylinder3d({0, 1, 2}, {4, 5, 6}, 2);
    cylinder3d.fillUniform({10, 20, 30}, {11, 20, 32});
    size_t internal_count = 30 + 30 * (10 - 1) * 20;
    size_t boundary_count = 2 + 2 * (11 - 2) * 20 + 20 * 32;
    EXPECT_EQ(internal_count + boundary_count, cylinder3d.positions.size());
    EXPECT_EQ(internal_count + boundary_count, cylinder3d.types.size());
    cylinder3d.clearInternalNodes();
    EXPECT_EQ(boundary_count, cylinder3d.positions.size());
    EXPECT_EQ(boundary_count, cylinder3d.types.size());
    cylinder3d.clearBoundaryNodes();
    EXPECT_EQ(0, cylinder3d.positions.size());
    EXPECT_EQ(0, cylinder3d.types.size());

    Tube3D<Vec3d> tube3d(0, 1, 2, 10,
                         [](const double& t) -> Vec3d {
                             return (Vec3d({0, 0, 0}) + Vec3d({0, 0, 4 * t}));
                         },
                         [](const double&) -> Vec3d {
                             return Vec3d({0, 0, 4});
                         });
    tube3d.fillUniform({10, 20, 30}, {11, 20, 32});
    EXPECT_EQ(internal_count + boundary_count, tube3d.positions.size());
    EXPECT_EQ(internal_count + boundary_count, tube3d.types.size());
    tube3d.clearInternalNodes();
    EXPECT_EQ(boundary_count, tube3d.positions.size());
    EXPECT_EQ(boundary_count, tube3d.types.size());
    tube3d.clearBoundaryNodes();
    EXPECT_EQ(0, tube3d.positions.size());
    EXPECT_EQ(0, tube3d.types.size());
    // torus using Tube3D class
    double torus_larger_radius = 6;
    double torus_smaller_radius = 2;
    Tube3D<Vec3d> tube_torus(0, 2 * M_PI, torus_smaller_radius, 10000,
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({std::cos(t), std::sin(t), 0}) * torus_larger_radius;
                             },
                             [&torus_larger_radius](const double& t) -> Vec3d {
                                 return Vec3d({-std::sin(t), std::cos(t), 0}) * torus_larger_radius;
                             });
    internal_count = 30 + 1 + (30 + 1) * (10 - 1) * 20;
    boundary_count = 20 * 31;
    tube_torus.fillUniform({10, 20, 30}, {11, 20, 32});
    EXPECT_EQ(internal_count + boundary_count, tube_torus.positions.size());
    EXPECT_EQ(internal_count + boundary_count, tube_torus.types.size());
    tube_torus.clearInternalNodes();
    EXPECT_EQ(boundary_count, tube_torus.positions.size());
    EXPECT_EQ(boundary_count, tube_torus.types.size());
    tube_torus.clearBoundaryNodes();
    EXPECT_EQ(0, tube_torus.positions.size());
    EXPECT_EQ(0, tube_torus.types.size());
}

TEST(Domain, CloneEqual) {  // copying -- check that copies are independant
    RectangleDomain<Vec2d> rd({0, 0}, {4, 3});
    rd.fillRandom(5, 5);
    rd.add(RectangleDomain<Vec2d>({1, 1}, {5, 5}));

    auto rd_copy = Domain<Vec2d>::makeClone(rd);
    EXPECT_EQ(rd.positions, rd_copy.positions);
    EXPECT_EQ(rd.types, rd_copy.types);
    EXPECT_EQ(rd.support, rd_copy.support);
    EXPECT_EQ(rd.distances, rd_copy.distances);
    EXPECT_EQ(rd.getBBox(), rd_copy.getBBox());
    EXPECT_EQ(rd.child_domains.size(), rd_copy.child_domains.size());
    EXPECT_EQ(rd.getContainsPrecision(), rd_copy.getContainsPrecision());
    EXPECT_EQ(rd.getThickness(), rd_copy.getThickness());
    EXPECT_EQ(rd.beg, rd_copy.beg);
    EXPECT_EQ(rd.end, rd_copy.end);

    CircleDomain<Vec2d> cd({0, 0}, 1);
    cd.fillRandom(5, 5);
    cd.add(RectangleDomain<Vec2d>({1, 1}, {5, 5}));

    auto cd_copy = Domain<Vec2d>::makeClone(cd);
    EXPECT_EQ(cd.positions, cd_copy.positions);
    EXPECT_EQ(cd.types, cd_copy.types);
    EXPECT_EQ(cd.getBBox(), cd_copy.getBBox());
    EXPECT_EQ(cd.support, cd_copy.support);
    EXPECT_EQ(cd.distances, cd_copy.distances);
    EXPECT_EQ(cd.child_domains.size(), cd_copy.child_domains.size());
    EXPECT_EQ(cd.getContainsPrecision(), cd_copy.getContainsPrecision());
    EXPECT_EQ(cd.getThickness(), cd_copy.getThickness());
    EXPECT_EQ(cd.center, cd_copy.center);
    EXPECT_EQ(cd.radius, cd_copy.radius);
}

TEST(Domain, CloneIndependent) {  // copying -- check that copies are independant
    RectangleDomain<Vec2d> rd({0, 0}, {4, 3});
    rd.subtract(RectangleDomain<Vec2d>(Vec2d{0, 0}, Vec2d{1, 1}));
    rd.subtract(CircleDomain<Vec2d>(Vec2d{4, 3}, 2));
    CircleDomain<Vec2d> circle_rect_union({4, 3}, 1);
    circle_rect_union.add(rd);
    auto circle_rect_union_copy = Domain<Vec2d>::makeClone(circle_rect_union);
    EXPECT_EQ(circle_rect_union.positions, circle_rect_union_copy.positions);
    EXPECT_EQ(circle_rect_union_copy.child_domains.size(), circle_rect_union.child_domains.size());
    EXPECT_EQ(circle_rect_union.types, circle_rect_union_copy.types);
    EXPECT_TRUE(circle_rect_union_copy.valid());
    circle_rect_union_copy.add(CircleDomain<Vec2d>(Vec2d{-1, -1}, 1));
    EXPECT_EQ(circle_rect_union.child_domains.size() + 1,
              circle_rect_union_copy.child_domains.size());
    EXPECT_FALSE(circle_rect_union.contains({-1, -1}));
    EXPECT_TRUE(circle_rect_union_copy.contains({-1, -1}));
    EXPECT_TRUE(circle_rect_union_copy.contains({4.1, 3.3}));
    EXPECT_TRUE(circle_rect_union_copy.contains({1, 2}));
    EXPECT_FALSE(circle_rect_union_copy.contains({0.5, 0.5}));
    EXPECT_FALSE(circle_rect_union_copy.contains({3, 2}));
    EXPECT_FALSE(circle_rect_union_copy.contains({6.2, 3}));

    circle_rect_union_copy.positions.push_back({2, 3});
    circle_rect_union_copy.types.push_back(1);
    EXPECT_EQ(circle_rect_union_copy.positions.size(), circle_rect_union.positions.size() + 1);
    EXPECT_EQ(circle_rect_union_copy.types.size(), circle_rect_union.types.size() + 1);

    circle_rect_union.positions.push_back({1, 1});
    circle_rect_union.types.push_back(1);
    circle_rect_union.positions.push_back({1, 1});
    circle_rect_union.types.push_back(1);
    EXPECT_EQ(circle_rect_union.positions.size(), circle_rect_union_copy.positions.size() + 1);
    EXPECT_EQ(circle_rect_union_copy.types.size() + 1, circle_rect_union.types.size());
}

TEST(Domain, Subtract) {  // 2D obstacle
    CircleDomain<Vec2d> circ({2, 1}, 1);
    circ.positions = {{2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}, {1.5, 0.5}, {3, 1}, {2, 1.5}};
    circ.types = {-1, 1, -1, 1};
    assert(circ.valid());
    RectangleDomain<Vec2d> rect({0, 0}, {2, 1});
    rect.positions = {{1, 0.5}, {1.5, 0}, {1.5, 0.5}, {2, 0.5}};
    rect.types = {1, -1, 1, -1};
    assert(rect.valid());
    EXPECT_EQ(0u, rect.child_domains.size());
    rect.subtract(circ);
    assert(rect.valid());
    EXPECT_EQ(3u, rect.positions.size());
    EXPECT_EQ(1u, rect.child_domains.size());
    EXPECT_TRUE(rect.child_domains.front()->positions.empty());
    EXPECT_TRUE(rect.child_domains.front()->types.empty());
    EXPECT_TRUE(rect.child_domains.front()->child_domains.empty());
    Range<Vec2d> expected_boundary = {{1.5, 0}, {2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}};
    Range<Vec2d> boundary = rect.getBoundaryNodes();
    EXPECT_EQ(expected_boundary, expected_boundary);
    Range<Vec2d> expected_interior = {{1, 0.5}};
    Range<Vec2d> interior = rect.getInternalNodes();
    EXPECT_EQ(interior, expected_interior);
}

TEST(Domain, Add) {  // making a union in 2d
    CircleDomain<Vec2d> union_test({2, 1}, 1);
    RectangleDomain<Vec2d> rect({0, 0}, {2, 1});
    rect = RectangleDomain<Vec2d>({0, 0}, {2, 1});
    rect.positions = {{1, 0.5}, {1.5, 0}, {1.5, 0.5}, {2, 0.5}};
    rect.types = {1, -1, 1, -1};
    EXPECT_TRUE(rect.valid());
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(0u, union_test.child_domains.size());
    union_test.positions = {
        {2 - std::sqrt(2) / 2, 1 - std::sqrt(2) / 2}, {1.5, 0.5}, {3, 1}, {2, 1.5}};
    union_test.types = {-1, 1, -1, 1};
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(0u, union_test.child_domains.size());
    union_test.add(rect);
    EXPECT_TRUE(union_test.valid());
    EXPECT_EQ(1u, union_test.child_domains.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->child_domains.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->types.size());
    EXPECT_EQ(0u, union_test.child_domains.back()->positions.size());
    Range<Vec2d> expected_boundary = {{3, 1}, {1.5, 0}};
    Range<Vec2d> boundary = union_test.getBoundaryNodes();
    EXPECT_EQ(expected_boundary, boundary);
    Range<Vec2d> expected_interior = {{1.5, 0.5}, {2, 1.5}, {1, 0.5}, {1.5, 0.5}};
    Range<Vec2d> interior = union_test.getInternalNodes();
    EXPECT_EQ(interior, expected_interior);
}

//  TEST(Domain, Plot) { // Merging and removing 3D
//      CircleDomain<Vec3d> test1({1, 1, 1}, 0.5);
//      test1.fillUniformBoundary(400);
//      test1.fillUniformInterior(400);
//      CircleDomain<Vec3d> test5({2, 0, 0}, 0.5);
//      test5.fillUniformBoundary(400);
//      test5.fillUniformInterior(400);
//      RectangleDomain<Vec3d> test2({0, 0, 0}, {2, 2, 2});
//      test2.fillUniformBoundary({22, 22, 22});
//      test2.fillUniformInterior({20, 20, 20});
//      test2.subtract(test1);
//      test2.subtract(test5);

//      CircleDomain<Vec3d> test4({2, 0, 2}, 0.5);
//      test4.fillUniformBoundary(400);
//      test4.fillUniformInterior(400);
//      test4.add(test2);

//      std::cerr << test4.getMatlabData() << std::endl;
//  }

TEST(Domain, RemoveDuplicates) {
    /// internal -- boundary
    CircleDomain<Vec2d> duplicate_test({0, 0}, 1);
    duplicate_test.positions = {{0.5, 0.5}, {0.5, 0.5}};
    duplicate_test.types = {1, -1};
    duplicate_test.clearDuplicateNodes(1e-7);
    EXPECT_EQ((Range<Vec2d>{{0.5, 0.5}}), duplicate_test.positions);
    EXPECT_EQ(Range<int>{-1}, duplicate_test.types);

    /// internal -- boundary
    duplicate_test.positions = {{0.5, 0.5}, {0.5, 0.5}};
    duplicate_test.types = {-3, -1};
    duplicate_test.clearDuplicateNodes(1e-7);
    EXPECT_EQ((Range<Vec2d>{{0.5, 0.5}}), duplicate_test.positions);
    EXPECT_EQ(Range<int>{-1}, duplicate_test.types);

    /// internal -- boundary
    duplicate_test.positions = {{0.5, 0.5}, {0.5, 0.5}};
    duplicate_test.types = {1, 2};
    duplicate_test.clearDuplicateNodes(1e-7);
    EXPECT_EQ((Range<Vec2d>{{0.5, 0.5}}), duplicate_test.positions);
    EXPECT_EQ(Range<int>{2}, duplicate_test.types);

    /// internal -- boundary
    duplicate_test.positions = {{0.5, 0.5}, {0.5, 0.5}};
    duplicate_test.types = {-2, 1};
    duplicate_test.clearDuplicateNodes(1e-7);
    EXPECT_EQ((Range<Vec2d>{{0.5, 0.5}}), duplicate_test.positions);
    EXPECT_EQ(Range<int>{-2}, duplicate_test.types);

    /// jumble of everything
    duplicate_test.positions = {{0.5, 0.5}, {0.5, 0.5},     {1, 1},
                                {4, -6},    {1.001, 1.001}, {4, -6 + 1e-5}};
    duplicate_test.types = {-1, 1, 1, -1, -4, 3};
    duplicate_test.clearDuplicateNodes(1e-4);
    EXPECT_EQ(duplicate_test.positions,
              (Range<Vec2d>{{0.5, 0.5}, {1, 1}, {4, -6}, {1.001, 1.001}}));
    EXPECT_EQ((Range<int>{-1, 1, -1, -4}), duplicate_test.types);
}

TEST(Domain, UnionBorderSpecification) {
    RectangleDomain<Vec2d> border_test({0, 0}, {1, 1});
    RectangleDomain<Vec2d> border_test1({1, 0}, {2, 1});
    border_test1.fillUniform({1, 1}, {3, 3});

    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::SINGLE);
    EXPECT_EQ(13u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::DOUBLE);
    EXPECT_EQ(16u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.add(border_test1, BOUNDARY_TYPE::NONE);
    EXPECT_EQ(10u, border_test.getBoundaryNodes().size());
}

TEST(Domain, ObstacleBorderSpecification) {
    RectangleDomain<Vec2d> border_test({0, 0}, {1, 1});
    RectangleDomain<Vec2d> border_test1({1, 0}, {2, 1});
    border_test1.fillUniform({1, 1}, {3, 3});

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::SINGLE);
    EXPECT_EQ(8u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::DOUBLE);
    EXPECT_EQ(11u, border_test.getBoundaryNodes().size());

    border_test = RectangleDomain<Vec2d>({0, 0}, {1, 1});
    border_test.fillUniform({1, 1}, {3, 3});
    border_test.subtract(border_test1, BOUNDARY_TYPE::NONE);
    EXPECT_EQ(5u, border_test.getBoundaryNodes().size());
}

TEST(Domain, BBox) {
    RectangleDomain<Vec2d> d(Vec2d(0.), Vec2d(1.));
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d.getBBox());
    RectangleDomain<Vec2d> d2(Vec2d(1.), Vec2d(0.));
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d2.getBBox());
    CircleDomain<Vec2d> c(2., 1.);
    EXPECT_EQ(std::make_pair(Vec2d(1, 1), Vec2d(3, 3)), c.getBBox());

    d.subtract(d2);
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(1, 1)), d.getBBox());
    d.add(c);
    EXPECT_EQ(std::make_pair(Vec2d(0, 0), Vec2d(3, 3)), d.getBBox());

    RectangleDomain<Vec1d> d1d(Vec1d(0.), Vec1d(1.));
    EXPECT_EQ(std::make_pair(Vec1d(0.0), Vec1d(1.0)), d1d.getBBox());
    RectangleDomain<Vec1d> d1d2(Vec1d(-1.), Vec1d(0.));
    EXPECT_EQ(std::make_pair(Vec1d(-1.0), Vec1d(0.0)), d1d2.getBBox());
    d1d.add(d1d2);
    EXPECT_EQ(std::make_pair(Vec1d(-1.0), Vec1d(1.0)), d1d.getBBox());

    RectangleDomain<Vec3d> d3d(Vec3d(0.), Vec3d(1.));
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(1.0)), d3d.getBBox());
    RectangleDomain<Vec3d> d3d2(Vec3d(2.), Vec3d(0.));
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(2.0)), d3d2.getBBox());
    d3d.add(d3d2);
    EXPECT_EQ(std::make_pair(Vec3d(0.0), Vec3d(2.0)), d3d.getBBox());
}

TEST(Domain, DeathTest) {
    RectangleDomain<Vec2d> d(Vec2d(0.), Vec2d(1.));
    EXPECT_DEATH(d.fillUniformBoundary({0, 2}), "All counts must be greater than 2.");
    EXPECT_DEATH(d.fillUniformBoundary({2, 0}), "All counts must be greater than 2.");
    EXPECT_DEATH(CircleDomain<Vec3d>(2., -1.), "Circle radius must be greater than 0.");
    EXPECT_DEATH(CircleDomain<Vec3d>(2., 0.), "Circle radius must be greater than 0.");
    EXPECT_DEATH(d.fillUniformInteriorWithStep({0, 1}),
                 "Step in interior discretization of rectangle domain is too small!");
    EXPECT_DEATH(d.fillUniformBoundaryWithStep({1, 0}),
                 "Step in boundary discretization of rectangle domain is too small!");
    EXPECT_DEATH(RectangleDomain<Vec2d>({{2, 3}, {5, 7}}, {1, 2, 3}),
                 "Lengths of positions and types must match in RectangleDomain constructor.");
    EXPECT_DEATH(RectangleDomain<Vec2d>({{2, 3}, {5, 7}}, Range<int>{-1}),
                 "Lengths of positions and types must match in RectangleDomain constructor.");
    EXPECT_DEATH(RectangleDomain<Vec2d>(Range<Vec2d>(), 5), "Add at least some points to domain.");
    EXPECT_DEATH(Cylinder3D<Vec2d>(Vec2d(), Vec2d(), 5),
                 "Currently Cyllinder3D only works in 3 dimensions");
    EXPECT_DEATH(Cylinder3D<Vec3d>({1, 2, 3}, {1, 2, 3}, 0),
                 "Radius of a cylinder must be greater than 0.");
    EXPECT_DEATH(Cylinder3D<Vec3d>({1, 2, 3}, {1, 2, 3}, 2),
                 "Starting and ending points of a cylinder should be different.");
    Cylinder3D<Vec3d> cylinder({0, 0, 0}, {1, 1, 1}, 1);
    EXPECT_DEATH(cylinder.fillUniformInteriorWithStep({0, 1, 0}),
                 "Step in interior discretization of rectangle domain is too small!");
    Tube3D<Vec3d> tube(0, M_PI, 1, 10000,
                       [](const double a) {
                           return Vec3d({std::sin(a), std::cos(a), 1});
                       },
                       [](const double a) {
                           return Vec3d({std::cos(a), -std::sin(a), 0});
                       });
    EXPECT_DEATH(tube.fillUniformInteriorWithStep({0, 1, 0}),
                 "Step in interior discretization of rectangle domain is too small!");
    EXPECT_DEATH(Tube3D<Vec3d>({2, 3, 4},
                               [](const double&) {
                                   return Vec3d({0, 0, 0});
                               },
                               [](const double&) {
                                   return Vec3d({0, 0, 0});
                               }),
                 "Incorrect number of parameters for Tube constructor.");
    EXPECT_DEATH(Tube3D<Vec2d> tube2d({2, 3, 4, 5},
                                      [](const double&) {
                                          return Vec2d({0, 0, 0});
                                      },
                                      [](const double&) {
                                          return Vec2d({0, 0, 0});
                                      }),
                 "Tube3D works only in 3 dimensions.");
}

TEST(Domain, DISABLED_DeathStar) {
    CircleDomain<Vec3d> death_star({0, 0, 0}, 5);
    death_star.fillUniform(10000, 1000);
    CircleDomain<Vec3d> biteoff({4.6, 5, 0}, 3.24);
    biteoff.fillUniform(400, 400);
    death_star.subtract(biteoff);
}

TEST(Domain, DISABLED_SphereInt) {
    CircleDomain<Vec3d> d(0., 1.);
    int n = 10000;
    for (int i = 1; i < n; ++i) {
        d.fillUniformInterior(i);
        std::cout << i << ' ' << d.positions.size() << std::endl;
    }
}

}  // namespace mm
