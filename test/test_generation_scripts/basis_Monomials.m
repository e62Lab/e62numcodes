clc;clear
powers=[0, 0;0, 1;1, 0;0, 2;1, 1;2, 0];
val = @(x,y,sx,sy,i) (x/sx)^powers(i,1)*(y/sy)^powers(i,2) ;
val_d11 = @(x,y,sx,sy,i) (x/sx)^1*(y/sy)^1; %4
val_d02 = @(x,y,sx,sy,i) (x/sx)^0*(y/sy)^2; %3
val_d20 = @(x,y,sx,sy,i) (x/sx)^2*(y/sy)^0; %5


% val = @(x,y,sx,sy,i) (x/sx)*(y/sx) ;
sx=4.34;
sy=2.54;


 disp('TEST(BasisFunc, MonomialsShaped) { ')
 disp(['Monomials<Vec2d> g({',num2str(sx),', ',num2str(sy),'}, 3);'])
 for x=[0.45,1.55]
 for y=[0.22,5.4]
 for i=[1:6]
 disp(['EXPECT_NEAR(',num2str(val(x,y,sx,sy,i),'%5.7f'),...
     ', g(',num2str(i-1,'%i'),', {', num2str(x),', ', num2str(y),'}','), 1e-6);'])
 end
 end
 end
disp('}');
%%
sx=4.34;
sy=2.54;
disp('TEST(BasisFunc, MonomialsShapedDerivatives) { ')

    disp(['Monomials<Vec2d> g({',num2str(sx),', ',num2str(sy),'}, 3);'])

    syms x y;f=diff(val_d11,x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(4, {', num2str(x),', ', num2str(y),'}',', {1, 0}), 1e-6);'])
 
    syms x y;f=diff(val_d11,y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(4, {', num2str(x),', ', num2str(y),'}',', {0, 1}), 1e-6);'])
    
    syms x y;f=diff(diff(val_d11,x),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(4, {', num2str(x),', ', num2str(y),'}',', {1, 1}), 1e-6);'])
 
    syms x y;f=diff(diff(val_d11,x),x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(4, {', num2str(x),', ', num2str(y),'}',', {2, 0}), 1e-6);'])

    syms x y;f=diff(diff(val_d11,y),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(4, {', num2str(x),', ', num2str(y),'}',', {0, 2}), 1e-6);'])
 
%%%%%%%%%%%%%%%%%%%%

    syms x y;f=diff(val_d02,x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(3, {', num2str(x),', ', num2str(y),'}',', {1, 0}), 1e-6);'])
 
    syms x y;f=diff(val_d02,y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(3, {', num2str(x),', ', num2str(y),'}',', {0, 1}), 1e-6);'])
    
    syms x y;f=diff(diff(val_d02,x),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(3, {', num2str(x),', ', num2str(y),'}',', {1, 1}), 1e-6);'])
 
    syms x y;f=diff(diff(val_d02,x),x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(3, {', num2str(x),', ', num2str(y),'}',', {2, 0}), 1e-6);'])

    syms x y;f=diff(diff(val_d02,y),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(3, {', num2str(x),', ', num2str(y),'}',', {0, 2}), 1e-6);'])  
%%%%%%%%%%%%%%%%%%%
    syms x y;f=diff(val_d20,x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(5, {', num2str(x),', ', num2str(y),'}',', {1, 0}), 1e-6);'])
 
    syms x y;f=diff(val_d20,y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(5, {', num2str(x),', ', num2str(y),'}',', {0, 1}), 1e-6);'])
    
    syms x y;f=diff(diff(val_d20,x),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(5, {', num2str(x),', ', num2str(y),'}',', {1, 1}), 1e-6);'])
 
    syms x y;f=diff(diff(val_d20,x),x);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(5, {', num2str(x),', ', num2str(y),'}',', {2, 0}), 1e-6);'])

    syms x y;f=diff(diff(val_d20,y),y);x=0.45;y=2.54;f=eval(subs(f));
    disp(['EXPECT_NEAR(',num2str(f,'%5.7f'),...
     ', g(5, {', num2str(x),', ', num2str(y),'}',', {0, 2}), 1e-6);'])  
disp('}');
