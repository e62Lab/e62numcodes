#include "shape_storage.hpp"  // should not use any other user-defined libraries

#include "domain.hpp"
#include "mls.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(ShapeStorage, Uniform) {
    RectangleDomain<Vec1d> domain(0., 1.);
    domain.positions = {-1.0, -0.5, 0.0, 0.5, 1.0};
    domain.types = {-1, 1, 1, 1, -1};
    domain.support = {{0, 1, 2}, {1, 0, 2}, {2, 1, 3}, {3, 2, 4}, {4, 3, 2}};

    EngineMLS<Vec1d, Monomials, Monomials> engine(3, 1);

    auto storage = make_shapes(domain, engine);

    int node_idx = 2;
    engine.setSupport(domain.positions[domain.support[node_idx]]);

    EXPECT_EQ(3, storage.support_size(node_idx));
    EXPECT_EQ(2, storage.support(node_idx, 0));
    EXPECT_EQ(1, storage.support(node_idx, 1));
    EXPECT_EQ(3, storage.support(node_idx, 2));

    Eigen::VectorXd shape = engine.getShapeAt(0.0, {2});
    EXPECT_EQ(shape[0], storage.d2(0, 0, node_idx, 0));  // d^2/dx^2
    EXPECT_EQ(shape[1], storage.d2(0, 0, node_idx, 1));
    EXPECT_EQ(shape[2], storage.d2(0, 0, node_idx, 2));
}

TEST(ShapeStorage, Ragged) {
    RectangleDomain<Vec1d> domain(0., 1.);
    domain.positions = {-1.0, -0.5, 0.0, 0.5, 1.0};
    domain.types = {-1, 1, 1, 1, -1};
    domain.support = {{0, 1, 2}, {1, 0, 2, 3}, {2, 1, 3, 0, 4}, {3, 2, 4}, {4, 3, 2}};

    EngineMLS<Vec1d, Monomials, Monomials> engine(3, 1);

    auto storage = make_ragged_shapes(domain, engine);

    int node_idx = 2;
    engine.setSupport(domain.positions[domain.support[node_idx]]);

    EXPECT_EQ(5, storage.support_size(node_idx));
    EXPECT_EQ(2, storage.support(node_idx, 0));
    EXPECT_EQ(1, storage.support(node_idx, 1));
    EXPECT_EQ(3, storage.support(node_idx, 2));
    EXPECT_EQ(0, storage.support(node_idx, 3));
    EXPECT_EQ(4, storage.support(node_idx, 4));

    Eigen::VectorXd shape = engine.getShapeAt(0.0, {2});
    EXPECT_EQ(shape[0], storage.d2(0, 0, node_idx, 0));  // d^2/dx^2
    EXPECT_EQ(shape[1], storage.d2(0, 0, node_idx, 1));
    EXPECT_EQ(shape[2], storage.d2(0, 0, node_idx, 2));
    EXPECT_EQ(shape[3], storage.d2(0, 0, node_idx, 3));
    EXPECT_EQ(shape[4], storage.d2(0, 0, node_idx, 4));
}

}  // namespace mm
