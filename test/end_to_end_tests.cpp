#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "draw.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "integrators.hpp"
#include <Eigen/Sparse>

#include "gtest/gtest.h"

namespace mm {

class DiffusionExplicitTest : public ::testing::Test {
  public:
    double dx;
    int n, m;
    double time, dt, t_steps;
    double sigma;
    RectangleDomain<Vec2d> domain;
    Range<int> interior, boundary;
    MLSM<UniformShapeStorage<RectangleDomain<Vec2d>,
            EngineMLS<Vec2d, Monomials, NNGaussians>, mlsm::lap>> op;
    Range<VecXd> shape_laplace;

    DiffusionExplicitTest() : dx(1. / 50.), n(12), m(3), time(0.05), dt(1e-5),
                              t_steps(std::ceil(time / dt)), sigma(1.0), domain(prep_domain()),
                              interior(domain.types > 0), boundary(domain.types < 0),
                              op(prep_operators()), shape_laplace(prep_shapes()) {}
  protected:
    RectangleDomain<Vec2d> prep_domain() {
        // prep domain
        RectangleDomain<Vec2d> d({0, 0}, {1, 1});
        d.fillUniformWithStep(dx, dx);
        d.findSupport(n);
        return d;
    }

    MLSM<UniformShapeStorage<RectangleDomain<Vec2d>,
            EngineMLS<Vec2d, Monomials, NNGaussians>, mlsm::lap>> prep_operators() {
        // prep operators
        EngineMLS<Vec2d, Monomials, NNGaussians> mls(m, sigma);
        return make_mlsm<mlsm::lap>(domain, mls, interior, false);
    }

    Range<VecXd> prep_shapes() {
        // prep shape funcs
        Range<VecXd>shapes(domain.size());
        for (auto& c : interior) {
            Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
            EngineMLS<Vec2d, Monomials, NNGaussians> MLS(m, supp_domain, sigma*dx);
            shapes[c] = MLS.getShapeAt(supp_domain[0], {2, 0}) +
                        MLS.getShapeAt(supp_domain[0], {0, 2});
        }
        return shapes;
    }

    static double analytical(const Vec2d& pos, double t, double a, double D, size_t N) {
        double T = 0;
        double f = M_PI / a;
        for (size_t n = 1; n < N; n = n + 2) {
            for (size_t m = 1; m < N; m = m + 2) {
                T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                     std::exp(-D * t * ((n * n + m * m) * f * f));
            }
        }
        return T;
    }

    double LinfError(double t, const VecXd& value) {
        Range<double> E2(domain.size(), 0);
        for (auto& c : interior) {
            E2[c] = std::abs(value[c] - analytical(domain.positions[c], t, 1, 1, 50));
        }
        return *std::max_element(E2.begin(), E2.end());
    }

  public:
    double solveBasic() {
        VecXd T1 = VecXd::Zero(domain.size());
        T1[domain.types > 0] = 1.0;
        T1[domain.types < 0] = 0.0;
        VecXd T2 = T1;
        int tt;
        for (tt = 0; tt < t_steps; ++tt) {
            // new temperature
            for (auto& c : interior) {
                double Lap = 0;
                for (int i = 0; i < n; ++i) Lap += shape_laplace[c][i] * T1[domain.support[c]][i];
                T2[c] = T1[c] + dt * Lap;
            }
            // time advance
            T1.swap(T2);
        }
        return LinfError(tt*dt, T1);
    }
    double solveOperators() {
        VecXd T1 = VecXd::Zero(domain.size());
        T1[domain.types > 0] = 1.0;
        VecXd T2 = T1;

        int tt;
        for (tt = 0; tt < t_steps; ++tt) {
            // new temp.
            for (auto& c : interior) {
                T2[c] = T1[c] + dt * op.lap(T1, c);
            }
            // time advance
            T1.swap(T2);
        }
        return LinfError(tt*dt, T1);
    }
    double solveRKEuler() {
        auto dv_dt = [&](double, const VecXd& y) {
            Eigen::VectorXd der(y.size());
            for (int c : interior) {
                der[c] = op.lap(y, c);
            }
            for (int c : boundary) {
                der[c] = 0;
            }
            return der;
        };

        VecXd T1 = VecXd::Zero(domain.size());
        T1[domain.types > 0] = 1.0;

        auto integrator = integrators::Explicit::Euler().solve(dv_dt, 0.0, time, dt, T1);
        auto stepper = integrator.begin();
        while (stepper) {
            ++stepper;
        }

        return LinfError(stepper.time(), stepper.value());
    }

    double solveABEuler() {
        auto dv_dt = [&](double, const VecXd& y) {
            Eigen::VectorXd der(y.size());
            for (int c : interior) {
                der[c] = op.lap(y, c);
            }
            for (int c : boundary) {
                der[c] = 0;
            }
            return der;
        };

        VecXd T1 = VecXd::Zero(domain.size());
        T1[domain.types > 0] = 1.0;

        auto integrator = integrators::ExplicitMultistep::AB1().solve(dv_dt, 0.0, time, dt, T1);
        auto stepper = integrator.begin();
        while (stepper) {
            ++stepper;
        }

        return LinfError(stepper.time(), stepper.value());
    }
};


TEST_F(DiffusionExplicitTest, IntegratorsMatch) {
    double err_basic = solveBasic();
    double err_op = solveOperators();
    double err_rk = solveRKEuler();
    double err_ab = solveABEuler();

    double tol = 1e-5;
    EXPECT_NEAR(err_basic, err_op, tol);
    EXPECT_NEAR(err_basic, err_rk, tol);
    EXPECT_NEAR(err_rk, err_ab, tol);
    EXPECT_NEAR(err_ab, err_basic, tol);
    EXPECT_LE(err_basic, 5e-4);
}

class DiffusionRefineRelaxTest : public ::testing::Test {
  public:
    template <template <typename> class BasisType>
    double solve(const BasisType<Vec2d>& basis, int num_nodes_x, double sigmaW = 1.0) {
        double rad = 1;
        CircleDomain<Vec2d> domain(0, rad);
        domain.fillUniform(iceil(M_PI*rad*rad*num_nodes_x*num_nodes_x),
                           iceil(2*M_PI*rad*num_nodes_x));

        // Closed form solution -- refer to cantilever_beam.nb for reference.
        std::function<double(Vec2d)> analytical = [=](const Vec2d& p) {
            return 0.25*(1.0-p.squaredNorm());
        };


        BasicRelax relax; relax.iterations(10);
        HalfLinksRefine refine;
        domain.apply(relax);
        domain.findSupport(9);
        refine.region(domain.positions.filter([] (const Vec2d& v) { return v[1] > 0; }));
        domain.apply(refine);
        domain.apply(relax);
        domain.findSupport(9);
        refine.region(domain.positions.filter([] (const Vec2d& v) { return v[1] > 0; }));
        domain.apply(refine);
        relax.iterations(100);
        domain.apply(relax);

        int support_size = 13;
        domain.findSupport(support_size);

        NNGaussians<Vec2d> gau(sigmaW);
        EngineMLS<Vec2d, BasisType, NNGaussians> mls(basis, gau);
        auto op = make_mlsm<mlsm::lap>(domain, mls, domain.types > 0, false);

        int N = domain.size();
        Eigen::SparseMatrix<double> M(N, N);
        M.reserve(Range<int>(2 * N, 2 * support_size));
        Eigen::VectorXd rhs(N);
        rhs.setZero();
        for (int i : (domain.types > 0)) {
            op.lap(M, i, -1.0);
            rhs(i) = 1.0;
        }
        for (int i : (domain.types < 0)) {
            op.value(M, i, 1.0);
            rhs(i) = 0.0;
        }

        M.makeCompressed();
        Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
        solver.compute(M);
        Eigen::VectorXd sol = solver.solve(rhs);

        Range<double> error(N);
        for (int i = 0; i < N; ++i) {
            double anal = analytical(domain.positions[i]);
            error[i] = std::abs(sol(i) - anal);
        }

        double L_inf_error = *std::max_element(error.begin(), error.end());
        return L_inf_error;
    }
};

TEST_F(DiffusionRefineRelaxTest, Monomials) {
    Monomials<Vec2d> mon5({{0, 0}, {1, 0}, {2, 0}, {0, 1}, {0, 2}});
    int nx = 10;
    double err = solve(mon5, nx);
    EXPECT_LE(err, 1e-6);

    nx = 15;
    err = solve(mon5, nx);
    EXPECT_LE(err, 1e-6);
}

class CantileverBeamTest : public ::testing::Test {
  public:
    // Physical parameters
    static constexpr double E = 72.1e9;
    static constexpr double v = 0.33;
    static constexpr double P = 1000;
    static constexpr double D = 5;
    static constexpr double L = 30;

    // Numerical parameters
    static constexpr double solver_tolerance = 1e-15;
    static constexpr int max_iterations = 50;
    static constexpr double drop_tolerance = 1e-5;
    static constexpr int fill_factor = 20;

    typedef Eigen::SparseMatrix<double, Eigen::RowMajor> matrix_t;
    Eigen::BiCGSTAB<matrix_t, Eigen::IncompleteLUT<double>> solver;

    template <template <typename> class BasisType>
    double solve(const BasisType<Vec2d>& basis, int num_nodes_x, double sigmaW = 1.0) {
        // Derived parameters
        const double I = D * D * D / 12;
        double lam = E * v / (1 - 2 * v) / (1 + v);
        const double mu = E / 2 / (1 + v);
        lam = 2 * mu * lam / (2 * mu + lam);  // plane stress

        // Closed form solution -- refer to cantilever_beam.nb for reference.
        std::function<Vec2d(Vec2d)> analytical = [=](const Vec2d& p) {
            double x = p[0], y = p[1];
            double ux = (P*y*(3*D*D*(1+v)-4*(3*L*L-3*x*x+(2+v)*y*y))) / (2.*D*D*D*E);
            double uy = -(P*(3*D*D*(1+v)*(L-x) + 4*(L-x)*(L-x)*(2*L+x)+12*v*x*y*y)) / (2.*D*D*D*E);
            return Vec2d(ux, uy);
        };

        // Domain definition
        Vec2d low(0, -D / 2), high(L, D / 2);
        RectangleDomain<Vec2d> domain(low, high);
        double step = L / num_nodes_x;
        domain.fillUniformWithStep(step, step);
        int N = domain.size();

        // Set indices for different domain parts
        double tol = 1e-10;
        Range<int> internal = domain.types > 0, boundary = domain.types < 0,
                   top = domain.positions.filter(
                       [=](const Vec2d& p) { return std::abs(p[1] - high[1]) < tol; }),
                   bottom = domain.positions.filter(
                       [=](const Vec2d& p) { return std::abs(p[1] - low[1]) < tol; }),
                   right = domain.positions.filter([=](const Vec2d& p) {
                       return std::abs(p[0] - high[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                              std::abs(p[1] - low[1]) > tol;
                   }),
                   left = domain.positions.filter([=](const Vec2d& p) {
                       return std::abs(p[0] - low[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                              std::abs(p[1] - low[1]) > tol;
                   }),
                   all = domain.types != 0;
        int support_size = 9;
        domain.findSupport(support_size);

        // Approximation
        NNGaussians<Vec2d> weight(sigmaW * domain.characteristicDistance());
        EngineMLS<Vec2d, BasisType, NNGaussians> mls(basis, weight);

        // Initialize operators on all nodes
        auto op = make_mlsm(domain, mls, all);

        matrix_t M(2 * N, 2 * N);
        M.reserve(Range<int>(2 * N, 2 * support_size));
        Eigen::VectorXd rhs(2 * N);

        // Set equation on interior
        for (int i : internal) {
            op.graddiv(M, i, lam + mu);  // graddiv + laplace in interior
            op.lapvec(M, i, mu);
            rhs(i) = 0;
            rhs(i + N) = 0;
        }

        // Set bottom boundary conditions - traction free
        for (int i : bottom) {
            op.traction(M, i, lam, mu, {0, -1});
            rhs(i) = 0;
            rhs(i + N) = 0;
        }

        // Set left boundary conditions - given traction
        for (int i : left) {
            op.traction(M, i, lam, mu, {-1, 0});
            rhs(i) = 0;
            double y = domain.positions[i][1];
            rhs(i + N) = -P * (D * D - 4 * y * y) / (8. * I);
        }

        // Set right boundary conditions - given displacement
        for (int i : right) {
            double y = domain.positions[i][1];
            M.coeffRef(i, i) = 1;
            rhs(i) = (P * y * (3 * D * D * (1 + v) - 4 * (2 + v) * y * y)) / (24. * E * I);
            M.coeffRef(i + N, i + N) = 1;
            rhs(i + N) = -(L * v * P * y * y) / (2. * E * I);
        }

        // set top boundary conditions - traction free
        for (int i : top) {
            op.traction(M, i, lam, mu, {0, 1});
            rhs(i) = 0;
            rhs(i + N) = 0;
        }
        M.makeCompressed();

        solver.preconditioner().setDroptol(drop_tolerance);
        solver.preconditioner().setFillfactor(fill_factor);
        solver.setMaxIterations(max_iterations);
        solver.setTolerance(solver_tolerance);
        solver.compute(M);
        Eigen::VectorXd sol = solver.solve(rhs);

        Range<Vec2d> displacement(N, 0);
        std::vector<std::array<double, 3>> stress_field(N);
        for (int i : all) {
            displacement[i] = Vec2d({sol[i], sol[i + N]});
        }
        for (int i : all) {
            auto grad = op.grad(displacement, i);
            stress_field[i][0] = (2 * mu + lam) * grad(0, 0) + lam * grad(1, 1);
            stress_field[i][1] = lam * grad(0, 0) + (2 * mu + lam) * grad(1, 1);
            stress_field[i][2] = mu * (grad(0, 1) + grad(1, 0));
        }

        Range<double> error(N);
        double maxuv = 0;
        for (int i = 0; i < N; ++i) {
            Vec2d uv = analytical(domain.positions[i]);
            maxuv = std::max(maxuv, std::max(std::abs(uv[0]), std::abs(uv[1])));
            error[i] = std::max(std::abs(uv[0] - sol[i]), std::abs(uv[1] - sol[i + N]));
        }

        double L_inf_error = *std::max_element(error.begin(), error.end()) / maxuv;
        return L_inf_error;
    }
};
constexpr double CantileverBeamTest::solver_tolerance;
constexpr double CantileverBeamTest::drop_tolerance;
constexpr double CantileverBeamTest::L;

TEST_F(CantileverBeamTest, Monomials) {
    Monomials<Vec2d> mon9({{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}});
    int nx = 100;
    double err = solve(mon9, nx);
    EXPECT_LE(err, 1.9e-2);
    EXPECT_LE(solver.iterations(), 8);
    EXPECT_LE(solver.error(), solver_tolerance);

    nx = 200;
    err = solve(mon9, nx);
    EXPECT_LE(err, 5e-3);
    EXPECT_LE(solver.iterations(), 12);
    EXPECT_LE(solver.error(), solver_tolerance);
}

TEST_F(CantileverBeamTest, MonomialsNoWeight) {
    Monomials<Vec2d> mon9({{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}});
    int nx = 100;
    double err = solve(mon9, nx, 10000.0);
    EXPECT_LE(err, 1.9e-2);
    EXPECT_LE(solver.iterations(), 8);
    EXPECT_LE(solver.error(), solver_tolerance);

    nx = 200;
    err = solve(mon9, nx, 10000.0);
    EXPECT_LE(err, 5e-3);
    EXPECT_LE(solver.iterations(), 20);
    EXPECT_LE(solver.error(), solver_tolerance);
}

TEST_F(CantileverBeamTest, GaussiansLowAccuracy) {
    int nx = 100;
    double sigmaB = 400.0 * L / nx;
    NNGaussians<Vec2d> gau9(sigmaB, 9);
    double err = solve(gau9, nx);
    EXPECT_LE(err, 2.5e-2);
    EXPECT_LE(solver.iterations(), 8);
    EXPECT_LE(solver.error(), solver_tolerance);
}

TEST_F(CantileverBeamTest, GaussiansHighAccuracy) {
    int nx = 200;
    double sigmaB = 400.0 * L / nx;
    NNGaussians<Vec2d> gau9(sigmaB, 9);
    double err = solve(gau9, nx);
    EXPECT_LE(err, 8e-3);
    EXPECT_LE(solver.iterations(), 12);
    EXPECT_LE(solver.error(), solver_tolerance);
}

class HertzianContactTest : public ::testing::Test {
  public:
    // Physical parameters
    static constexpr double E = 72.1e9;
    static constexpr double v = 0.33;
    static constexpr double R = 1.0;
    static constexpr double P = 543;

    // Numerical parameters
    static constexpr double solver_tolerance = 1e-15;
    static constexpr int max_iterations = 10000;
    static constexpr double drop_tolerance = 1e-6;
    static constexpr int fill_factor = 100;

    typedef Eigen::SparseMatrix<double, Eigen::RowMajor> matrix_t;
    Eigen::BiCGSTAB<matrix_t, Eigen::IncompleteLUT<double>> solver;

    template <template <typename> class BasisType>
    double solve(const BasisType<Vec2d>& basis, int num_nodes_x, double sigmaW = 1.0) {
        double mu = E / 2. / (1+v);
        double lam = E * v / (1-2*v) / (1+v);
        double Estar = E / (2*(1-v*v));
        double b = 2*std::sqrt(std::abs(P*R/(M_PI*Estar)));
        double p0 = std::sqrt(std::abs(P*Estar/(M_PI*R)));

        // Closed form solution
        std::function<Vec3d(Vec2d)> analytical = [=] (const Vec2d& p) {
            double x = p[0], z = p[1];
            double bxz = b*b - x*x + z*z;
            double xz = 4*x*x*z*z;
            double koren = std::sqrt(bxz*bxz + xz);
            double m2 = 0.5 * (koren + bxz);
            double n2 = 0.5 * (koren - bxz);
            double m = std::sqrt(m2);
            double n = signum(x) * std::sqrt(n2);
            double mpn = m2 + n2;
            double zmn = (z*z + n2) / mpn;
            double sxx = -p0 / b * (m * (1 + zmn) + 2*z);
            double syy = -p0 / b * m * (1 - zmn);
            double sxy = p0 / b * n * ((m2 - z*z) / mpn);
            return Vec3d(sxx, syy, sxy);
        };

        // Domain definition
        double H = 1;
        Vec2d low(-H, -H), high(H, 0);
        RectangleDomain<Vec2d> domain(low, high);
        double dy = H / num_nodes_x;
        domain.fillUniformWithStep(dy, dy);
        int support_size = 9;

        // Refine
        std::vector<double> lengths = {1000, 500, 200, 100, 50, 20, 10, 5, 4, 3, 2};
        Vec2d center = {0, 0};
        HalfLinksRefine refine(0.4);
        for (double l : lengths) {
            refine.region(domain.positions.filter([&] (const Vec2d& x) {
                return std::max(std::abs(x[0]-center[0]), std::abs(x[1] - center[1])) < l*b;
            }));
            domain.findSupport(support_size);
            domain.apply(refine);
        }

        // Set indices for different domain parts
        static const double tol = 1e-10;
        Range<int> internal = domain.types > 0, boundary = domain.types < 0,
                   top = domain.positions.filter(
                       [=](const Vec2d& p) { return std::abs(p[1] - high[1]) < tol; }),
                   bottom = domain.positions.filter(
                       [=](const Vec2d& p) { return std::abs(p[1] - low[1]) < tol; }),
                   right = domain.positions.filter([=](const Vec2d& p) {
                       return std::abs(p[0] - high[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                              std::abs(p[1] - low[1]) > tol;
                   }),
                   left = domain.positions.filter([=](const Vec2d& p) {
                       return std::abs(p[0] - low[0]) < tol && std::abs(p[1] - high[1]) > tol &&
                              std::abs(p[1] - low[1]) > tol;
                   }),
                   all = domain.types != 0;

        int N = domain.size();
        domain.findSupport(support_size);

        NNGaussians<Vec2d> weight(sigmaW);
        EngineMLS<Vec2d, BasisType, NNGaussians> mls(basis, weight);

        // Initialize operators on all nodes
        auto op = make_mlsm(domain, mls, all, false);

        matrix_t M(2 * N, 2 * N);
        M.reserve(Range<int>(2*N, 2 * support_size));
        Eigen::VectorXd rhs(2*N);

        // Set equation on interior
        for (int i : internal) {
            op.lapvec(M, i, mu);
            op.graddiv(M, i, lam + mu);  // graddiv + laplace in interior
            rhs(i) = 0;
            rhs(i+N) = 0;
        }

        // Set bottom boundary conditions
        for (int i : bottom) {
            M.coeffRef(i, i) = 1;
            rhs(i) = 0;
            M.coeffRef(i+N, i+N) = 1;
            rhs(i+N) = 0;
        }

        // Set left boundary conditions
        for (int i : left) {
            M.coeffRef(i, i) = 1;
            rhs(i) = 0;
            M.coeffRef(i+N, i+N) = 1;
            rhs(i+N) = 0;
        }

        // Set right boundary conditions
        for (int i : right) {
            M.coeffRef(i, i) = 1;
            rhs(i) = 0;
            M.coeffRef(i+N, i+N) = 1;
            rhs(i+N) = 0;
        }

        for (int i : top) {
            // traction in x-direction
            op.der1(M, 0, 1, i, mu, 0);
            op.der1(M, 1, 0, i, mu, 0);
            // traction in y-direction
            op.der1(M, 0, 0, i, lam, 1);
            op.der1(M, 1, 1, i, 2. * mu + lam, 1);
            double x = std::abs(domain.positions[i][0]);
            double trac = (x <= b) ? -p0 * std::sqrt(1 - x*x/b/b) : 0.0;
            rhs(i) = 0;
            rhs(i+N) = trac;
        }
        M.makeCompressed();

        // Solve the system
        solver.preconditioner().setDroptol(drop_tolerance);
        solver.preconditioner().setFillfactor(fill_factor);
        solver.compute(M);

        solver.setMaxIterations(max_iterations);
        solver.setTolerance(solver_tolerance);
        Eigen::VectorXd sol = solver.solve(rhs);

        // Postprocess, calculate the stresses
        Range<Vec2d> disp(N, 0);
        Range<Vec3d> stress(N);
        for (int i : all) {
            disp[i] = Vec2d({sol[i], sol[i+N]});
        }
        for (int i : all) {
            auto grad = op.grad(disp, i);
            stress[i][0] = (2*mu + lam)*grad(0, 0) + lam*grad(1, 1);
            stress[i][1] = lam*grad(0, 0) + (2*mu+lam)*grad(1, 1);
            stress[i][2] = mu*(grad(0, 1)+grad(1, 0));
        }

        // Compare against analytical solution
        Range<double> error(N);
        for (int i = 0; i < N; ++i) {
            if (domain.positions[i].norm() < 3*b) {  // error under contact
                Vec3d astress = analytical(domain.positions[i]);
                error[i] = (stress[i] - astress).cwiseAbs().maxCoeff() / p0;
            }
        }
        double L_inf_error = *std::max_element(error.begin(), error.end());
        return L_inf_error;
    }
};
constexpr double HertzianContactTest::solver_tolerance;
constexpr double HertzianContactTest::drop_tolerance;

TEST_F(HertzianContactTest, GaussiansRefine) {
    int nx = 50;
    double sigmaB = 400.0;
    NNGaussians<Vec2d> gau9(sigmaB, 9);
    double err = solve(gau9, nx);
    EXPECT_LE(err, 0.05);
    EXPECT_LE(solver.iterations(), 45);
    EXPECT_LE(solver.error(), solver_tolerance);
}

}  // namespace mm
