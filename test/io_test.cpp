#include "io.hpp"
#include "common.hpp"
#include "types.hpp"
#include "Eigen/Sparse"

#include "gtest/gtest.h"
#include <cstdint>

namespace mm {

TEST(IO, XMLRead) {
    XMLloader xml("test/data/test_xml.xml");
    EXPECT_EQ("low level test", xml.textRead({"test", "mls", "weight"}));
    EXPECT_EQ("level 3 test", xml.textRead({"test", "level1", "level2", "level3"}));
    EXPECT_EQ(2.3, static_cast<double>(xml.getAttribute(
            {"test", "level1", "level2", "level3"}, "att")));
    EXPECT_EQ(2, static_cast<int>(xml.getAttribute(
            {"test", "level1", "level2", "level3"}, "att")));
    EXPECT_TRUE(static_cast<double>(xml.getAttribute(
            {"test", "level1", "level2", "level31"}, "bool")));
    EXPECT_EQ("banana", xml.get<std::string>("test.level1.level2.strval"));
    EXPECT_EQ(2.0, xml.get<double>("test.mlsm.support"));
    EXPECT_EQ(2, xml.get<int>("test.mlsm.support"));
}

TEST(IO, XMLWrite) {
    XMLloader xml("test/data/test_xml.xml");
    xml.set("test.mlsm.xx", 12);
    EXPECT_EQ(12, xml.get<int>("test.mlsm.xx"));
    xml.set("test.abc.xyz.att", "asdf");
    EXPECT_EQ("asdf", xml.get<std::string>("test.abc.xyz.att"));
    xml.setAttribute({"test", "level1"}, "attr", false);
    EXPECT_FALSE(xml.get<bool>("test.level1.attr"));
}

TEST(IO, XMLKeyValue) {
    XMLloader xml("test/data/test_conf.xml");
    auto data = xml.getKeyValuePairs();
    std::vector<std::pair<std::string, std::string>> expected = {
            {"mls.m", "3"}, {"mls.basis_type", "mon"}, {"solver.droptol", "1e-5"},
            {"num.nxs", "10, 12, 14, 16"}, {"meta.filename", "one_domain_implicit_smooth"}};
    EXPECT_EQ(expected, data);
}

#ifndef __MIC__  // Disable HDF5 on MIC
TEST(IO, HDF5Read) {
    // open file
    std::string file_name = "test/data/test_hdf5.h5";
    HDF5IO reader;
    reader.openFile(file_name, HDF5IO::READONLY);
    // read parameters in /what
    reader.openFolder("/what");

    EXPECT_EQ("/what", reader.getFolderName());
    EXPECT_EQ(file_name, reader.getFileName());

    std::string read_str = reader.getStringAttribute("object");
    EXPECT_EQ("PVOL", read_str);
    std::string date_str = reader.getStringAttribute("date");
    EXPECT_EQ("20150501", date_str);
    std::string time_str = reader.getStringAttribute("time");
    EXPECT_EQ("000000", time_str);
    std::string source_str = reader.getStringAttribute("source");
    EXPECT_EQ("WMO:14024,RAD:SI41,PLC:Lisca,NOD:silis", source_str);

    reader.openFolder("/where");
    double height = reader.getDoubleAttribute("height");
    EXPECT_DOUBLE_EQ(950.0, height);
    double lat = reader.getDoubleAttribute("lat");
    EXPECT_DOUBLE_EQ(46.06776997447014, lat);
    double lon = reader.getDoubleAttribute("lon");
    EXPECT_DOUBLE_EQ(15.28489999473095, lon);

    reader.openFolder("/dataset1/how");
    double avgpwr = reader.getDoubleAttribute("avgpwr");
    EXPECT_DOUBLE_EQ(135.0, avgpwr);

    reader.openFolder("/dataset2/how");
    double avgpwr2 = reader.getDoubleAttribute("avgpwr");
    EXPECT_DOUBLE_EQ(135.0, avgpwr2);

    reader.openFolder("/dataset1/data1/what");
    std::string quantity = reader.getStringAttribute("quantity");
    EXPECT_EQ("DBZH", quantity);
    reader.openFolder("/dataset1/data1");
    auto tmp = reader.getBScope("data");
    reader.openFolder("/dataset1/data2/what");
    quantity = reader.getStringAttribute("quantity");
    EXPECT_EQ("VRAD", quantity);
}

class HDF5WriteTest : public ::testing::Test {
  public:
    HDF5IO file;
    static const std::string file_name;
  protected:
    void SetUp() override {
        file.openFile(file_name, HDF5IO::DESTROY);
        file.openFolder("/test");
    }

    void TearDown() override {
        file.closeFolder();
        file.closeFile();
        std::remove(file_name.c_str());
    }
};
const std::string HDF5WriteTest::file_name = "test/data/test_hdf5_write.h5";

TEST_F(HDF5WriteTest, Attribute) {
    // root folder
    file.openFolder("/");
    file.setIntAttribute("int_attribute", 5);
    EXPECT_EQ(5, file.getIntAttribute("int_attribute"));

    // ordinary folder
    file.openFolder("/test");
    // INT
    file.setIntAttribute("int_attribute", -10);
    EXPECT_EQ(-10, file.getIntAttribute("int_attribute"));

    // folder within existing folder
    file.openFolder("/test/test2");
    // DOUBLE
    file.setDoubleAttribute("double_attribute", 5.1);
    EXPECT_DOUBLE_EQ(5.1, file.getDoubleAttribute("double_attribute"));
    file.setDoubleAttribute("double_attribute", 10.1245);
    EXPECT_DOUBLE_EQ(10.1245, file.getDoubleAttribute("double_attribute"));

    // new double folder
    file.openFolder("/test1/test1");
    // FLOAT
    file.setFloatAttribute("float_attribute", 5.1f);
    EXPECT_FLOAT_EQ(5.1f, file.getFloatAttribute("float_attribute"));
    file.setFloatAttribute("float_attribute", 10.1245f);
    EXPECT_FLOAT_EQ(10.1245f, file.getFloatAttribute("float_attribute"));

    // existing folder
    file.openFolder("/test/test2");
    // STRING
    file.setStringAttribute("string_attribute", "Test string");
    EXPECT_EQ("Test string", file.getStringAttribute("string_attribute"));
    file.setStringAttribute("string_attribute", "Second test string 1234.");
    EXPECT_EQ("Second test string 1234.", file.getStringAttribute("string_attribute"));
}

TEST_F(HDF5WriteTest, DoubleArray) {
    std::vector<double> ret, data = {1.0, 10.234, 2.4};
    file.setDoubleArray("vector_doubles", data);
    ret = file.getDoubleArray("vector_doubles");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_DOUBLE_EQ(data[i], ret[i]);
    }

    data = {5.0, 1234.234, 556};
    file.setDoubleArray("vector_doubles", data);
    ret = file.getDoubleArray("vector_doubles");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_DOUBLE_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<double>> data2d = {
        {0.12, 3.14157, 12.7}, {2.12, 2.6, 18.65}, {3.12, 4.6, 99.65}, {5, 5, 5}};
    file.setDouble2DArray("vec_vec_double", data2d);
    std::vector<std::vector<double>> ret2d = file.getDouble2DArray("vec_vec_double");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }
}

TEST_F(HDF5WriteTest, FloatArray) {
    std::vector<float> ret;
    std::vector<float> data = {1.0, 10.234, 2.4};
    file.setFloatArray("vector_floats", data);
    ret = file.getFloatArray("vector_floats");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    data = {5.0, 1234.234, 556};
    file.setFloatArray("vector_floats", data);
    ret = file.getFloatArray("vector_floats");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<float>> data2d = {
        {0.12, 3.14157, 12.7}, {2.12, 2.6, 18.65}, {3.12, 4.6, 99.65}, {5, 5, 5}};
    file.setFloat2DArray("vec_vec_float", data2d);
    std::vector<std::vector<float>> ret2d = file.getFloat2DArray("vec_vec_float");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }
}

TEST_F(HDF5WriteTest, IntArray) {
    std::vector<int> ret;
    std::vector<int> data = {1, 10, 2};
    file.setIntArray("vector_ints", data);
    ret = file.getIntArray("vector_ints");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<int>> data2d = {{12, 3, 12}, {2, 2, 18}, {3, 4, 99}, {5, 5, 5}};
    file.setInt2DArray("vec_vec_int", data2d);
    std::vector<std::vector<int>> ret2d = file.getInt2DArray("vec_vec_int");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }
}

TEST_F(HDF5WriteTest, GeneralArray) {
    /// [Writing nonstandard type]
    std::vector<int> v = {1, 2, 3, -6};
    file.setArray<int16_t>("an_array", v, H5::PredType::NATIVE_INT16);
    std::vector<int16_t> result = file.getArray<int16_t>("an_array");  // result is {1, 2, 3, -6};
    /// [Writing nonstandard type]
    ASSERT_EQ(v.size(), result.size());
    for (size_t i = 0; i < result.size(); i++) {
        EXPECT_EQ(v[i], result[i]);
    }
}

TEST_F(HDF5WriteTest, ArrayWithVec) {
    Range<Vec<double, 3>> vector_data;
    for (auto i = 1; i < 100; ++i) vector_data.push_back(Vec<double, 3>{0.1, 0.2, 0.3});
    file.setDouble2DArray("vector_data", vector_data, {10, 3});
    // note Chunk size {10, 3}--check documentation for more

    std::vector<std::vector<double>> tt = file.getDouble2DArray("vector_data");
    for (auto i = 0; i < vector_data.size(); ++i) {
        for (auto j = 0; j < vector_data[0].size(); ++j)
            EXPECT_DOUBLE_EQ(tt[i][j], vector_data[i][j]);
    }
}

TEST_F(HDF5WriteTest, SparseMatrix) {
    Eigen::SparseMatrix<double> M(10, 10);
    M.coeffRef(0, 0) = 4.3;
    M.coeffRef(4, 5) = -2.1;
    file.setSparseMatrix("M1", M);
    file.setSparseMatrix("M0", M, false);

    auto M1 = file.getDouble2DArray("M1");
    std::vector<std::vector<double>> M1_expected = {{1, 1, 4.3}, {5, 6, -2.1}};
    EXPECT_EQ(M1_expected, M1);
    auto M0 = file.getDouble2DArray("M0");
    std::vector<std::vector<double>> M0_expected = {{0, 0, 4.3}, {4, 5, -2.1}};
    EXPECT_EQ(M0_expected, M0);
}

TEST_F(HDF5WriteTest, Timer) {
    Timer t;
    t.addCheckPoint("a");
    t.addCheckPoint("b");
    t.addCheckPoint("c");
    file.setTimer("time", t);
    file.openFolder("/test/time");
    double total = file.getDoubleAttribute("total");
    EXPECT_LE(total, 1e-2);
    EXPECT_GE(total, 0);

    double ab = file.getDoubleAttribute("a-b");
    EXPECT_LE(ab, 1e-2);
    EXPECT_GE(ab, 0);

    double bc = file.getDoubleAttribute("b-c");
    EXPECT_LE(bc, 1e-2);
    EXPECT_GE(bc, 0);
}

TEST_F(HDF5WriteTest, Conf) {
    XMLloader conf("test/data/test_conf.xml");
    file.setConf("conf", conf);

    file.openFolder("/test/conf");
    double actual = file.getDoubleAttribute("mls.m");
    EXPECT_EQ(3, actual);
    actual = file.getDoubleAttribute("solver.droptol");
    EXPECT_EQ(1e-5, actual);
    std::string data = file.getStringAttribute("mls.basis_type");
    EXPECT_EQ("mon", data);
    data = file.getStringAttribute("num.nxs");
    EXPECT_EQ("10, 12, 14, 16", data);
    data = file.getStringAttribute("meta.filename");
    EXPECT_EQ("one_domain_implicit_smooth", data);
}

#endif  // ifndef __MIC__

}  // namespace mm
