#include "domain.hpp"
#include "domain_fill_engines.hpp"
#include "domain_extended.hpp"

#include "gtest/gtest.h"

namespace mm {


TEST(Domain, PoissonDiskSampling2DConstantFill) {
    PoissonDiskSamplingFill fill_engine;
    // setup
    fill_engine.seed(1);
    fill_engine.iterations(1000000).optim_after(1000).randomize(true).proximity_relax(0.8);
    // create test domain
    RectangleDomain<Vec2d> domain({0, 0}, {1.2, 1.5});
    CircleDomain<Vec2d> o1({0.4, 0.4}, 0.25);
    domain.subtract(o1);
    // execute fill domain with constant density
    fill_engine(domain, 0.01);
    // find minimal distance
    domain.findSupport(2);
    std::vector<double> tmp;
    for (auto c : domain.distances) tmp.push_back(std::sqrt(c[1]));
    // check if minimal distance between two nodes is above
    // target radius (0.01) * proximity_relax (0.8)
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.008);
}

TEST(Domain, PoissonDiskSampling2DFunctionFill) {
    PoissonDiskSamplingFill fill_engine;
    // setup
    fill_engine.seed(1);
    fill_engine.iterations(1000000).optim_after(1000).randomize(true).proximity_relax(0.8);
    // create test domain
    RectangleDomain<Vec2d> domain({0, 0}, {1.2, 1.5});
    CircleDomain<Vec2d> o1({0.4, 0.4}, 0.25);
    domain.subtract(o1);
    // define target density
    auto fill_density = [](Vec2d p)->double{
        return (std::pow(p[0]/10, 2) + 0.01);
    };
    fill_engine(domain, fill_density);
    // find minimal distance
    domain.findSupport(2);
    std::vector<double> tmp;
    for (auto c : domain.distances) tmp.push_back(std::sqrt(c[1]));
    // check if minimal distance between two nodes is above
    // target radius (0.01) * proximity_relax (0.8)
    // and below maximal density (0.0288) -- including 5% threshold factor
    EXPECT_LT(*std::max_element(std::begin(tmp), std::end(tmp)),  0.0244 * 1.05);
}


TEST(Domain, PoissonDiskSampling3DConstantFill) {
    PoissonDiskSamplingFill fill_engine;
    // setup
    fill_engine.seed(1);
    fill_engine.iterations(1000000).optim_after(1000).randomize(true).proximity_relax(0.8);
    // create test domain
    RectangleDomain<Vec3d> domain({0, 0, 0}, {1.2, 1.5, 1.1});
    CircleDomain<Vec3d> o1({0, 0, 0}, 0.5);
    domain.subtract(o1);
    // execute fill domain with constant density
    fill_engine(domain, 0.05);

    // find minimal distance - Actual test
    domain.findSupport(2);
    std::vector<double> tmp;
    for (auto c : domain.distances) tmp.push_back(std::sqrt(c[1]));
    // check if minimal distance between two nodes is above
    // target radius (0.01) * proximity_relax (0.8)
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.04);
}

TEST(Domain, PoissonDiskSampling3DFunctionFill) {
    PoissonDiskSamplingFill fill_engine;
    // setup
    fill_engine.seed(1);
    fill_engine.iterations(1000000).optim_after(1000).randomize(true).proximity_relax(0.8);
    // create test domain
    RectangleDomain<Vec3d> domain({0, 0, 0}, {1.2, 1.5, 1.1});
    CircleDomain<Vec3d> o1({0, 0, 0}, 0.5);
    domain.subtract(o1);
    // define target density
    auto fill_density = [](Vec3d p)->double{
        return (std::pow(p[0]/10 + p[1]/10 + p[2]/10, 2) + 0.025);
    };
    fill_engine(domain, fill_density);

    // find minimal distance - Actual test
    domain.findSupport(2);
    std::vector<double> tmp;
    for (auto c : domain.distances) tmp.push_back(std::sqrt(c[1]));
    // check if minimal distance between two nodes is above
    // target radius  * proximity_relax  -- ((0.5/10)^2 + 0.025 )*0.8 = 0.02
    // and below maximal density ((1.5/10 + 1.2/10 + 1.1/10)^2 + 0.025 )= 0.1694
    // -- including 5% threshold factor
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.0220);
    EXPECT_LT(*std::max_element(std::begin(tmp), std::end(tmp)),  0.1694 * 1.05);
}


}  // namespace mm
