#include "domain_refine_engines.hpp"
#include "domain.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, Refine1d) {
    RectangleDomain<Vec1d> domain(0, 1);
    domain.fillUniform(9, 2);
    int N = domain.size();
    domain.findSupport(3);
    auto region = domain.positions.filter([](const Vec1d &v) { return v[0] < 0.45; });
    HalfLinksRefine refine; refine.region(region);
    auto new_points = domain.apply(refine);
    Range<Vec1d> expected = {0.05, 0.15, 0.25, 0.35, 0.45};
    ASSERT_EQ(expected.size(), new_points.size());
    for (int i = 0; i < expected.size(); ++i) {
        ASSERT_EQ(N + i, new_points[i]);
        EXPECT_DOUBLE_EQ(expected[i][0], domain.positions[new_points[i]][0]);
    }
}

TEST(Domain, Refine2dCorner) {
    RectangleDomain<Vec2d> domain(0, 1);
    domain.fillUniformWithStep(0.2, 0.2);
    domain.findSupport(9);
    int N = domain.size();

    auto region = domain.positions.filter(
            [](const Vec2d &v) { return v[0] < 0.35 && v[1] < 0.35; });
    HalfLinksRefine refine; refine.region(region);
    auto new_points = domain.apply(refine);
    EXPECT_EQ(N + new_points.size(), domain.size());
    Range<Vec2d> expected = {{0.0, 0.1}, {0.0, 0.3}, {0.1, 0.0}, {0.1, 0.1},
                             {0.1, 0.2}, {0.1, 0.3}, {0.2, 0.1}, {0.2, 0.3},
                             {0.3, 0.0}, {0.3, 0.1}, {0.3, 0.2}, {0.3, 0.3}};
    Range<int> expected_types = {-1, -1, -1, 1, 1, 1, 1, 1, -1, 1, 1, 1};
    ASSERT_EQ(expected.size(), new_points.size());
    std::sort(new_points.begin(), new_points.end(), [&](int i, int j) {
        return domain.positions[i] < domain.positions[j];
    });
    double tol = 1e-6;
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_LT(new_points[i], domain.size());
        EXPECT_GE(new_points[i], N);
        EXPECT_NEAR(expected[i][0], domain.positions[new_points[i]][0], tol);
        EXPECT_NEAR(expected[i][1], domain.positions[new_points[i]][1], tol);
        EXPECT_EQ(expected_types[i], domain.types[new_points[i]]);
    }
}

TEST(Domain, Refine2dLeftSide) {
    RectangleDomain<Vec2d> domain(0, 1);
    domain.fillUniformWithStep(0.5, 0.5);
    domain.findSupport(9);
    int N = domain.size();
    auto region = domain.positions.filter([](const Vec2d &v) { return v[0] < 0.4; });
    HalfLinksRefine refine; refine.region(region);
    auto new_points = domain.apply(refine);
    Range<Vec2d> expected = {{0.0, 0.25}, {0.0, 0.75},  {0.25, 0.0}, {0.25, 0.25},
                               {0.25, 0.5}, {0.25, 0.75}, {0.25, 1.0}};
    Range<int> expected_types = {-1, -1, -1, 1, 1, 1, -1, 1, 1};
    ASSERT_EQ(expected.size(), new_points.size());
    std::sort(new_points.begin(), new_points.end(), [&](int i, int j) {
        return domain.positions[i] < domain.positions[j];
    });
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_LT(new_points[i], domain.size());
        EXPECT_GE(new_points[i], N);
        EXPECT_NEAR(expected[i][0], domain.positions[new_points[i]][0], EPS);
        EXPECT_NEAR(expected[i][1], domain.positions[new_points[i]][1], EPS);
        EXPECT_EQ(expected_types[i], domain.types[new_points[i]]);
    }
}

}  // namespace mm
