#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_fill_engines.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, RelaxWithConstDIstributionTestBoundaryProjection0) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);
    int n = 55;
    // use 10 times lower number of nodes on boundary to test projection
    c.fillUniform(iceil(M_PI * r * r * n * n), iceil(2 * M_PI * r * n) / 10);
    // no of boudary nodes before relax
    int N_1 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    double min_spacing = 0.5 * std::sqrt(M_PI * r * r / n);
    BasicRelax relax;
    relax.iterations(20).initialHeat(5).numNeighbours(4).projectionType(BasicRelax::DO_NOT_PROJECT);
    relax(c, min_spacing);
    // no of boudary nodes after relax
    int N_2 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    // actual tests
    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), min_spacing / 10);
    // check if nodes are added on boundary
    EXPECT_EQ(N_2, N_1);
}

TEST(Domain, RelaxWithConstDIstributionTestBoundaryProjection1) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);
    int n = 55;
    // use 10 times lower number of nodes on boundary to test projection
    c.fillUniform(iceil(M_PI * r * r * n * n), iceil(2 * M_PI * r * n) / 10);
    // no of boudary nodes before relax
    int N_1 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    double min_spacing = 0.5 * std::sqrt(M_PI * r * r / n);
    BasicRelax relax;
    relax.iterations(50).initialHeat(1).numNeighbours(4).finalHeat(0.1)
            .projectionType(BasicRelax::PROJECT_IN_DIRECTION).boundaryProjectionThreshold(0.35);
    relax(c, min_spacing);
    // no of boudary nodes after relax
    int N_2 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    // actual tests
    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), min_spacing / 10);
    // check if nodes are added on boundary
    EXPECT_GT(N_2, N_1);
}

TEST(Domain, RelaxWithConstDistributionTestBoundaryProjection2) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);
    int n = 55;
    // use 10 times lower number of nodes on boundary to test projection
    c.fillUniform(iceil(M_PI * r * r * n * n), iceil(2 * M_PI * r * n) / 10);
    // no of boudary nodes before relax
    int N_1 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    double min_spacing = 0.5 * std::sqrt(M_PI * r * r / n);
    BasicRelax relax;
    relax.iterations(20).initialHeat(1).numNeighbours(5)
            .projectionType(BasicRelax::PROJECT_BETWEEN_CLOSEST);
    relax(c, min_spacing);
    // no of boudary nodes after relax
    int N_2 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    // actual tests
    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), min_spacing / 10);
    // check if nodes are added on boundary
    EXPECT_GT(N_2, N_1);
}

TEST(Domain, RelaxWithConstDIstributionTestBoundaryProjectiontreshold) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);
    int n = 55;
    // use 10 times lower number of nodes on boundary to test projection
    c.fillUniform(iceil(M_PI * r * r * n * n), iceil(2 * M_PI * r * n) / 10);
    // no of boudary nodes before relax
    int N_1 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    double min_spacing = 0.5 * std::sqrt(M_PI * r * r / n);
    BasicRelax relax;
    relax.iterations(20).initialHeat(1).numNeighbours(4).boundaryProjectionThreshold(1.5)
            .projectionType(BasicRelax::PROJECT_IN_DIRECTION);
    relax(c, min_spacing);

    // no of boudary nodes after relax
    int N_2 = (c.types == NODE_TYPE::DEFAULT_BOUNDARY).size();
    // actual tests
    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), min_spacing / 10);
    // check if nodes are added on boundary
    EXPECT_EQ(N_2, N_1);
}

TEST(Domain, RelaxWithVariableDistribution) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);

    BasicRelax relax;
    relax.iterations(50).initialHeat(1).finalHeat(0.05).boundaryProjectionThreshold(0.75)
            .projectionType(BasicRelax::PROJECT_IN_DIRECTION).numNeighbours(5);
    auto fill_density = [](Vec2d p) -> double {
        return (0.005 + (p[0] - 0.5) * (p[0] - 0.5) / 2 + (p[1] - 0.5) * (p[1] - 0.5) / 2);
    };

    c.fillUniformBoundaryWithStep(fill_density(Vec2d({r, 0.0})));
    PoissonDiskSamplingFill fill_engine;
    fill_engine.seed(1);
    fill_engine(c, fill_density);
    relax(c, fill_density);

    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.001);
}

TEST(Domain, DeathTest1) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);

    BasicRelax relax;

    relax.iterations(50).initialHeat(20).finalHeat(0.05).boundaryProjectionThreshold(0.75)
            .projectionType(BasicRelax::PROJECT_IN_DIRECTION).numNeighbours(5);
    auto fill_density = [](Vec2d p) -> double {
        return (0.05 + (p[0] - 0.5) * (p[0] - 0.5) / 2 + (p[1] - 0.5) * (p[1] - 0.5) / 2);
    };

    int n = 15;
    c.fillUniform(iceil(M_PI * r * r * n * n), iceil(2 * M_PI * r * n));


    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_DEATH(relax(c, fill_density), "No nodes in relax pool anymore, perhaps use lower heat");
}

TEST(Domain, RelaxWithoutDistribution) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);

    BasicRelax relax;
    relax.iterations(100).initialHeat(1).finalHeat(0).numNeighbours(3)
            .projectionType(BasicRelax::PROJECT_IN_DIRECTION).boundaryProjectionThreshold(0.55);
    auto fill_density = [](Vec2d p) -> double {
        return (0.005 + (p[0] - 0.5) * (p[0] - 0.5) / 2 + (p[1] - 0.5) * (p[1] - 0.5) / 2);
    };

    c.fillUniformBoundaryWithStep(fill_density(Vec2d({r, 0.0})));
    PoissonDiskSamplingFill fill_engine;
    fill_engine.seed(1);
    fill_engine(c, fill_density);
    relax(c);

    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.001);
}

TEST(Domain, DISABLED_WikiImage1) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);

    auto fill_density = [](Vec2d p) -> double {
        return (0.0025 + (p[0] - 0.5) * (p[0] - 0.5) / 2 + (p[1] - 0.5) * (p[1] - 0.5) / 4);
    };
    prn(fill_density(Vec2d({0.25, 0})))
    c.fillUniformBoundaryWithStep(fill_density(Vec2d({0.25, 0})) / 2);
    PoissonDiskSamplingFill fill_engine;
    fill_engine(c, fill_density);

    BasicRelax relax;
    relax.iterations(200).initialHeat(2).numNeighbours(3)
            .projectionType(BasicRelax::PROJECT_BETWEEN_CLOSEST);
    relax.boundaryProjectionThreshold(0.45).finalHeat(0.5);
    relax(c, fill_density);


    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.0005);
}
TEST(Domain, DISABLED_WikiImage2) {
    double r = 0.25;
    CircleDomain<Vec2d> c({0.5, 0.5}, r);

    auto fill_density = [](Vec2d p) -> double {
        return (0.0025 + (p[0] - 0.5) * (p[0] - 0.5) / 2 + (p[1] - 0.5) * (p[1] - 0.5) / 4);
    };
    prn(fill_density(Vec2d({0.25, 0})))
    c.fillUniformBoundaryWithStep(fill_density(Vec2d({0.25, 0})) / 2);
    PoissonDiskSamplingFill fill_engine;
    fill_engine(c, fill_density);

    BasicRelax relax;
    relax.iterations(200).initialHeat(2).numNeighbours(3)
            .projectionType(BasicRelax::PROJECT_BETWEEN_CLOSEST);
    relax.boundaryProjectionThreshold(0.45).finalHeat(0.5);
    relax(c);


    c.findSupport(2);
    std::vector<double> tmp;
    for (auto d : c.distances) tmp.push_back(std::sqrt(d[1]));
    // check for overlapping nodes
    EXPECT_GT(*std::min_element(std::begin(tmp), std::end(tmp)), 0.0005);
}

}  // namespace mm
