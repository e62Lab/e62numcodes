#include "linear_solvers.hpp"

#include "gtest/gtest.h"

#include "common.hpp"
#include "types.hpp"
#include "includes.hpp"

/**
 * @file test_mls.cpp
 * @brief MLS Tests
 */

namespace mm {

TEST(LinearSolver, SVDSolverSquare) {
    typedef SVDLinearSolver<double>::ei_matrix_t ei_matrix_t;
    typedef SVDLinearSolver<double>::ei_vec_t ei_vec_t;
    SVDLinearSolver<double> solver;
    ei_matrix_t A(3, 3);
    ei_vec_t rhs(3), expected(3), actual(3);

    // Small test case
    A << 1, 2, 3,   4, 5, 6,   7, 8, 10;
    expected << -2, 1, 1;
    rhs << 3, 3, 4;
    solver.compute(A);
    actual = solver.solve(rhs);
    for (int i = 0; i < 3; i++) {
        EXPECT_NEAR(expected(i), actual(i), 1e-10);
    }

    // Big test case
    A = ei_matrix_t::Random(10, 10);
    expected = ei_vec_t::Random(10);
    rhs = A*expected;
    solver.compute(A);
    actual = solver.solve(rhs);
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < 10; i++) {
        EXPECT_NEAR(expected(i), actual(i), 1e-10);
    }
}

TEST(LinearSolver, SVDSolverLeastSquare) {
    typedef SVDLinearSolver<double>::ei_matrix_t ei_matrix_t;
    typedef SVDLinearSolver<double>::ei_vec_t ei_vec_t;
    SVDLinearSolver<double> solver;
    ei_matrix_t A = ei_matrix_t::Random(5, 3);
    ei_vec_t expected = ei_vec_t::Random(3), actual, rhs;

    rhs = A*expected;
    solver.compute(A);
    actual = solver.solve(rhs);
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); i++) {
        EXPECT_NEAR(expected(i), actual(i), 1e-10);
    }
}

TEST(LinearSolver, FullPivLU) {
    FullPivLULinearSolver<double> solver;
    typedef FullPivLULinearSolver<double>::ei_matrix_t ei_matrix_t;
    typedef FullPivLULinearSolver<double>::ei_vec_t ei_vec_t;

    ei_matrix_t A = ei_matrix_t::Random(5, 5);
    ei_vec_t expected = ei_vec_t::Random(5), actual, rhs;

    rhs = A*expected;
    solver.compute(A);
    actual = solver.solve(rhs);
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); i++) {
        EXPECT_NEAR(expected(i), actual(i), 1e-10);
    }
}

TEST(LinearSolver, PartialPivLU) {
    PartialPivLULinearSolver<double> solver;
    typedef PartialPivLULinearSolver<double>::ei_matrix_t ei_matrix_t;
    typedef PartialPivLULinearSolver<double>::ei_vec_t ei_vec_t;

    ei_matrix_t A = ei_matrix_t::Random(5, 5);
    ei_vec_t expected = ei_vec_t::Random(5), actual, rhs;

    rhs = A*expected;
    solver.compute(A);
    actual = solver.solve(rhs);
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); i++) {
        EXPECT_NEAR(expected(i), actual(i), 1e-10);
    }
}

TEST(LinearSolver, PartialPivLUDeathTest) {
    typedef PartialPivLULinearSolver<double>::ei_matrix_t ei_matrix_t;
    typedef PartialPivLULinearSolver<double>::ei_vec_t ei_vec_t;
    PartialPivLULinearSolver<double> solver;

    ei_vec_t rhs(3);
    rhs << 1, 2, 3;
    // Compute before you solve
    EXPECT_DEATH(solver.solve(rhs), "call `compute` before");

    // Only accept square matrices
    ei_matrix_t A = ei_matrix_t::Random(5, 2);
    EXPECT_DEATH(solver.compute(A), "only for square .+ matrices");

    // Non-inverible matrix
    A.resize(3, 3);
    A << 1, 2, 3,  4, 5, 6,  1, 2, 3;
    solver.compute(A);
    EXPECT_EQ(false, std::isnormal(solver.solve(rhs)(0)));
}

}  // namespace mm
