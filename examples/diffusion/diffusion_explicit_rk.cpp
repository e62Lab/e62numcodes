/*
[Explicit solution of a diffusion equation with a use of MLSM operators
with a mixed boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "integrators.hpp"

using namespace mm;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */
double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

int main() {
    Timer timer;
    timer.addCheckPoint("start");

    double dx = 1. / 100.;
    int n = 12;  // support size
    int m = 3;  // monomial basis of second order, i.e. 6 monomials
    double time = 0.1;  // time
    double dt = 1e-5;  // time step
    int t_steps = std::ceil(time / dt);
    double sigma = 1;  // naive normalization

    // prep domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformWithStep(dx, dx);
    domain.findSupport(n);

    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    // init state
    VecXd T1(domain.size());
    T1[boundary] = 0.0;
    T1[interior] = 1.0;

    timer.addCheckPoint("shapes");
    // prep operators
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(m, sigma);
    auto op = make_mlsm<mlsm::lap>(domain, mls, interior, false);

    auto dv_dt = [&](double, const VecXd& y) {
        Eigen::VectorXd der(y.size());
        for (int c : interior) {
            der[c] = op.lap(y, c);
        }
        for (int c : boundary) {
            der[c] = 0;
        }
        return der;
    };

    auto integrator = integrators::Explicit::Euler().solve(dv_dt, 0.0, time, dt, T1);
    auto stepper = integrator.begin();

    timer.addCheckPoint("solve");
    while (stepper) {
        ++stepper;
    }
    timer.addCheckPoint("end");

    Range<double> E2(domain.size(), 0);
    for (auto& c : interior) {
        E2[c] = std::abs(stepper.value()[c] -
                         diff_closedform(domain.positions[c], stepper.time(), 1, 1, 50));
    }
    std::cout << "Max error at t = " << stepper.time() << ": "
              << *std::max_element(E2.begin(), E2.end()) << "\n";

    timer.showTimings();

    return 0;
}
