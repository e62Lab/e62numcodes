close all
clc;clear;
working_dir='../'; 

addpath(working_dir);
files=dir([working_dir,'*.m']);
E=length(files);

toMp4=0;
toImg=0;
range=[E]; %[1:E] full range

plot_pos = 1;
plot_temp = 1;


fig=createFig('pos',2,'scale',1);hold on;
for file_num=range 
    eval(files(file_num).name(1:find(files(file_num).name=='.')-1));
    
    if(plot_pos && exist('pos') && ~exist('pp'))
%         p_inter=plot(pos(types>0,1),pos(types>0,2),'k.');
        p_bound=plot(pos(types<0,1),pos(types<0,2),'b.');
        p_inter=plot(pos(domain.int+1,1),pos(domain.int+1,2),'k.');
    end
    
    if(plot_temp && exist('T') && ~exist('pp'))
        pX=linspace(domain.lo(1),domain.hi(1),domain.size(1));
        pY=linspace(domain.lo(2),domain.hi(2),domain.size(2));
        [pX,pY]=meshgrid(pX,pY);
        pT=griddata(pos(:,1),pos(:,2),T,pX,pY,'cubic'); 
        contour(pX,pY,pT,'fill','on');
%         surf(pX,pY,pT);
    end
    
    if(exist('supp'))
        for j=1:length(supp);
            p_supp=plot([pos(supp(1)+1,1),pos(supp(j)+1,1)],[pos(supp(1)+1,2),pos(supp(j)+1,2)],'r');
        end
    end
end