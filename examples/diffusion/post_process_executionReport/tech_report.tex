\documentclass[a4paper,oneside,12pt]{article}

\usepackage[english]{babel}    % slovenian language and hyphenation
\usepackage[utf8]{inputenc}    % make čšž work on input
\usepackage[T1]{fontenc}       % make čšž work on output
\usepackage[reqno]{amsmath}    % basic ams math environments and symbols
\usepackage{amssymb,amsthm}    % ams symbols and theorems
\usepackage{mathtools}         % extends ams with arrows and stuff
\usepackage{url}               % \url and \href for links
\usepackage{icomma}            % make comma a thousands separator with correct spacing
\usepackage{units}             % \unit[1]{m} and unitfrac
\usepackage{enumerate}         % enumerate style
\usepackage{array}             % mutirow
\usepackage[usenames]{color}   % colors with names
\usepackage{graphicx}          % images

\usepackage[bookmarks, colorlinks=true, linkcolor=black, anchorcolor=black,
  citecolor=black, filecolor=black, menucolor=black, runcolor=black,
  urlcolor=black, pdfencoding=unicode]{hyperref}  % clickable references, pdf toc
\usepackage[
  paper=a4paper,
  top=2.5cm,
  bottom=2.5cm,
  textwidth=15cm,
% textheight=24cm,
]{geometry}  % page geomerty

\newtheorem{izrek}{Izrek}
\newtheorem{posledica}{Posledica}

\theoremstyle{definition}
\newtheorem{definicija}{Definicija}
\newtheorem{opomba}{Opomba}
\newtheorem{zgled}{Zgled}

% basic sets
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\renewcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}

% lists with less vertical space
\newenvironment{itemize*}{\vspace{-6pt}\begin{itemize}\setlength{\itemsep}{0pt}\setlength{\parskip}{2pt}}{\end{itemize}}
\newenvironment{enumerate*}{\vspace{-6pt}\begin{enumerate}\setlength{\itemsep}{0pt}\setlength{\parskip}{2pt}}{\end{enumerate}}
\newenvironment{description*}{\vspace{-6pt}\begin{description}\setlength{\itemsep}{0pt}\setlength{\parskip}{2pt}}{\end{description}}

\newcommand{\Title}{Techinal report}
\newcommand{\Author}{Jure Slak}
\title{\Title}
\author{\Author}
\date{\today}
\hypersetup{pdftitle={\Title}, pdfauthor={\Author}, pdfcreator={\Author},
            pdfproducer={\Author}, pdfsubject={}, pdfkeywords={}}  % setup pdf metadata

% \pagestyle{empty}              % vse strani prazne
% \setlength{\parindent}{0pt}    % zamik vsakega odstavka
% \setlength{\parskip}{10pt}     % prazen prostor po odstavku
\setlength{\overfullrule}{30pt}  % oznaci predlogo vrstico z veliko črnine

\begin{document}

\begin{center}
  \huge \bfseries Report on analysis of running time of explicit Euler method
  for different containers.
\end{center}

\vspace{2ex}

\tableofcontents

\vspace{2ex}

\listoffigures

\newpage

\section{Problem}
We test the speed of time iteration for explicit Euler method when solving the
diffusion equation.
Using different types may result in different performance, due to double
indexing, size of the problem and cache-friendliness. A detailed analysis of the
topic can be seen in below attached report.

Basic code being tested
is\footnote{Full code is avaliable at our repository
  \href{https://bitbucket.org/GregorKosec/e62numcodes/src/7fee1fa4355576c7f7c70f1769fdf401b61ab22c/mechanics/performance_test.cpp?at=master&fileviewer=file-view-default}{(link)}.}:
\begin{verbatim}
for (int tt = 0; tt < t_steps; ++tt) {
    for (int c : interior) {
        double Lap = 0;
        for (int j = 0; j < n; ++j) {
            Lap += SL[c][j] * T1[SD[c][j]];
        }
        T2[c] = T1[c] + dt * Lap;
    }
    T1.swap(T2);
}
\end{verbatim}
where \verb|T1|, \verb|T2|, \verb|SL| are (one or two dimensional) arrays of
integers, and \verb|SD| is a 2D array of int's.

The outer loop performs time step iterations. The middle loop iterates over all
nodes in the domain. For each node value of Laplace operator is calculated by
looping over the support nodes (inner loop). Temperature of nodes is then
updated, and after the calculations for all nodes are complete, we swap the
vectors of previous and newly calculated temperatures.

Other parameters are:
\begin{itemize*}
  \item End time: $0.2$
  \item Time step: $10^{-5}$
  \item Number of time iterations: $tsteps = 20 000$
  \item Basis: $5$ Gaussians
  \item Domain: $[0, 1]^2$
\end{itemize*}
We mark node count by $N$ and support size by $n$.
Total number of iterations performed is:
\[ I = tsteps \times n \times (N - 4 \sqrt{N} + 4) \]

We tested 7 different types of containers, which will appear in the same order
throughout the report.
\begin{enumerate*}
  \item our own types: Range, Vec
  \item our own operators
  \item pure Eigen types
  \item C arrays (1 dimensional)
  \item std::vector (2 dimensional)
  \item std::vector (1 dimensional)
  \item our types (1 dimensional)
\end{enumerate*}

\section{Environment}
For mesuring time standard
\href{http://www.cplusplus.com/reference/chrono/high_resolution_clock/}{C++
high resolution clock} was used. For measuring number of cache accesses
\href{https://software.intel.com/en-us/articles/intel-performance-counter-monitor}{Intel
PCM} was used.

All tests were run on our clusters computers separately and independently.
Hardware and software specifications:
\begin{itemize*}
  \item
    \href{http://ark.intel.com/products/40200/Intel-Xeon-Processor-E5520-8M-Cache-2_26-GHz-5_86-GTs-Intel-QPI}{Intel(R)
    Xeon(R) CPU E5520  @ 2.27GHz processor}
  \item \unit[256]{kB} of L2 cache and \unit[8]{MB} of L3 cache.
  \item \unit[6]{GB} of DDR3 RAM
  \item system: \verb|Linux k5 3.2.0-26-generic #41-Ubuntu x86_64 GNU/Linux|
  \item \texttt{g++} version: \verb|g++ (Ubuntu 4.9.2-0ubuntu1~12.04) 4.9.2|
  \item \texttt{cmake} version: 3.3.2
\end{itemize*}

All tests were ran using the following compile flags:
\begin{verbatim}
g++ -03 -DNDEBUG -DEIGEN_NO_DEBUG -std=c++14 -Wall
\end{verbatim}

\section{Initial measurements}
Initially our types and Eigen were much slower that the rest. However, after
disabling the debug modes we gained first relevant measurements on
figures~\ref{fig:firsttime} to~\ref{fig:firstL3total}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_time.pdf}
  \caption{Initial running time for different types.}
  \label{fig:firsttime}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_L2hitratio.pdf}
  \caption{Initial L2 cache hit ratio for different types.}
  \label{fig:firstL2hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_L3hitratio.pdf}
  \caption{Initial L3 cache hit ratio for different types.}
  \label{fig:firstL3hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_L2total.pdf}
  \caption{Initial L2 cache total access count for different types.}
  \label{fig:firstL2total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_L3total.pdf}
  \caption{Initial L3 cache total access count for different types.}
  \label{fig:firstL3total}
\end{figure}

\clearpage

As the problem size is quite small, L2 cache hit ratio is important.
Figure~\ref{fig:firstcorr} plots correlation between average cache hit ratio and
running time growth coefficient, showing strong correlation (except for Eigen).

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_first_correlation.pdf}
  \caption{Correlation between running time and L2 cache hit ratio.}
  \label{fig:firstcorr}
\end{figure}

Looking at L2 total accesses graph, we see that C array uses twice as many
accesses as 1 dimensional types, and our types use 4 times as many. Operators,
which call a function in their own class, are by far the worst, which is blamed
on their general structure, as they do not assume all supports have the same
size.

Running the tests with bigger support size revealed even more behaviour. The
same types of plots as before are shown on figures~\ref{fig:first25time}
to~\ref{fig:first25L3total}.

\clearpage

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_first_time.pdf}
  \caption{Initial running time with larger support size.}
  \label{fig:first25time}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_first_L2hitratio.pdf}
  \caption{Initial L2 cache hit ratio with larger support size.}
  \label{fig:first25L2hit}
\end{figure}

\clearpage

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_first_L3hitratio.pdf}
  \caption{Initial L3 cache hit ratio with larger support size.}
  \label{fig:first25L3hit}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_first_L2total.pdf}
  \caption{Initial L2 cache total access count with larger support size.}
  \label{fig:first25L2total}
\end{figure}

\clearpage

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_first_L3total.pdf}
  \caption{Initial L3 cache total access count with larger support size.}
  \label{fig:first25L3total}
\end{figure}

Of special interest is the break of time for our operators on
figure~\ref{fig:first25time} at around $N \approx 5000$. The time increase is
believed to be the consequence of running out of L3 cache space. This also happens
for some of the other types later on the graph. This slowdowns coincide with the declines
of the L3 cache hit ratio curve on figure~\ref{fig:first25L3hit}.

We have to answer a few questions:
\begin{enumerate*}
  \item Why do we have significantly different times for different types with 1d
    structure? \label{q:1d}
  \item Why do we have significantly different times for different types with 2d
    structure? \label{q:2d}
  \item What accounts for the difference between 1d and 2d types and how
    relevant is it? \label{q:1d2d}
  \item Do slowdowns happen at expected node counts and are they well correlated
    with cache size? \label{q:slowwhen}
  \item Why do different types experience slowdowns at smaller node counts than
    others? \label{q:slowdiff}
  \item Why the hell are operators so much slower? \label{q:op}
\end{enumerate*}

\section{Fixes and second round of measurements}
Measurements helped us bind a few inconsistencies in our code. C-array was using
a wrong swap technique, which accounted for more memory accesses than necessary.
Some types used \verb|size_t| for indexes and other used \verb|int|. This was
one of the causes for different slowdowns, as array of \verb|size_t|-s takes twice
more space than an array of \texttt{int}-s. We fixed the whole code to use ints
(and plan to do so throughout the whole library).

We also used Eigen suboptimally, not using their own \texttt{ArrayXii} class,
which is implemented as 1d array. We also changed all matrices used to be
row-major, to ensure cache friendliness, as we only do row traversals.

For every time, we copied support domain data \texttt{SD} and shape functions
\texttt{SL} to local variables of the same type.

We believe the above accounts for differences among the similar types and
answers questions~\ref{q:1d} and~\ref{q:2d}.

Assuming that slowdowns are caused because we ran out of cache space, we looks
at out memory use. Our fixes included using consistent types for indexes and
decimal numbers, copying all data to locally variables, and always allocating
arrays of the same size on the heap. This helped smooth the differences in
slowdown starts for different types, answering question~\ref{q:slowdiff}.

We currently have around 10 local \texttt{int} or \texttt{double} variables,
two 1d arrays of $N$ \texttt{double}-s, one 2d $N\times n$ array of
\texttt{int}-s and one 2d array of \texttt{double}-s.
This sums up to approximately
\[ [N\times n \times (8 + 4) + N \times 8 + 64] \text{ bytes} \]
To answer question~\ref{q:slowwhen}, we check at which node count do we fill the
L2 or L3 cache memory and we expect that the slowdown will appear around the
same node count. The expected results are calculated in table~\ref{tab:expslow}.

\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|} \hline
    $n$ & L2 & L3 \\ \hline
     5  & $3\,350$  & $105\,000$ \\ \hline
    13  & $1\,500$ & $46\,500$ \\ \hline
    25  & 800 & $23\,300$ \\ \hline
  \end{tabular}
  \caption{Expected node counts at which slowdowns should happen.}
  \label{tab:expslow}
\end{table}

To answer question~\ref{q:op} we need to look at operators structure a bit
closer. Operators are a class, repsresenting a collestion of operators (of which
we only use laplace). The compute and store all first derivatives and laplace
shape functions. When called for a certain node, they perform the innermost for
loop in code above. As they were planned to be as general as possible, we
assumed that we do not know the size of the shape functions and used our 2d types
for storage. We changed that to 1d heap allocated C-arrays, storing only
pointers, changed all methods to be constant and make appropriate variables
static. With some loss of generality (that we never even had elsewhere), we have
made them comparable to the other types.

Measurements after changes above for 3 different support sizes, can be seen on
figures~\ref{fig:5time} to~\ref{fig:25L3total}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_time.pdf}
  \caption{Final running time for $n = 5$.}
  \label{fig:5time}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_L2hitratio.pdf}
  \caption{Final L2 cache hit ratio for $n = 5$.}
  \label{fig:5L2hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_L3hitratio.pdf}
  \caption{Final L3 cache hit ratio for $n = 5$.}
  \label{fig:5L3hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_L2total.pdf}
  \caption{Final L2 cache total access count for $n = 5$.}
  \label{fig:5L2total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot5_L3total.pdf}
  \caption{Final L3 cache total access count for $n = 5$.}
  \label{fig:5L3total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot13_time.pdf}
  \caption{Final running time for $n = 13$.}
  \label{fig:13time}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot13_L2hitratio.pdf}
  \caption{Final L2 cache hit ratio for $n = 13$.}
  \label{fig:13L2hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot13_L3hitratio.pdf}
  \caption{Final L3 cache hit ratio for $n = 13$.}
  \label{fig:13L3hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot13_L2total.pdf}
  \caption{Final L2 cache total access count for $n = 13$.}
  \label{fig:13L2total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot13_L3total.pdf}
  \caption{Final L3 cache total access count for $n = 13$.}
  \label{fig:13L3total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_time.pdf}
  \caption{Final running time for $n = 25$.}
  \label{fig:25time}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_L2hitratio.pdf}
  \caption{Final L2 cache hit ratio for $n = 25$.}
  \label{fig:25L2hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_L3hitratio.pdf}
  \caption{Final L3 cache hit ratio for $n = 25$.}
  \label{fig:25L3hit}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_L2total.pdf}
  \caption{Final L2 cache total access count for $n = 25$.}
  \label{fig:25L2total}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{img_plot25_L3total.pdf}
  \caption{Final L3 cache total access count for $n = 25$.}
  \label{fig:25L3total}
\end{figure}

We see that L3 and L2 cahce declines coincide very nicely with slowdowns on time
graphs. The decline of L3 cahce hit ration in the $n= 25$ case in
figure~\ref{fig:25L3hit} happens at approximately the same node cont as we
predicted. Similarly, our predictions match in the $n=13$ case as well
(fig.~\ref{fig:13L3hit}). All 1d types are grouped together and 2d types are
grouped together, with only alight difference between them. We believe that this
difference is due to an additional work of another lookup / dereference, which
is large, compared to simple addition and multiplication used in 1d arrays.
Operators are now the fastest (which is surprising\dots)

All solutions have also been tested and they return identical and correct
results.

\clearpage

\section{Conclusions}
Overall, we are satisfied with the results. We learned some things.
Details matter. Caches are important.

% text here

\end{document}
% vim: syntax=tex
% vim: spell spelllang=en
% vim: foldlevel=99
% Latex template: Jure Slak, jure.slak@gmail.com

