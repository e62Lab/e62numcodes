/*
[Solution of de Vahl Davis natural convection benchmark test
 :Explicit artificial compressibility with CBS stabilization
 execute with parameters from params/, i.e. .lid_driven_explicit_2D ../params/lid_driven_re100
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>


#define _MAX_ITER_ 500

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    //domain.fillUniformWithStep(O.d_space, O.d_space);
    domain.fillUniform(O.d_num - Vec<int, 2>(2), O.d_num );

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    double tol = 1e-10;
    Range<int> top = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_hi[1]) < tol; }),
            bottom = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_lo[1]) < tol; }),
            right = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_hi[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; }),
            left = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_lo[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; });


    domain.findSupport(O.n, all);

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, NNGaussians, NNGaussians> mls
            ({O.sigmaB, O.m},
             O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");
    // init state
    Range<double> P_1(domain.size(), 0);
    Range<double> P_2(domain.size(), 0);

    Range<vec_t> v_1(domain.size(), 0);
    Range<vec_t> v_2(domain.size(), 0);

    Range<double> T_1(domain.size(), O.T_init);
    Range<double> T_2(domain.size(), O.T_init);

    // boundary conditions
    v_1 = v_2;
    T_2[left] = O.T_hot;
    T_2[right] = O.T_cold;
    T_1 = T_2;

    // hydrostatic initial pressure
    for (auto i : all) {
        P_1[i] = O.g_0 * O.rho * domain.positions[i][1]*(1 - O.beta * (T_1[i] - O.T_ref));
    }
    P_2 = P_1;

    std::chrono::high_resolution_clock::time_point time_1, time_2;
    int step_history = 0;

    for (int step = 0; step <= O.t_steps; ++step) {
        double C; //speed of sound
        Range<scal_t> div_v(domain.size(), 0);     // divergence
        time_1 = std::chrono::high_resolution_clock::now();
        int i, i_count, i_count_cumulative;

        for (i_count = 1; i_count < _MAX_ITER_; ++i_count){
            // Navier-Stokes
            #pragma omp parallel for private(i) schedule(static)
            for (i = 0; i < interior.size(); ++i) {
                int c = interior[i];
                v_2[c] = v_1[c] + O.dt * (-1 / O.rho * op.grad(P_1, c)
                                          + O.mu / O.rho * op.lap(v_1, c)
                                          - op.grad(v_1, c) * v_1[c]
                                          + O.g * (1 - O.beta * (T_1[c] - O.T_ref)));
            }

            //Speed of sound
            Range<scal_t> norm = v_2.map([](const vec_t& p) {return p.norm();});
            C = O.dl*std::max(*std::max_element(norm.begin(), norm.end()),O.v_ref);
            // PV coupling -- Mass continuity
            #pragma omp parallel for private(i) schedule(static)
            for (i = 0; i < domain.size(); ++i) {
                div_v[i] = op.div(v_2, i);
                P_2[i] = P_1[i] - C * C * O.dt * O.rho * div_v[i] +
                         O.dl2 * C * C * O.dt * O.dt * op.lap(P_1, i);
            }
            P_1.swap(P_2);
            if(*std::max_element(div_v.begin(), div_v.end()) < O.max_div) break;
        }
        i_count_cumulative+= i_count;
        // heat transport
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < interior.size(); ++i) {
            int c = interior[i];
            T_2[c] = T_1[c] + O.dt * O.lam / O.rho / O.c_p * op.lap(T_1, c) -
                     O.dt * v_1[c].transpose() * op.grad(T_1, c);
        }
        // heat Neumann condition
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < top.size(); ++i) {
            int c = top[i];
            T_2[c] = op.neumann(T_2, c, vec_t{0, -1}, 0.0);
        }
        #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < bottom.size(); ++i) {
            int c = bottom[i];
            T_2[c] = op.neumann(T_2, c, vec_t{0, 1}, 0.0);
        }

        // time step
        v_1.swap(v_2);
        T_1.swap(T_2);

        // intermediate IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            //prepare Nusselt
            Range<scal_t> Nu; //normalized Nusselt
            for (auto c : all)
                Nu.push_back((v_2[c][0]*T_2[c]*O.c_p*O.rho/O.lam - op.grad(T_2, c)[0])*
                             O.height/(O.T_hot - O.T_cold));

            O.hdfWrite("Nu", Nu);
            O.hdfWrite("v", v_2);
            O.hdfWrite("p", P_2);
            O.hdfWrite("T", T_2);
            O.hdfWrite("time", (O.dt * step));
            O.closeHDF();
            O.out_recordI++;
        }
        // on screen reports
        if ((O.on_screen > 0 && std::chrono::duration<double>(time_1 - time_2).count() > O.on_screen )
            || step == O.t_steps - 1 || step == 0) {
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);
            Range<scal_t> Nu_x, v_norm;     // Nu
            for (auto c : interior)
                v_norm.push_back(std::pow(v_2[c][0]*v_2[c][0] + v_2[c][1]*v_2[c][1],0.5));  // dimensional velocity -- * O.rho * O.c_p / O.lam
            for (auto c : left)
                Nu_x.push_back(-op.grad(T_2, c)[0] * O.height /
                               (O.T_hot - O.T_cold));  // Nusselt

            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "#:" << step << "-" << std::setprecision(1)
                      << (double) step / O.t_steps * 100 << " % " << std::setprecision(0)
                      << "exe[m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "max v:" << *std::max_element(v_norm.begin(), v_norm.end())
                      << std::setprecision(2)
                      << " C:" << C << " "
                      << " PV:" << static_cast<double> (i_count_cumulative) / (step - step_history + 1) << " "
                      << " div:" << *std::max_element(div_v.begin(), div_v.end()) << " "
                      << " Nu_l:" << std::accumulate(Nu_x.begin(), Nu_x.end(), 0.0) / Nu_x.size() << std::endl;
            i_count_cumulative = 0;
            time_2 = time_1;
            step_history = step;

            if ( std::isnan(*std::max_element(v_norm.begin(), v_norm.end()))) break;
        }
    }

    O.timings.addCheckPoint("time_end");
    double exec_time =
            std::chrono::duration<double>(O.timings.getTime("time_start", "time_end")).count();

    // DEBUG OUTPUTS
    /*
    Range<vec_t> pos_mid_x, pos_mid_y, pos_left;
    pos_mid_x = domain.positions[mid_plane_x];
    pos_mid_y = domain.positions[mid_plane_y];
    pos_left = domain.positions[left];
    */

    O.debug_out << "exec_time = " << exec_time << ";\n";
    /*O.debug_out << "v_mid_x = " << v_mid_x << ";\n";
    O.debug_out << "v_mid_y = " << v_mid_y << ";\n";
    O.debug_out << "pos_mid_x = " << pos_mid_x << ";\n";
    O.debug_out << "pos_mid_y = " << pos_mid_y << ";\n";
    O.debug_out << "pos_left = " << pos_left << ";\n";*/
    O.debug_out << "Ra = " << O.Ra << ";\n";
    O.debug_out << "rho = " << O.rho << ";\n";
    O.debug_out << "cp = " << O.c_p << ";\n";
    O.debug_out << "lam = " << O.lam << ";\n";
}
