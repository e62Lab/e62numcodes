#ifndef EXAMPLES_FLUID_FLOW_OVERSEER_HPP_
#define EXAMPLES_FLUID_FLOW_OVERSEER_HPP_

#include "io.hpp"
/**
 * @brief class with all global thingies like params, timers ...
 */
using namespace mm;
template <typename scal_t, typename vec_t>
class Overseer {
  private:
    XMLloader xml;

  public:
    HDF5IO hdf_out;
    HDF5IO hdf_in;

    std::ofstream debug_out;
    std::string hdf_out_filename;
    // Timer
    Timer timings;
    // MLS params
    std::ofstream out_file;
    int out_recordI;

    int num_threads;

    int n;  // support size;
    int m;  // number of basis or order of basis
    scal_t sigmaW;  // squared sigma for weight
    scal_t sigmaB;  // squared sigma for Gaussian basis
    // num param
    scal_t dt;  // time step
    scal_t dt_i;// time step for implicit solver

    int Cu_f;
    scal_t Cu_max;
    scal_t dl;  // square of speed of sound ACM
    scal_t dl2;  // CBS relax param
    scal_t v_ref; // refrence velocity for ACM speed of sound estimation
    scal_t max_div; // max allowed divergence in coupling
    vec_t d_space;  // discretization step
    Vec<int, 2> d_num; //num nodes;
    scal_t fill_proximity;
    size_t relax_iter_num;
    scal_t relax_initial_heat, relax_final_heat;
    int t_steps, t_steps_i;
    // phy params
    scal_t rho, mu, lam, c_p;
    scal_t g_0, beta, T_ref;
    vec_t g;

    scal_t time;  // time
    vec_t domain_lo;  // domain lo point
    vec_t domain_hi;  // domain hi point
    scal_t width;  // half width of domain
    scal_t height;  // height of domain

    scal_t T_hot;
    scal_t T_cold;
    scal_t T_init;

    scal_t Ra;  //rayleigh number

    // io params
    double out_no;  // number of outputs
    scal_t on_screen;  // number of outputs
    std::string load_from_file;
    int load_from_step;
    size_t input_hdf5_steps;

    Overseer() : hdf_out(1) {};
    ~Overseer() { debug_out.close(); }
    template <typename string_array_t>
    void operator()(int argc, string_array_t argv) {
        //file logic
        mkdir("out", 0755);
        std::string fn;
        if (argc < 2) fn = "../params/default";
        else fn = std::string(argv[1]);
        //out names is made of executable name and param file name
        std::stringstream ss;
        ss << mm::split(argv[0], '/').back() <<"_"<< mm::split(std::string(fn), '/').back();
        std::string filename = ss.str();
        std::string debug_out_filename = "out/" + filename + ".m";
        //Set output files
        hdf_out_filename = "out/" + filename + ".h5";
        std::remove(hdf_out_filename.c_str());  // remove old h5

        hdf_out.openFile(hdf_out_filename, HDF5IO::DESTROY);  // create new blank file, ignore errors
        hdf_out.closeFile();

        debug_out.open(debug_out_filename, std::ofstream::out);  // debug out file
        assert_msg(debug_out.is_open(), "Error opening file '%s': '%s'",
                   debug_out_filename, strerror(errno));

        //load other parameters from xml
        xml(fn + ".xml");

        std::cout << "loading params, using: ";
        print_white(fn + ".xml\n");

        m = xml.get<int>("params.mls.m");
        n = xml.get<int>("params.mls.n");

        sigmaW = xml.get<double>("params.mls.sigmaW");
        sigmaB = xml.get<double>("params.mls.sigmaB");

        relax_iter_num = xml.get<size_t>("params.relax.iter_num");
        relax_initial_heat = xml.get<size_t>("params.relax.initial_heat");
        relax_final_heat = xml.get<size_t>("params.relax.final_heat");
        fill_proximity = xml.get<double>("params.domain.fill_proximity");

        dt = xml.get<double>("params.num.dt");

        Cu_max = xml.get<double>("params.num.Cu_max");
        Cu_f = xml.get<double>("params.num.Cu_f");


                dt_i = xml.get<double>("params.num.dt_i");
        dl = xml.get<double>("params.num.dl");
        dl2 = xml.get<double>("params.num.dl2");
        v_ref = xml.get<double>("params.num.v_ref");
        max_div = xml.get<double>("params.num.max_div");

        d_space = xml.get<double>("params.num.d_space");
        //d_num = xml.get<int>("params.num.d_space");

        width = xml.get<double>("params.case.width");
        height = xml.get<double>("params.case.height");
        T_hot = xml.get<double>("params.case.T_hot");
        T_cold = xml.get<double>("params.case.T_cold");
        T_init = xml.get<double>("params.case.T_init");

        time = xml.get<double>("params.phy.time");
        rho = xml.get<double>("params.phy.rho");
        mu = xml.get<double>("params.phy.mu");
        lam = xml.get<double>("params.phy.lam");
        c_p = xml.get<double>("params.phy.c_p");
        g_0 = xml.get<double>("params.phy.g_0");
        beta = xml.get<double>("params.phy.beta");
        T_ref = xml.get<double>("params.phy.T_ref");

        out_no = xml.get<double>("params.io.out_num");
        on_screen = xml.get<double>("params.io.on_screen_num");
        load_from_file = xml.get<std::string>("params.io.load_from_file");
        load_from_step = xml.get<int>("params.io.load_from_step");
        num_threads = xml.get<int>("params.sys.num_threads");

        //params LOGIC
        g[1] = g_0;

        t_steps = std::floor(time / dt);
        t_steps_i = std::floor(time / dt_i);


        domain_lo = vec_t(0, 0);
        domain_hi = vec_t(width, height);
        // outputs
        std::cout << "-----------------------------------------------------------" << std::endl;
        xml.doc.Print();
        std::cout << std::endl;

        omp_set_num_threads(num_threads);

        std::cout << "Re = " << rho / mu << std::endl;
        Ra = std::abs(g_0) * beta * rho * rho * c_p * std::pow(height, 3) *
             std::abs(T_hot - T_cold) / (lam * mu);
        std::cout << "Ra = " << Ra
                  << std::endl;
        std::cout << "Pr = " << mu * c_p / lam << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl;

        mm::print_green("Params loaded ... \n");

        // open in file
        // load logic: put an in file into the in/ folder. Note, that the in file is of the
        // same format as an out file.
        if (load_from_file != "") {
            std::string in_file = "in/" + load_from_file + ".h5";
            hdf_in.openFile(in_file, HDF5IO::READONLY);
            input_hdf5_steps = hdf_in.ls().size() - 1;
            print_green("Input HDF5 file loaded ");
            std::cout << " with " << input_hdf5_steps << " time steps ";
            if (load_from_step == -1) load_from_step = input_hdf5_steps;  // load final
            assert_msg(static_cast<size_t>(load_from_step) <= input_hdf5_steps,
                       "You want to load %i-th step from HDF5 with %f steps", load_from_step,
                       input_hdf5_steps);
            std::cout << " -- Loading " << load_from_step << "-th time step\n";
        }
    }

    /// hdf overloads
    void reOpenHDF() { hdf_out.openFile(hdf_out_filename); }
    void closeHDF() { hdf_out.closeFile(); }
    void hdfWrite(std::string var_name, const std::vector<int>& v) {
        hdf_out.setIntArray(var_name, v, {static_cast<size_t>(std::ceil(v.size() / 5.))});
    }
    void hdfWrite(const std::string& var_name, const Range<Range<int>>& v) {
        hdf_out.setInt2DArray(
            var_name, v,
            {static_cast<size_t>(std::ceil(v.size() / 5.)), static_cast<size_t>(v[0].size())});
    }
    void hdfWrite(const std::string& var_name, const std::vector<std::vector<int>>& v) {
        hdf_out.setInt2DArray(
            var_name, v,
            {static_cast<size_t>(std::ceil(v.size() / 5.)), static_cast<size_t>(v[0].size())});
    }
    void hdfWrite(const std::string& var_name, const std::vector<vec_t>& v) {
        hdf_out.setDouble2DArray(
            var_name, v,
            {static_cast<size_t>(std::ceil(v.size() / 5.)), static_cast<size_t>(v[0].size())});
    }
    void hdfWrite(const std::string& var_name, double v) { hdf_out.setDoubleAttribute(var_name, v);}
    void hdfWrite(const std::string& var_name, const std::vector<std::vector<double>>& v) {
        hdf_out.setDouble2DArray(
            var_name, v,
            {static_cast<size_t>(std::ceil(v.size() / 5.)), static_cast<size_t>(v[0].size())});
    }
    void hdfWrite(const std::string& var_name, const std::vector<double>& v) {
        hdf_out.setDoubleArray(var_name, v, {static_cast<size_t>(std::ceil(v.size() / 5.))});
    }
    void hdfWrite(const std::string& var_name, const VecXd& v) {hdf_out.setDoubleArray(var_name, v, static_cast<size_t>(std::ceil(v.size() / 5.)));}

    Range<vec_t> hdfReadVec(const std::string& var_name) {
        std::vector<std::vector<double>> in = hdf_in.getDouble2DArray(var_name);
        Range<vec_t> out;
        for (size_t i = 0; i < in.size(); ++i) out.push_back(vec_t{in[i][0], in[i][1]});

        return out;
    }
    Range<Range<int>> hdfRead2DInt(const std::string& var_name) {
        Range<Range<int>> out;
        std::vector<std::vector<int>> in = hdf_in.getInt2DArray(var_name);
        for (size_t i = 0; i < in.size(); ++i) out.push_back(in[i]);
        return out;
    }
};

#endif  // EXAMPLES_FLUID_FLOW_OVERSEER_HPP_
