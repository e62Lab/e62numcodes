/*
[Solution of de Vahl Davis natural convection benchmark test
 :Full implicit with PP
]
*/
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "overseer.hpp"
#include "types.hpp"
#include <omp.h>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#define _MAX_ITER_ 500

using namespace mm;
using namespace Eigen;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char* arg[]) {
    typedef SparseMatrix<double, RowMajor> mat_t;
    mat_t M_velocity, M_temperature;

    VecXd T, P;

    O.timings.addCheckPoint("time_start");
    O(arg_num, arg);

    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi);  // domain definition

    domain.fillUniform(O.d_num - Vec<int, 2>(2), O.d_num );
    prn(domain.size());
    size_t N = domain.size();

    Range<int> all = domain.types.filter([](vec_t p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    double tol = 1e-10;
    Range<int> top = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_hi[1]) < tol; }),
            bottom = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[1] - O.domain_lo[1]) < tol; }),
            right = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_hi[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; }),
            left = domain.positions.filter([=](const Vec2d&p) { return std::abs(p[0] - O.domain_lo[0]) < tol && std::abs(p[1] - O.domain_hi[1]) > tol && std::abs(p[1] - O.domain_lo[1]) > tol; });
    domain.findSupport(O.n, all);

    BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver_T;
    BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver_v;
    SparseLU<SparseMatrix<double>> solver_p;

    O.timings.addCheckPoint("setup");

    EngineMLS<vec_t, Monomials, NNGaussians> mls
            ({O.sigmaB, O.m},
             O.sigmaW);
    auto op = make_mlsm(domain, mls, all, false);

    O.timings.addCheckPoint("time loop");
    // init state

    Range<vec_t> v_1(domain.size(), 0);
    Range<vec_t> v_2(domain.size(), 0);


    // hydrostatic initial pressure
    for (auto i : all) {
        P[i] = O.g_0 * O.rho * domain.positions[i][1]*(1 - O.beta * (T[i] - O.T_ref));
    }

    std::chrono::high_resolution_clock::time_point time_1, time_2;
    int step_history = 0;


    // PRESSURE MATRIX
    SparseMatrix<double, RowMajor>
            M_pressure(N + 1, N + 1);
    Range<int> res(domain.size() + 1, O.n + 1);
    res[res.size() - 1] = domain.size() + 1;
    M_pressure.reserve(res);
    // pressure air-alu boundary conditions dp/dn = 0;
    for (int i : top) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : left) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : right) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    for (int i : bottom) op.neumann_implicit(M_pressure, i, domain.normal(i), 1);
    //Poisson equation
    for (int i : interior) op.lap(M_pressure, i, 1.0);
    //regularization
    // Integral P = 0, average P = 0, referred to also as regularization
    for (int i = 0; i < domain.size(); ++i) {
        M_pressure.coeffRef(domain.size(), i) = 1;
        M_pressure.coeffRef(i, domain.size()) = 1;
    }
    M_pressure.makeCompressed();
    SparseMatrix<double> MM(M_pressure);
    solver_p.compute(MM);
    // END OF PRESSURE MATRIX


    // TIME ITERATION
    for (int step = 0; step <= O.t_steps; ++step) {
        // NS MATRIX
        M_velocity = mat_t(2 * N, 2 * N);
        // Navier stokes
        M_velocity.reserve(Range<int>(2 * N, O.n));
        for (int i : interior) {
            op.valuevec(M_velocity, i, 1 / O.dt);
            op.lapvec(M_velocity, i, -O.mu / O.rho);
            op.gradvec(M_velocity, i, v_1[i]);
        }
        for (int i : top) op.valuevec(M_velocity, i, 1); //sets velocity to 0
        for (int i : bottom) op.valuevec(M_velocity, i, 1); //sets velocity to 0
        for (int i : right) op.valuevec(M_velocity, i, 1); //sets velocity to 0
        for (int i : left) op.valuevec(M_velocity, i, 1); //sets velocity to 0

        M_velocity.makeCompressed();
        solver_v.compute(M_velocity);

        Range<vec_t> rhs_vec(domain.size(), 0);
        for (int i : interior) {
            rhs_vec[i] = -1 / O.rho * op.grad(P, i)
                         + v_1[i] / O.dt
                         + O.g * (1 - O.beta * (T[i] - O.T_ref));
        }
        v_2 = reshape<2>(solver_v.solveWithGuess(reshape(rhs_vec), reshape(v_1)));
        // END OF NAVIER STOKES

        // PRESSURE CORRECTION
        VecXd rhs_pressure(N + 1, 0); //Note N+1, +1 stands for regularization equation
        rhs_pressure(N) = 0; // = 0 part of the regularization equation
        double alpha;
        double max_div;
        for (int i : interior) rhs_pressure(i) = O.rho / O.dt * op.div(v_2, i);

        max_div = (*std::max_element(rhs_pressure.begin(),
                                     rhs_pressure.end())) * O.dt * O.rho;
        VecXd solution = solver_p.solve(rhs_pressure);
        alpha = solution[N];
        VecXd P_c = solution.head(N);
        // apply velocity correction
        for (int i : interior) {
            v_2[i] -= O.dl * O.dt / O.rho * op.grad(P_c, i);
        }
        P += O.dl * P_c;
        // END OF PRESSURE CORRECTION

        // HEAT TRANSPORT
        M_temperature = mat_t(N, N);
        Range<int> per_row(N, O.n);
        M_temperature.reserve(per_row);
        // outer boundary dirichlet BC
        for (int i : left) op.neumann_implicit(M_temperature, i, domain.normal(i), 1);
        for (int i : right) op.neumann_implicit(M_temperature, i, domain.normal(i), 1);
        for (int i : bottom) op.value(M_temperature, i, 1.0);
        for (int i : top) op.value(M_temperature, i, 1.0);
        // heat transport in air
        for (int i : interior) {
            op.value(M_temperature, i, 1.0);                          // time dependency
            op.lap(M_temperature, i, -O.dt * O.lam / O.rho / O.c_p);  //laplace in interior
        }

        M_temperature.makeCompressed();
        solver_T.compute(M_temperature);

        VectorXd rhs = VectorXd::Zero(N);
        for (int i : interior) rhs(i) = T(i);

        for (int i : top) rhs(i) = 0;
        for (int i : bottom) rhs(i) = 0;
        for (int i : left) rhs(i) = O.T_hot;
        for (int i : right) rhs(i) = O.T_cold;
        T = solver_T.solveWithGuess(rhs, T);
        // END OF HEAT TRANSPORT
        v_1.swap(v_2);

        // INTERMEDIATE IO
        if (step % (1 + O.t_steps / O.out_no) == 0 || step == O.t_steps - 1) {
            O.reOpenHDF();
            // assigning new time step
            O.hdf_out.createFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.openFolder("/step" + std::to_string(O.out_recordI));
            O.hdf_out.setDoubleAttribute("TimeStep", step);
            // initial output :: nodes, supports and all other static stuff
            if (step == 0) {
                O.hdfWrite("support", domain.support);
                O.hdfWrite("types", domain.types);
                O.hdfWrite("pos", domain.positions);
            }

            //prepare Nusselt
            Range<scal_t> Nu; //normalized Nusselt
            for (auto c : all)
                Nu.push_back((v_2[c][0]*T[c]*O.c_p*O.rho/O.lam - op.grad(T, c)[0])*
                             O.height/(O.T_hot - O.T_cold));
            O.hdfWrite("Nu", Nu);
            O.hdfWrite("v", v_2);
            //O.hdfWrite("p", P);
            O.hdfWrite("T", T);
            O.hdfWrite("time", (O.dt * step));
            O.closeHDF();
            O.out_recordI++;
        }
        // on screen reports
        if ((O.on_screen > 0 && std::chrono::duration<double>(time_1 - time_2).count() > O.on_screen )
            || step == O.t_steps - 1 || step == 0) {
            double time_elapsed = (O.timings.getTimeToNow("time loop"));
            double time_estimated = time_elapsed * (O.t_steps / (step + 0.001) - 1);

            Range<scal_t> Nu_x;     // Nu
            Range<double> v_tmp;
            for (auto c : all)
                v_tmp.push_back(std::pow(v_2[c][0]*v_2[c][0] + v_2[c][1]*v_2[c][1],0.5));

            for (auto c : left)
                Nu_x.push_back(-op.grad(T, c)[0] * O.height /
                               (O.T_hot - O.T_cold));  // Nusselt

            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "#:" << step << "-" << std::setprecision(1)
                      << (double) step / O.t_steps * 100 << " % " << std::setprecision(0)
                      << "exe[m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                      << time_estimated / 60 << " " << std::setprecision(4)
                      << "v_max:" << *std::max_element(v_tmp.begin(), v_tmp.end())
                      << std::setprecision(2)
                      << " Nu_l:" << std::accumulate(Nu_x.begin(), Nu_x.end(), 0.0) / Nu_x.size() << std::endl;
            time_2 = time_1;
            step_history = step;

        }
    }

    O.timings.addCheckPoint("time_end");
    double exec_time =
            std::chrono::duration<double>(O.timings.getTime("time_start", "time_end")).count();
}
