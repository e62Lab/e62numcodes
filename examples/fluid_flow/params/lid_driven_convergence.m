clc;
parent_file = 'lid_driven_re100.xml';
out_dir = 'generated/';

if (7==exist(out_dir,'dir')) rmdir(out_dir, 's');end
mkdir(out_dir)


%% lid Driven convergence params
i = 1;
for N = 10.^[2:0.1:4.5]
  
    offspring = [out_dir,sprintf('a%06d.xml',i)];
    copyfile(parent_file, offspring) 

    docNode = xmlread(offspring);
    a = docNode.getElementsByTagName('params');
    b = a.item(0).getElementsByTagName('num');
    b.item(0).setAttribute('d_space', num2str(1/floor(N)^0.5))

    xmlwrite(offspring, docNode);
    i = i + 1;
end