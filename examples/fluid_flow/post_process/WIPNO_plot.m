function mainPlot()
tic    
%%SETUP
  close all
    clc;clear;
    cmap = open('cmap.mat');
    
    fontname = 'Times';
    set(0,'defaultaxesfontname',fontname);
    set(0,'defaulttextfontname',fontname);
    
    fontsize = 13;
    set(0,'defaultaxesfontsize',fontsize);
    set(0,'defaulttextfontsize',fontsize);
    
    working_dir='../bin/out/'; addpath(working_dir);
%     working_dir='Q:/thermo/out/'; 
    
    case_name='de_Vahl_Davis_explicit_ACM_DVD_realistic';
    case_name='de_Vahl_Davis_explicit_PP_DVD_realistic';
    case_name='de_Vahl_Davis_implicit_PP_DVD_realistic';
    case_name='NC_open_channel_ACM_NC_realistic';
    case_name='NC_open_channel_PP_NC_realistic';
%     case_name='NC_open_channel_cylinder_PP_NC_realistic';
    
%     case_name='de_Vahl_Davis_explicit_ACM_DVD_Ra1e8_100';
%     case_name='de_Vahl_Davis_explicit_ACM_a000079';
%     case_name='thermo_fluid_cylinder_Ra1e6_Pr01';
%     case_name='thermo_fluid_cylinder_Ra1e6_Pr10';
%     case_name='thermo_fluid_cylinder_TFC_wip';
%     case_name='thermo_fluid_cylinder_a000280';
    

    %% PLOT DEFINITIONS
    plot_pos        = 1;
        plot_IT     = 1;
        scale       = 1.5;
    
    contour_resolution = [50,50]; level_step = 1;
    %REGISTERED VARIABLES -- variables to load from HDF5
    regV={'v', 'T', 'Nu'};
    atts = {'time'};  
    load_data;
   
    if 0
        %%
        M = spconvert(h5read(fn, '/M')');
        disp(['cond no of a system: ', num2str(condest(M),'%e')]);
        spy(M);
        %%
    end
    %%
    %FIGURE CONFIG
    fig=createFig('ps', 'm2 1x2 p1','scale',0);hold on;    
    
    axis('equal');

    %%REGISTER PLOTS
    rP={};iip=0;%registered plots - system variable thaT have to be filled for each plot
    rS={};iis=0;%registered scattered plots - system variable thaT have to be filled for each plot
    rV={};iiv=0;%registered vector plots
   
    if plot_pos && exist('pos') && ~exist('pp') 
        if 1 %contour plot - only one allowed
            if sum(isnan(T)) == 0 
               rC =  {'pos(1,:)', 'pos(2,:)', 'T' }; 
               [h, pc] = scontour(eval(cell2mat(rC(1))), eval(cell2mat(rC(2))), eval(cell2mat(rC(3))), contour_resolution(1), contour_resolution(1)...
                   ,'levelstep',level_step, 'linestyle', 'none', 'fill', 'on');
               colormap(cmap.cmap1)
               colorbar;
            end
        end
        
        if 0 %nodes plotting
           iip=iip+1;
            rP=[rP;{'pos(1,:)','pos(2,:)'}];
            xp=plot(eval(cell2mat(rP(end,1))),eval(cell2mat(rP(end,2))),'k.', 'markersize', 5);   
            pp(iip)=xp; 
        end
        
        if 0 %register scatter plot
            iis=iis+1;
            if ~exist(scattvar,'var') eval([scattvar,'=ones(1, length(v));']);end
            rS=[rS;{'pos(1,types>0)','pos(2,types>0)',[scattvar,'(types>0)']}];
            xp=scatter(eval(cell2mat(rS(end,1))),eval(cell2mat(rS(end,2))),[8],eval(cell2mat(rS(end,3))),'marker','.');   
            ps(iis)=xp; 
        end   
        
        if 1 %register quiver plot 
            iiv=iiv+1;
            rV=[rV;{'pos(1,:)','pos(2,:)','v(1,:)','v(2,:)'}];
            pv(iiv)=quiver(eval(cell2mat(rV(end,1))),eval(cell2mat(rV(end,2))),eval(cell2mat(rV(end,3))),eval(cell2mat(rV(end,4))),scale,'color','k');   
        end  
    end
  
    closest_pair(pos(1,:),pos(2,:))
    
    evalin('base','clear')
    W = who; 
    putvar(W{:});
    
    if plot_IT
        set(fig,'KeyPressFcn',@keyDownListener);
    end
    toc
    
    %%% Garbage place
    if 0
        figx=createFig('monitor',2,'divider',1,'divider_y',2,'pos',1,'scale',0);hold on;    
        plot(pos_left(:,2), Nu_x,'+');
    end
    
    %%%
end

%%IT TOOLS functions
function keyDownListener(~, event)
    switch event.Key
    case 'leftarrow'
        evalin('base',...
        'curr_step=curr_step-1;if (curr_step < 0) curr_step=no_steps-1; end;');
    case 'rightarrow'
        evalin('base',...
        'curr_step=curr_step+1;if (curr_step == no_steps) curr_step=0; end');
    end
    
    %reload registered data
    evalin('base', 'ii=curr_step');
    evalin('base', 'load_data');
    %step=['/step',num2str(evalin('base','curr_step')),'/'];
    %fn=evalin('base','fn');
    %for var=evalin('base','regV') evalin('base',[cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
    
   %update registered ordinary plots 
    if 0
        rP=evalin('base','rP'); 
        for i=1:size(rP,1)
          set(evalin('base',['pp(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rP(',num2str(i),',1)))']));  
          set(evalin('base',['pp(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rP(',num2str(i),',2)))'])); 
        end
    end
    
    %update registered scatter plots 
    rS=evalin('base','rS'); 
    for i=1:size(rS,1)
      set(evalin('base',['ps(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',1)))']));  
      set(evalin('base',['ps(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rS(',num2str(i),',2)))']));
      set(evalin('base',['ps(',num2str(i),')']), 'cdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',3)))']));  
    end
    
    %update registered vector plots 
    rV=evalin('base','rV'); 
    for i=1:size(rV,1)
      set(evalin('base',['pv(',num2str(i),')']), 'udata',evalin('base',['eval(cell2mat(rV(',num2str(i),',3)))']));  
      set(evalin('base',['pv(',num2str(i),')']), 'vdata',evalin('base',['eval(cell2mat(rV(',num2str(i),',4)))']));  
    end
    %update contour plot
    if(evalin('base', 'exist(''pc'',''var'')'))
        rC = evalin('base','rC');
        [~, ~, zz] = ...
        evalin('base', ['interpolateToMeshGrid('...
            ,cell2mat(rC(1)), ',', cell2mat(rC(2)),',', cell2mat(rC(3)),...
            ',contour_resolution(1), contour_resolution(2), ''interpolation'', ''cubic'', ''out'', ''mesh'')']);  
        x = evalin('base','pc'); 
        x.ZData = zz;
    end
    evalin('base','title([''t='',num2str(time,''%2.2f'')])')
    disp([num2str(evalin('base','curr_step'))]);
end


function [min_dist, min_p1, min_p2] = closest_pair(x, y)

if isrow(x), x = x'; end
if isrow(y), y = y'; end

KDT = KDTreeSearcher([x, y]);
idx = knnsearch(KDT, [x, y], 'K', 2);
dist = sqrt((x - x(idx(:, 2))).^2 + (y - y(idx(:, 2))).^2);
[min_dist, I] = min(dist);
min_p1 = I;
min_p2 = idx(I, 2);

end

