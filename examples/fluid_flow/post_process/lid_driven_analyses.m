clear; clc
close all

fontname = 'Times';
set(0, 'defaultaxesfontname', fontname);
fontsize = 16;
set(0, 'defaultaxesfontsize', fontsize);
LineStyle = 'none'; %LineStyle='-';
Format = 'eps'; %eps %bmp, jpeg70 (stevilka je quality), P - preview mode
Resolution = 400;
Color = 'rgb';
cmap = open('cmap.mat');

LS = {':', '-', ':', '--', '-', ':', '--', '-', ':'};
MS = {'+', 'square', '>', 'o', '<', '.', 'pentagram', 'diamond', '*', 'x', '^'};
MSS = 7;

dir = 'data\lid_driven\';
addpath(dir)

%% velocity magnitude contour plot
if 0
    load([dir, 'lid_driven_explicit_Re_3200_2D.mat'])
    [xim, yim, fim] = interpolateToMeshGrid(pos(1, :), pos(2, :), (v(1, :).^2 + v(2, :).^2).^0.5, 101, 101, 'interpolation', 'cubic', 'out', 'mesh');
    f1 = createFig('monitor', 2, 'divider_y', 2, 'divider', 2, 'pos', 1);
    contour(xim, yim, fim, 'fill', 'on', 'levelstep', 0.005, 'linestyle', 'none')
    colormap(cmap.cmap1)
    
    xlabel('$x$', 'interpreter', 'latex');
    ylabel('$y$', 'interpreter', 'latex');
    title('velocity magnitude for Re=3200 case')

    exportfig(f1, 'figures/lid_driven_contour_Re3200.png', 'format', 'png', 'color', 'rgb', 'Resolution', 200, 'Bounds', 'loose')
    
    load([dir, 'lid_driven_explicit_Re_100_2D.mat'])
    [xim, yim, fim] = interpolateToMeshGrid(pos(1, :), pos(2, :), (v(1, :).^2 + v(2, :).^2).^0.5, 101, 101, 'interpolation', 'cubic', 'out', 'mesh');
    f1 = createFig('monitor', 2, 'divider_y', 2, 'divider', 1, 'pos', 2);
    contour(xim, yim, fim, 'fill', 'on', 'levelstep', 0.005, 'linestyle', 'none')
    colormap(cmap.cmap1)
    xlabel('$x$', 'interpreter', 'latex');
    ylabel('$y$', 'interpreter', 'latex');
    title('velocity magnitude for Re=100 case')
    exportfig(f1, 'figures/lid_driven_contour_Re100.png', 'format', 'png', 'color', 'rgb', 'Resolution', 200, 'Bounds', 'loose')
end

%% comparison
if 1; f1 = createFig('monitor', 2, 'divider_y', 2, 'divider', 1, 'pos', 1, ...
        'ylabel', '$v_y$', 'xlabel', '$x$', 'scale', 0, 'fontsize', 12);
    LG = [];
    
    hold on; grid on;
    axis([0, 1, -0.55, 0.57]);
    axis('tight');

%     A = load([dir, 'reference\Re100_vy.txt']');
%     plot(A(:, 1), A(:, 3), '.');
%     LG = [LG, {'Re=100 Reference'}];
%     lid_driven_explicit_re100
%     plot(pos_mid(:, 1), v_mid(:, 2), '.');
%     LG = [LG, {'explicit MLSM Re=100 solution'}];
%     set(gca, 'fontsize', 18);
    
    A = load([dir, 'reference\Re1000_vy.txt']');
    plot(A(:, 1), A(:, 3), '.');
    LG = [LG, {'Re=1000 Reference'}];
    lid_driven_explicit_re1000
    plot(pos_mid(:, 1), v_mid(:, 2), '.');
    LG = [LG, {'explicit MLSM Re=1000 solution'}];
    
%     lid_driven_wip
    plot(pos_mid(:, 1), v_mid(:, 2), '.');
    LG = [LG, {'implicit MLSM Re=1000 solution'}];
    
    set(gca, 'fontsize', 18);
    
    A = load([dir, 'reference\Re3200_vy.txt']');
    plot(A(:, 1), A(:, 3), '.');
    LG = [LG, {'Re=3200 Reference'}];
    lid_driven_explicit_re3200
    plot(pos_mid(:, 1), v_mid(:, 2), '.');
    LG = [LG, {'explicit MLSM Re=3200 solution'}];

    axis([0 1 -0.7 0.5])
    
    h = legend(LG);

%     exportfig(f1, 'figures/lid_driven_comparison.png','format','png','color','rgb','Resolution',200,'Bounds','loose')
end

if 0 % exec time
    LG = [];
    f1 = createFig('monitor', 2, 'divider_y', 2, 'divider', 1, 'pos', 1, ...
        'ylabel', '$v_y$', 'xlabel', '$x$', 'scale', 0, 'fontsize', 12);
    hold on; grid on;
    axis('tight');
    % RE 100
    load([dir, 'lid_driven_explicit_Re_100_2D_conv'])
    fig = createFig('monitor', 2, 'divider', 1, 'divider_y', 2);
    plot(conv(:, 1).^2, conv(:, 2), '-+');
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    set(gca, 'fontsize', 18)
    LG = [LG, {'MLSM explicit execution time'}];
    axis([100, 10^4.7, 1e-1, 1e3])
    grid on;
    h = legend(LG);
    xlabel('N');
    ylabel('exec time [s]');
    exportfig(fig, 'figures/lid_driven_explicit_exec_time.png', 'format', 'png', 'color', 'rgb', 'Resolution', 200, 'Bounds', 'loose')
end
if 0 %convergence
    f1 = createFig('monitor', 2, 'divider_y', 2, 'divider', 1, 'pos', 1, ...
        'ylabel', '$v_y$', 'xlabel', '$x$', 'scale', 0, 'fontsize', 12);
    hold on; grid on;
    axis('tight');
    %% Re 100
    load([dir, 'lid_driven_explicit_Re_100_2D_conv'])
    fig = createFig('monitor', 2, 'divider', 1, 'divider_y', 2');
    plot(conv(:, 1).^2, conv(:, 3), '-+');
    set(gca, 'XScale', 'log')
    set(gca, 'fontsize', 18)
    LG = [LG, {'MLSM explicit midplane maximal vertical velocity'}];
    %     axis([100, 10^4.7, 1e-1, 1e3])
    grid on;
    h = legend(LG, 'location', 'best');
    xlabel('$N$', 'interpreter', 'latex');
    ylabel('$v^{max}_{midplane}$', 'interpreter', 'latex');
    exportfig(fig, 'figures/lid_driven_explicit_convergence.png', 'format', 'png', 'color', 'rgb', 'Resolution', 200, 'Bounds', 'loose')
end
