#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include <domain_fill_engines.hpp>
#include <domain_relax_engines.hpp>
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "io.hpp"
#include <Eigen/Sparse>
#include <omp.h>

using namespace mm;
using namespace Eigen;


double linear (double x1, double y1, double x2, double y2, double x){
    double k = (y2 - y1) / (x2 - x1);
    double n = y1 -k*x1;
    return k*x + n;
}

int main(int argc, char* argv[]) {
    omp_set_num_threads(3);
    if (argc < 2) {print_red("Supply parameter file as the second argument.\n"); return 1;}

    XMLloader conf(argv[1]);
    double inner_radius = conf.get<double>("params.domain.inner_radius");
    double outer_radius = conf.get<double>("params.domain.outer_radius");
    int density = conf.get<int>("params.domain.density");
    double w = conf.get<double>("params.problem.omega");
    double A = conf.get<double>("params.problem.e0");
    double v = conf.get<double>("params.problem.v");
    int n = conf.get<int>("params.mls.n");  // support size
    int m = conf.get<int>("params.mls.m");  // monomial basis of second order, i.e. 6 monomials
    int fill_seed = conf.get<int>("params.fill.seed");
    double init_heat = conf.get<double>("params.relax.init_heat");
    double final_heat = conf.get<double>("params.relax.init_heat");
    double num_neighbours = conf.get<double>("params.relax.num_neighbours");
    double riter = conf.get<double>("params.relax.riter");
    double sigma = conf.get<double>("params.mls.sigma");
    double factor = conf.get<double>("params.problem.factor");
    double devide_by_factor = 1.0 / static_cast<double >(factor);
    double time = conf.get<double>("params.problem.time");  // time
    int t_factor =conf.get<int>("params.problem.t_factor");
    double dt = conf.get<double>("params.problem.dt")/ static_cast<double >(t_factor); // time step
    int t_steps = std::ceil(time / dt);
    double dx = outer_radius / density;
    int num_cores=conf.get<int>("params.settings.num_cores");
    omp_set_num_threads(num_cores);

    auto fill_density = [=](const Vec2d& p) {
        double r = p.norm();
        double default_value = dx;
        double dens = default_value;
        double r1 = 15*inner_radius;
        double r2 = 0.8*outer_radius;
        if (r < r1) dens = linear(inner_radius, devide_by_factor*default_value, r1, default_value, r );
        if (r > r2) dens = linear(r2, default_value, outer_radius, devide_by_factor* default_value, r);
        return dens;
    };

    HDF5IO hdf_out;
    std::string hdf_out_filename = conf.get<std::string>("params.output.path");
    hdf_out.openFile(hdf_out_filename, HDF5IO::DESTROY);

    // extra boundary labels
    int ANTENNA = -10;

    // build circle domain
    CircleDomain<Vec2d> domain({0, 0}, outer_radius);
    // domain.fillUniformInterior(domain_size);
    domain.fillUniformBoundaryWithStep(devide_by_factor*dx);

    // build antenna
    CircleDomain<Vec2d> empty({0, 0}, inner_radius);
    empty.fillUniformBoundaryWithStep(devide_by_factor*dx);
    empty.types[empty.types < 0] = ANTENNA;
    domain.subtract(empty);

    PoissonDiskSamplingFill fill;
    fill.seed(fill_seed);
    domain.apply(fill, fill_density);

    BasicRelax relax;
    relax.iterations(riter).numNeighbours(num_neighbours).initialHeat(init_heat).finalHeat(final_heat);
    domain.apply(relax, fill_density);

    // find support
    domain.findSupport(n);

    int domain_size = domain.size();
    std::cout << domain_size << std::endl;

    Range<int> all = domain.types.filter([](Vec2d p) { return true; });
    Range<int> interior = domain.types > 0;
    Range<int> boundary = domain.types < 0;

    // Prepare operators and matrix
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(m, domain.positions, sigma);
    SparseMatrix<double> M(domain_size, domain_size);
//    SparseLU<SparseMatrix<double>> solver;  // system solve
    BiCGSTAB<SparseMatrix<double, RowMajor>> solver;
    M.reserve(Range<int>(domain_size, n ));
    auto op = make_mlsm(domain, mls, all, false);  // All nodes, including boundary
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(domain_size); //set empty vector for rhs
    Eigen::VectorXd E0 = Eigen::VectorXd::Zero(domain_size); //0-th step
    Eigen::VectorXd E1 = Eigen::VectorXd::Zero(domain_size); //1-st step

    // Set equation on interior
    for (int i : interior) {
        M.coeffRef(i, i) = 1;
        op.lap(M, i, -(v*v*dt*dt));  // laplace in interior
        rhs(i) = 2*E1(i)-E0(i);

    }
    hdf_out.openFolder("/");
    hdf_out.setSparseMatrix("M_start", M);
    // Set boundary conditions
    for (int i : boundary) {
        M.coeffRef(i, i) = 1;  // fixed
        if (domain.types[i]==ANTENNA){
            rhs(i) = A * std::sin(w*dt*2); //step
        } else {
            rhs(i) = 0.0;
        }
    }
    hdf_out.openFolder("/");
    hdf_out.setSparseMatrix("M_full", M);


    M.makeCompressed();
    solver.compute(M);

    hdf_out.openFolder("/");
    hdf_out.setSparseMatrix("M", M);



    // time stepping
    int tt;
    int t_save=0;
    hdf_out.openFolder("/");
    hdf_out.setDouble2DArray("pos", domain.positions);
    hdf_out.openFolder("/step" + std::to_string(t_save));
    hdf_out.setDoubleAttribute("TimeStep", t_save);
    hdf_out.setDoubleAttribute("time", 0.0);
    hdf_out.setDoubleArray("E", E1);
    ++t_save;

    for (tt = 2; tt < t_steps; ++tt) {
        if (tt%(50*t_factor)==0) {
            // assigning new time step
            hdf_out.openFolder("/step" + std::to_string(t_save));
            hdf_out.setDoubleAttribute("TimeStep", t_save);
            std::cout<<t_save<<std::endl;
            ++t_save;
        }
        // solve
        VecXd E2 = solver.solve(rhs);

        if (tt%(50*t_factor)==0) {
            hdf_out.setDoubleArray("E", E2);
            hdf_out.setDoubleAttribute("time", dt * tt);
        }
        // time advance
        E0=E1;
        E1=E2;

        // Set rhs on interior
        #pragma omp parallel for  schedule(static)
//        for (int i: interior) {
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            rhs(i) = 2*E1(i)-E0(i);
        }

        // Set boundary conditions
        for (int i : (domain.types==ANTENNA)) {
            rhs(i) = A * std::sin(w*dt*tt); //step

        }

    }
    hdf_out.closeFile();

}



