project(linear_elasticity)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_CXX_FLAGS "-std=c++14 -Wall -O3")

# add subfolder
add_subdirectory(${CMAKE_SOURCE_DIR}/../../ build_util)

# include directories
include_directories(${CMAKE_SOURCE_DIR}/../../src/)
include_directories(SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/../../src/third_party/)

# link HDF5 on Ubuntu
link_directories(/usr/lib/x86_64-linux-gnu/hdf5/serial/)

# define your own executables below
add_executable(point_contact point_contact.cpp)
target_link_libraries(point_contact pde_utils)

add_executable(cantilever_beam cantilever_beam.cpp)
target_link_libraries(cantilever_beam pde_utils)

add_executable(hertzian_contact hertzian_contact.cpp)
target_link_libraries(hertzian_contact pde_utils)