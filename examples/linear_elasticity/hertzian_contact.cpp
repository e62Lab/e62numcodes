#include "domain.hpp"
#include "domain_extended.hpp"
#include "mls.hpp"
#include "io.hpp"
#include "mlsm_operators.hpp"
#include <Eigen/Sparse>
#include <unsupported/Eigen/SparseExtra>
#include "draw.hpp"

using namespace mm;
using namespace Eigen;

int main(int arg_num, char* arg[]) {
    // This is a harder case representing a contact of a disk with a half plane.
    // The contact area is very small and requires extensive refinement.

    // Physical parameters
    double E = 72.1e9;
    double v = 0.33;
    double R = 1.0;
    double P = 543;
    // Derived parameters
    double mu = E / 2. / (1+v);
    double lam = E * v / (1-2*v) / (1+v);
    double Estar = E / (2*(1-v*v));
    double b = 2*std::sqrt(std::abs(P*R/(M_PI*Estar)));
    double p0 = std::sqrt(std::abs(P*Estar/(M_PI*R)));

    // Closed form solution
    std::function<Vec3d(Vec2d)> analytical = [=] (const Vec2d& p) {
        double x = p[0], z = p[1];
        double bxz = b*b - x*x + z*z;
        double xz = 4*x*x*z*z;
        double koren = std::sqrt(bxz*bxz + xz);
        double m2 = 0.5 * (koren + bxz);
        double n2 = 0.5 * (koren - bxz);
        double m = std::sqrt(m2);
        double n = signum(x) * std::sqrt(n2);
        double mpn = m2 + n2;
        double zmn = (z*z + n2) / mpn;
        double sxx = -p0 / b * (m * (1 + zmn) + 2*z);
        double syy = -p0 / b * m * (1 - zmn);
        double sxy = p0 / b * n * ((m2 - z*z) / mpn);
        return Vec3d(sxx, syy, sxy);
    };

    // Domain definition
    double H = 1;
    Vec2d low(-H, -H), high(H, 0);
    RectangleDomain<Vec2d> domain(low, high);
    int n = 100;
    double dy = H / n;
    domain.fillUniformWithStep(dy, dy);

    // Refine
    std::vector<double> lengths = {1000, 500, 200, 100, 50, 20, 10, 5, 4, 3, 2};
    Vec2d center = {0, 0};
    for (double l : lengths) {
        Range<int> to_refine = domain.positions.filter([&] (const Vec2d& x) {
            return std::max(std::abs(x[0]-center[0]), std::abs(x[1] - center[1])) < l*b;
        });
        domain.refine(to_refine, 8, 0.4);
    }

    // Set indices for different domain parts
    const static double tol = 1e-10;
    Range<int> internal = domain.types > 0,
            boundary = domain.types < 0,
            top    = domain.positions.filter([=](const Vec2d &p) { return std::abs(p[1] - high[1]) < tol; }),
            bottom = domain.positions.filter([=](const Vec2d &p) { return std::abs(p[1] - low[1]) < tol; }),
            right  = domain.positions.filter([=](const Vec2d &p) { return std::abs(p[0] - high[0]) < tol && std::abs(p[1] - high[1]) > tol && std::abs(p[1] - low[1]) > tol; }),
            left   = domain.positions.filter([=](const Vec2d &p) { return std::abs(p[0] - low[0]) < tol && std::abs(p[1] - high[1]) > tol && std::abs(p[1] - low[1]) > tol; }),
            all = domain.types != 0;

    int N = domain.size();
    int support_size = 9;
    domain.findSupport(support_size);

    NNGaussians<Vec2d> weight(1.0);
    NNGaussians<Vec2d> g9(350.0, 9);
    EngineMLS<Vec2d, NNGaussians, NNGaussians> mls(g9, weight);

    // Initialize operators on all nodes
    auto op = make_mlsm(domain, mls, all, false);

    std::cout << "Operators constructed: " << op << std::endl;

    typedef SparseMatrix<double, RowMajor> matrix_t;
    matrix_t M(2 * N, 2 * N);
    M.reserve(Range<int>(2*N, 2 * support_size));
    VectorXd rhs(2*N);

    // Set equation on interior
    for (int i : internal) {
        op.lapvec(M, i, mu);
        op.graddiv(M, i, lam + mu);  // graddiv + laplace in interior
        rhs(i) = 0;
        rhs(i+N) = 0;
    }

    // Set bottom boundary conditions
    for (int i : bottom) {
        M.coeffRef(i, i) = 1;
        rhs(i) = 0;
        M.coeffRef(i+N, i+N) = 1;
        rhs(i+N) = 0;
    }

    // Set left boundary conditions
    for (int i : left) {
        M.coeffRef(i, i) = 1;
        rhs(i) = 0;
        M.coeffRef(i+N, i+N) = 1;
        rhs(i+N) = 0;
    }

    // Set right boundary conditions
    for (int i : right) {
        M.coeffRef(i, i) = 1;
        rhs(i) = 0;
        M.coeffRef(i+N, i+N) = 1;
        rhs(i+N) = 0;
    }

    for (int i : top) {
        // traction in x-direction
        op.der1(M,0,1,i,mu,0);
        op.der1(M,1,0,i,mu,0);
        // traction in y-direction
        op.der1(M,0,0,i,lam,1);
        op.der1(M,1,1,i,2.*mu+lam,1);
        double x = std::abs(domain.positions[i][0]);
        double trac = (x <= b) ? -p0 * std::sqrt(1 - x*x/b/b) : 0.0;
        rhs(i) = 0;
        rhs(i+N) = trac;
    }
    M.makeCompressed();

    std::cout << "Matrix constructed." << std::endl;

    // Solve the system
    BiCGSTAB<matrix_t, IncompleteLUT<double>> solver;
    solver.preconditioner().setDroptol(1e-6);
    solver.preconditioner().setFillfactor(100);
    solver.compute(M);

    std::cout << "Preconditioner computed." << std::endl;

    solver.setMaxIterations(10000);
    solver.setTolerance(1e-15);
    VectorXd sol = solver.solve(rhs);

    std::cout << "System solved." << std::endl;

    // Postprocess, calculate the stresses
    Range<Vec2d> disp(N, 0);
    Range<Vec3d> stress(N);
    for (int i : all) {
        disp[i] = Vec2d({sol[i], sol[i+N]});
    }
    for (int i : all) {
        auto grad = op.grad(disp, i);
        stress[i][0] = (2*mu + lam)*grad(0,0) + lam*grad(1,1);
        stress[i][1] = lam*grad(0,0) + (2*mu+lam)*grad(1,1);
        stress[i][2] = mu*(grad(0,1)+grad(1,0));
    }

    // Compare against analytical solution
    Range<double> error(N);
    for (int i = 0; i < N; ++i) {
        if (domain.positions[i].norm() < 3*b) {  // error under contact
            Vec3d astress = analytical(domain.positions[i]);
            error[i] = (stress[i] - astress).cwiseAbs().maxCoeff() / p0;
        }
    }
    double L_inf_error = *std::max_element(error.begin(), error.end());
    prn(L_inf_error);
    std::cout << "If you choose a smaller discretization step or increase refinement, "
              << "the error decreases." << std::endl;

    return 0;
}
