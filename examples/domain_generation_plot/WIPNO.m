close all;clear;
addpath('data');
%loader
% PDS_3D
% PDS_2D
RELAX

f = createFig('ps','m2 1x1 p1', 'scale', 1);hold on 
% 2D PLOT
% p = plot(nodes(1,:,1), nodes(1,:,2),'k.','markersize',4);

axis([0 1 0 1])
% axis equal; 
% for ii=1:size(nodes,1) 
%     p.XData=nodes(ii,:,1);
%     p.YData=nodes(ii,:,2);
%     pause(0.01);
%     ii
% end
%%
iii = boundary_map(boundary + 1) + 1;
plot(nodes_final(:,1),nodes_final(:,2),'k.','markersize',3)
quiver(nodes_final(boundary + 1,1),nodes_final(boundary + 1,2), 0.1*normal(iii,1),0.1*normal(iii,2),'AutoScale','off')
% 3D PLOT
% if (size(nodes,3) == 3)
%     plot3(nodes(:,1), nodes(:,2), nodes(:,3),'k.','markersize',2);
%     grid minor
%     % axis([0 1.2 0 1.5 0 1.1])
%     view([150 20])
% end

 
% exportfig(f, 'fig', 'format', 'png', 'color', 'rgb', 'Resolution', 200, 'Bounds', 'loose');
