#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_fill_engines.hpp"
#include "domain_relax_engines.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "io.hpp"
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <Eigen/PardisoSupport>

using namespace std;
using namespace mm;
using namespace Eigen;

class Solver {
  public:
    template<typename vec_t, template<class> class domain_t>
    Eigen::VectorXd solve(domain_t<vec_t>& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        int basis_size = conf.get<int>("params.mls.m");
        double basis_sigma = conf.get<double>("params.mls.sigmaB");
        double weight_sigma = conf.get<double>("params.mls.sigmaW");

        string basis_type = conf.get<string>("params.mls.basis_type");
        string weight_type = conf.get<string>("params.mls.weight_type");

        if (basis_type == "gau") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              Monomials<vec_t>(1));
            }
        } else if (basis_type == "mon") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              Monomials<vec_t>(1));
            }
        }
        assert_msg(false, "Unknown basis type '%s' or weight type '%s'.", basis_type, basis_sigma);
        throw "";
    }

  private:
    template<typename domain_t, typename basis_t, typename weight_t>
    Eigen::VectorXd solve_(domain_t& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer,
                           const basis_t& basis, const weight_t& weight) {
        int N = domain.size();
        int support_size = conf.get<int>("params.mls.n");

        double a = conf.get<double>("params.case.a");
        double r = conf.get<double>("params.case.r");
        double lam_inner = conf.get<double>("params.case.lam_inner");
        double lam_outer = conf.get<double>("params.case.lam_outer");
        double q0 = conf.get<double>("params.case.q0");
        auto mls = make_mls(basis, weight);
        timer.addCheckPoint("shapes");
        RaggedShapeStorage<domain_t, decltype(mls), mlsm::lap|mlsm::d1> storage(domain, mls, domain.types != 0, false);
        MLSM<decltype(storage)> op(storage);  // All nodes, including boundary

        timer.addCheckPoint("matrix");
        SparseMatrix<double, RowMajor> M(N, N);
        M.reserve(storage.support_sizes());
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

        double smooth_band = conf.get<double>("params.num.smooth_band") * domain.characteristicDistance();
        Range<double> lam(N), q(N);
        for (int i = 0; i < N; ++i) {
            double cur_r = domain.positions[i].norm();
            if (cur_r < r-smooth_band) {
                lam[i] = lam_inner;
                q[i] = q0 * cur_r;
            } else if (cur_r < r + smooth_band) {
                lam[i] = (cur_r - r + smooth_band)/2/smooth_band * lam_outer + (r+smooth_band-cur_r) / 2 / smooth_band * lam_inner;
                q[i] = 0;
            } else {
                lam[i] = lam_outer;
                q[i] = 0;
            }
        }

        // Set equation on interior
        for (int i : (domain.types > 0)) {
            op.lap(M, i, lam[i]);  // laplace in interior
            op.grad(M, i, op.grad(lam, i));
            rhs(i) = -q[i];
        }
        // Set boundary conditions
        for (int i : (domain.types < 0)) {
            M.coeffRef(i, i) = 1;
            rhs(i) = 0;
        }

        SparseMatrix<double> M2(M);
        M2.makeCompressed();

//          file.reopenFile();
//          file.reopenFolder();
//          file.setSparseMatrix("M", M2);
//          file.setDoubleArray("rhs", rhs);
//          file.closeFile();

//        double droptol = conf.get<double>("params.solver.droptol");
//        int fill_factor = conf.get<int>("params.solver.fill_factor");
//        double errtol = conf.get<double>("params.solver.errtol");
//        int maxiter = conf.get<int>("params.solver.maxiter");
//        BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
//        solver.preconditioner().setDroptol(droptol);
//        solver.preconditioner().setFillfactor(fill_factor);
//        solver.setMaxIterations(maxiter);
//        solver.setTolerance(errtol);
//        PardisoLU<SparseMatrix<double>> solver;
        timer.addCheckPoint("solve");
        SparseLU<SparseMatrix<double>> solver;
        solver.compute(M2);
        return solver.solve(rhs);
    }
};

int main() {
    XMLloader conf("params/one_domain_implicit_smooth.xml");
    std::string name = conf.get<std::string>("params.meta.filename");
    HDF5IO file("data/"+name+"_wip.h5", HDF5IO::DESTROY);

    file.openFolder("/");
    double a = conf.get<double>("params.case.a");
    double r = conf.get<double>("params.case.r");
    double lam_inner = conf.get<double>("params.case.lam_inner");
    double lam_outer = conf.get<double>("params.case.lam_outer");
    double q0 = conf.get<double>("params.case.q0");
    file.setDoubleAttribute("R1", r);
    file.setDoubleAttribute("R2", a);
    file.setDoubleAttribute("lam1", lam_inner);
    file.setDoubleAttribute("lam2", lam_outer);
    file.setDoubleAttribute("q0", q0);
    file.closeFile();

    vector<string> nxs = split(conf.get<std::string>("params.num.nxs"), ",");
    for (const string& snx : nxs) {
        int nx = std::stoi(snx);
        prn(nx);
        Timer timer;
        timer.addCheckPoint("begin");

        CircleDomain<Vec2d> domain(0, a);
        domain.fillUniformBoundaryWithStep(2. * a / nx);
        PoissonDiskSamplingFill fill;
        domain.apply(fill, 2. * a / nx);

        BasicRelax relax;
        int riter = conf.get<int>("params.num.riter");
        relax.iterations(riter).projectionType(0).initialHeat(0.5);
        domain.apply(relax, 2. * a / nx);

        int support_size = conf.get<int>("params.mls.n");
        domain.findSupport(support_size);

        prn(domain.size());

        file.reopenFile();
        file.openFolder(format("/%04d", nx));
        file.setDomain("domain", domain);
        file.closeFile();

        Solver solver;
        auto solution = solver.solve(domain, conf, file, timer);

        timer.addCheckPoint("end");
        prn(timer.getTime("begin", "end"));

        file.reopenFile();
        file.reopenFolder();
        file.setDoubleArray("sol", solution);
        file.setTimer("timer", timer);
        file.closeFile();
    }

    return 0;
}
