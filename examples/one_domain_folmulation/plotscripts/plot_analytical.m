close all
q0 = 10;
lam1 = 2;
lam2 = 1;
R1 = 1;
R2 = 2;
theta = linspace(0, 2*pi, 50);
r = linspace(0, R2, 50);
[R,T] = meshgrid(r, theta);
X = R.*cos(T);
Y = R.*sin(T);

Z = heat_analytical(X, Y, R1, R2, lam1, lam2, q0);

f1 = setfig('b1'); hold off;
surf(X, Y, Z)