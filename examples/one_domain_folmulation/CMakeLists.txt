project(ELES_domain_formulation)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_CXX_FLAGS "-std=c++11")

add_subdirectory(${CMAKE_SOURCE_DIR}/../../ build_util)
include_directories(${CMAKE_SOURCE_DIR}/../../src/)
include_directories(SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/../../src/third_party/)

include_directories(/opt/intel/compilers_and_libraries/linux/mkl/include)
link_directories(/opt/intel/compilers_and_libraries/linux/mkl/lib/intel64)
link_directories(/opt/intel/compilers_and_libraries/linux/lib/intel64)

function(register name)
    add_executable(${name} src/${name}.cpp)
    target_link_libraries(${name} pde_utils)
    target_compile_options(${name} PRIVATE "-fopenmp")
    target_compile_definitions(${name} PUBLIC EIGEN_USE_MKL_VML MKL_LP64)
    target_link_libraries(${name} pde_utils mkl_intel_lp64 mkl_intel_thread mkl_core pthread iomp5)
endfunction(register)

register(one_domain_implicit)
register(one_domain_implicit_smooth)
register(two_domain_implicit)
