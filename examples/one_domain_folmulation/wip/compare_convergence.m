prepare
format compact

casename1 = 'one_domain_implicit_smooth_wip';
casename2 = 'two_domain_implicit_wip';
casename = '';
datafile1 = [datapath casename1 casename '.h5'];
datafile2 = [datapath casename2 casename '.h5'];

R1 = h5readatt(datafile1, '/', 'R1');
R2 = h5readatt(datafile1, '/', 'R2');
lam1 = h5readatt(datafile1, '/', 'lam1');
lam2 = h5readatt(datafile1, '/', 'lam2');
q0 = h5readatt(datafile1, '/', 'q0');
TR1 = h5readatt(datafile2, '/', 'R1');
TR2 = h5readatt(datafile2, '/', 'R2');
Tlam1 = h5readatt(datafile2, '/', 'lam1');
Tlam2 = h5readatt(datafile2, '/', 'lam2');
Tq0 = h5readatt(datafile2, '/', 'q0');
assert(TR1 == R1, '%f != %f', TR1, R1);
assert(TR2 == R2, '%f != %f', TR2, R2);
assert(Tlam1 == lam1, '%f != %f', Tlam1, lam1);
assert(Tlam2 == lam2, '%f != %f', Tlam2, lam2);
assert(Tq0 == q0, '%f != %f', Tq0, q0);

info1 = h5info(datafile1);
ng1 = length(info1.Groups);
data1 = zeros(ng1, 2);
for i = 1:ng1
    name = info1.Groups(i).Name
    pos = h5read(datafile1, [name '/domain/pos']);
    x = pos(1, :);
    y = pos(2, :);

    try
        sol = h5read(datafile1, [name '/sol']);
    catch
        break
    end
    asol = heat_analytical(x, y, R1, R2, lam1, lam2, q0)';
    err = max(abs(asol - sol));

    data1(i, 1) = length(x);
    data1(i, 2) = err;
end

info2 = h5info(datafile2);
ng2 = length(info2.Groups);
data2 = zeros(ng2, 2);
for i = 1:ng2
    name = info2.Groups(i).Name
    pos = h5read(datafile2, [name '/domain/pos']);
    x = pos(1, :);
    y = pos(2, :);
    pos2 = h5read(datafile2, [name '/circ/pos']);
    x2 = pos2(1, :);
    y2 = pos2(2, :);
    
    try
        sol = h5read(datafile2, [name '/sol']);
    catch
        break
    end
    asol = heat_analytical([x x2], [y y2], R1, R2, lam1, lam2, q0)';
    err = max(abs(asol - sol));

    data2(i, 1) = length(x)+length(x2);
    data2(i, 3) = err;
end


setfig('b1');
plot(data1(:, 1), data1(:, 2), 'o-')
plot(data2(:, 1), data2(:, 3), 'x-')
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
title(sprintf('$\\ell_\\infty$ error, $\\lambda_1 = %.2f$, $\\lambda_2 = %.2f$', lam1, lam2))
xlabel('$N$')
legend('one domain', 'two domain')


f4 = setfig('b3');
xden = linspace(-R2, R2, 501);
yden = linspace(-R2, R2, 501);
[X,Y] = meshgrid(xden, yden);
I = find(abs(Y) < 1e-4);
pos = h5read(datafile1, [name '/domain/pos']);
sol = h5read(datafile1, [name '/sol']);
x = double(pos(1, :));
y = double(pos(2, :));
Z = griddata(x, y, sol, X, Y, 'linear');

plot(X(I), Z(I), '-');

pos = h5read(datafile2, [name '/domain/pos']);
x = pos(1, :);
y = pos(2, :);
pos2 = h5read(datafile2, [name '/circ/pos']);
sol = h5read(datafile2, [name '/sol']);
x2 = pos2(1, :);
y2 = pos2(2, :);
xx = double([x x2]);
yy = double([y y2]);
Z2 = griddata(xx, yy, sol, X, Y, 'linear');
plot(X(I), Z2(I), '-');
A = heat_analytical(X(I), 0*X(I), R1, R2, lam1, lam2, q0);
plot(X(I), A, '.-');
legend('one domain', 'two domain', 'analytical')
xlabel('$x$')
ylabel('$u(x, 0)$')
title('$y=0$ section of the solution')
