prepare

casename = 'one_domain_implicit_wip';
casename = 'one_domain_implicit_smooth_wip';
datafile = [datapath casename '.h5'];
info = h5info(datafile);

name = '/0050';
name = '/0071';

pos = h5read(datafile, [name '/domain/pos']);
types = h5read(datafile, [name '/domain/types']);
supp = h5read(datafile, [name '/domain/supp']);

x = double(pos(1, :));
y = double(pos(2, :));

f3 = setfig('b3');
plot_domain(datafile, [name '/domain'])

R1 = h5readatt(datafile, '/', 'R1');
R2 = h5readatt(datafile, '/', 'R2');
lam1 = h5readatt(datafile, '/', 'lam1');
lam2 = h5readatt(datafile, '/', 'lam2');
q0 = h5readatt(datafile, '/', 'q0');

sol = h5read(datafile, [name '/sol']);
asol = heat_analytical(x, y, R1, R2, lam1, lam2, q0);


f1 = setfig('b1');
h = scatter(x, y, 15, sol, 'filled');
explore_domain(f1, h, pos, supp, types);
axis equal
colorbar
xlabel('$x$')
ylabel('$y$')

f2 = setfig('b2'); hold off
T = delaunay(double(x)', double(y)');
trisurf(T, x, y, sol, 'EdgeColor', 'none');
title('solution')
colorbar
xlabel('$x$')
ylabel('$y$')

f4 = setfig('b4');
xden = linspace(min(x), max(x), 501);
yden = linspace(min(y), max(y), 501);
[X,Y] = meshgrid(xden, yden);
I = find(abs(Y) < 1e-4);
Z = griddata(x, y, sol, X, Y, 'linear');
plot(X(I), Z(I), '-');
A = heat_analytical(X(I), 0*X(I), R1, R2, lam1, lam2, q0);
plot(X(I), A, '-');
legend('computed', 'analytical')