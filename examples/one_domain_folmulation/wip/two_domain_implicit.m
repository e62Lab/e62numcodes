prepare

casename = 'two_domain_implicit_wip';
datafile = [datapath casename '.h5'];
info = h5info(datafile);
name = '/0250';
pos = h5read(datafile, [name '/domain/pos']);
x = pos(1, :);
y = pos(2, :);
types = h5read(datafile, [name '/domain/types']);
N = length(x);

f3 = setfig('b3');
plot_domain(datafile, [name '/domain'])

f4 = setfig('b4');
plot_domain(datafile, [name '/circ'])
pos2 = h5read(datafile, [name '/circ/pos']);
x2 = pos2(1, :);
y2 = pos2(2, :);
N2 = length(x2);

idx_map = h5read(datafile, [name '/idx_map']);
types(idx_map(idx_map ~= -1) + 1) = -4;


R1 = h5readatt(datafile, '/', 'R1');
R2 = h5readatt(datafile, '/', 'R2');
lam1 = h5readatt(datafile, '/', 'lam1');
lam2 = h5readatt(datafile, '/', 'lam2');
q0 = h5readatt(datafile, '/', 'q0');

sol = h5read(datafile, [name '/sol']);
asol = heat_analytical([x x2], [y y2], R1, R2, lam1, lam2, q0);

err = max(abs(sol'-asol))

f1 = setfig('b1');
h = scatter(x, y, 15, sol(1:N), 'filled');
h2 = scatter(x2, y2, 15, sol(N+1:end), 'filled');
axis equal
colorbar
xlabel('$x$')
ylabel('$y$')

f2 = setfig('b2'); hold off
T = delaunay(double(x)', double(y)');
trisurf(T, x, y, sol(1:N));
hold on
T2 = delaunay(double(x2)', double(y2)');
trisurf(T2, x2, y2, sol(N+1:end));
title('solution')
colorbar
xlabel('$x$')
ylabel('$y$')

f4 = setfig('b4');
xx = double([x x2]);
yy = double([y y2]);
xden = linspace(min(xx), max(xx), 501);
yden = linspace(min(yy), max(yy), 501);
[X,Y] = meshgrid(xden, yden);
I = find(abs(Y) < 1e-4);
Z = griddata(xx, yy, sol, X, Y, 'linear');
plot(X(I), Z(I), '-');
A = heat_analytical(X(I), 0*X(I), R1, R2, lam1, lam2, q0);
plot(X(I), A, '-');
legend('computed', 'analytical')