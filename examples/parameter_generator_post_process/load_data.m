remove_duplicates = 1
fn=[working_dir,case_name,'.h5'];
info = hdf5info(fn);
no_steps=size(info.GroupHierarchy.Groups,2);
ii=no_steps - 1; 

% support = h5read(fn,'/step0/support');
types = h5read(fn,'/step0/types');
pos = h5read(fn,'/step0/pos');
normals = h5read(fn,'/step0/normals');
boundary_map = h5read(fn,'/step0/boundary_map');


%actual load
step=['/step',num2str(ii),'/'];
for var=regV eval([cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
for var=atts eval( [cell2mat(var),'=','h5readatt(''',fn,''',''',step,''',''',cell2mat(var),''');']);end
skin = h5read(fn,'/step0/skin');

%debug load.
addpath(working_dir);
eval(case_name);
curr_step = ii;

%% remove duplicate nodes on alu-air boundary
if(remove_duplicates)
    y = pos'; 
    [x,i] = unique(y, 'rows');
    pos = x';
    T = T(i);
    v = v(:,i);
    types = types(i);
end


% disp(Q_Nu)