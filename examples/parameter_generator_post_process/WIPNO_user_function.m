% place holder for user functions. This is called each time a key is pressed in 
% in WIPNO plot. 
%
dir = 'data\lid_driven\';
addpath(dir)

lid_driven_wip

if(evalin('base', '~exist(''fig_user'',''var'')'))
	fig_user=createFig('monitor',2,'divider',1,'divider_y',2,'pos',1,'scale',0);hold on;    
 	putvar(fig_user);
 	evalin('base', 'figure(fig)');
 end


