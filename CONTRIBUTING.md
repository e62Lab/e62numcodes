We welcome contributions from all sources. 
Contribution can be done via merge requests. 
Please, follow the rules:
 - one change per contribution
 - meaningfully describe and add motivation for the change
 - all new functionalities must have tests
 - the code must follow the style guide
 - all new features must be properly documented
 - `./run_tests.sh` script must pass before making a request
 - Gitlab CI integration must pass for your merge request
 - add yourself to the alphabetical list of contributors

Contributors:

- Kolman Maks
- Kosec Gregor
- Pribec Ivan
- Slak Jure